#pragma checksum "C:\Faks\RPPP\17\JavnaNabava\Views\TroskovnikNatjecajMD\NovaStavkaTemplate.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b8b993024134a0b059d922b9338856a382ea54dc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TroskovnikNatjecajMD_NovaStavkaTemplate), @"mvc.1.0.view", @"/Views/TroskovnikNatjecajMD/NovaStavkaTemplate.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Model;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Controllers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b8b993024134a0b059d922b9338856a382ea54dc", @"/Views/TroskovnikNatjecajMD/NovaStavkaTemplate.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1928bef4f2f882be9d96e79ffd3ac429fa5a1eda", @"/Views/_ViewImports.cshtml")]
    public class Views_TroskovnikNatjecajMD_NovaStavkaTemplate : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<table id=""template"" style=""visibility:hidden"">
    <tr>
        <td class=""text-left col-sm-2"">
            <input type=""hidden"" name=""Stavke.Index"" value=""--sifra--"" />
            
            <input class=""form-control"" name=""Stavke[--sifra--].Opis"" value=""--opis--"" />
        </td>
        <td class=""text-left col-sm-1"">
            <input class=""form-control"" name=""Stavke[--sifra--].Kolicina"" value=""--kolicina--"" />
        </td>
        <td class=""text-left col-sm-4"">
            <input class=""form-control"" name=""Stavke[--sifra--].Naziv"" value=""--naziv--"" />
        </td>
        <td class=""text-right col-sm-2"">
            <button class=""btn btn-sm btn-danger deleterow"" title=""Izbaci"">
                <i class=""fa fa-minus""></i>
            </button>
        </td>
    </tr>
</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
