#pragma checksum "C:\Faks\RPPP\17\JavnaNabava\Views\Konzorcij\JTable.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5c7a3549dfb0de62a56e461e3f4c9033a3d382f8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Konzorcij_JTable), @"mvc.1.0.view", @"/Views/Konzorcij/JTable.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Model;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Faks\RPPP\17\JavnaNabava\Views\_ViewImports.cshtml"
using JavnaNabava.Controllers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5c7a3549dfb0de62a56e461e3f4c9033a3d382f8", @"/Views/Konzorcij/JTable.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1928bef4f2f882be9d96e79ffd3ac429fa5a1eda", @"/Views/_ViewImports.cshtml")]
    public class Views_Konzorcij_JTable : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5c7a3549dfb0de62a56e461e3f4c9033a3d382f83679", async() => {
                WriteLiteral(@"
    <meta charset=""utf-8"" />
    <title>Pregled svih konzorcija</title>
    <link href=""lib/jqueryui/themes/base/jquery-ui.min.css"" rel=""stylesheet"" type=""text/css"" />
    <link href=""lib/jtable/lib/themes/lightcolor/blue/jtable.min.css"" rel=""stylesheet"" type=""text/css"" />
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5c7a3549dfb0de62a56e461e3f4c9033a3d382f84936", async() => {
                WriteLiteral(@"

    <div class=""container"">
        <div class=""row"">
            <div class=""col-sm-12"" id=""konzorciji"">

            </div>
        </div>
    </div>

    <script type=""text/javascript"" src=""lib/jquery/jquery.min.js""></script>
    <script type=""text/javascript"" src=""lib/jqueryui/jquery-ui.min.js""></script>
    <script type=""text/javascript"" src=""lib/jtable/lib/jquery.jtable.min.js""></script>
    <script type=""text/javascript"" src=""lib/jtable/lib/localization/jquery.jtable.hr.js""></script>

    <script type=""text/javascript"">
        $(document).ready(function () {
            $('#konzorciji').jtable({
                title: 'Popis konzorcija',
                paging: true,
                pageSize: 5,
                sorting: true,
                defaultSorting: 'Oib DESC',
                actions: {
                    listAction: 'jtable/konzorcij/getall',
                    createAction: 'jtable/konzorcij/create',
                    updateAction: 'jtable/konzorcij/update',");
                WriteLiteral(@"
                    deleteAction: 'jtable/konzorcij/delete'
                },
                fields: {
                    Oib: {
                        create:true,
                        key: true,
                        list: true,
                        title: ""Oib konzorcija""
                    },
                    Naziv: {
                        title: 'Naziv konzorcija',
                        width: '20%'
                    },
                    AdresaSjedista: {
                        title: 'Adresa sjedišta',
                        width: '30%'
                    },
                    FkMjesto: {
                        title: 'Naziv mjesta',
                        width: '30%'
                    },
                    FkVrstaKonzorcija: {
                        title: 'Djelatnost konzorcija',
                        width: '20%',
                    }
                }
            });

            $(""#konzorciji"").jtable('load');
        });
    </");
                WriteLiteral("script>\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
