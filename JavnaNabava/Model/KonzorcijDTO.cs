﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Model {
    public class KonzorcijDTO {

        public Konzorcij konzorcij { get; set; }

        public Mjesto mjesto { get; set; }

        public SifVrstaKonzorcija sifVrstaKonzorcija { get; set; }

    }
}
