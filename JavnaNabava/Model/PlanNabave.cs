﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class PlanNabave
    {
        public PlanNabave()
        {
            StavkaNabaves = new HashSet<StavkaNabave>();
        }



        public int Id { get; set; }

        [Display(Name = "Naziv plana nabave", Prompt = "Unesite naziv")]
        [Required(ErrorMessage = "Naziv plana nabave je obvezno polje")]
        public string Naziv { get; set; }

        [Display(Name = "Tvrtka", Prompt = "Unesite OIB tvrtke")]
        [Required(ErrorMessage = "OIB tvrtke je obvezno polje")]
        public string FkTvrtkaOib { get; set; }

        public virtual Tvrtka FkTvrtkaOibNavigation { get; set; }

        public virtual ICollection<StavkaNabave> StavkaNabaves { get; set; }
    }
}
