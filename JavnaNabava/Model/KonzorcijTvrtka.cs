﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class KonzorcijTvrtka
    {
        [DisplayName("Tvrtka")]
        public string FkOibtvrtka { get; set; }
        [DisplayName("Konzorcij")]
        public string FkOibkonzorcij { get; set; }

        public virtual Konzorcij FkOibkonzorcijNavigation { get; set; }
        public virtual Tvrtka FkOibtvrtkaNavigation { get; set; }
    }
}
