﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class StavkaTroskovnika
    {
        public StavkaTroskovnika()
        {
            PonudaStavkaTroskovniks = new HashSet<PonudaStavkaTroskovnik>();
        }

        public int Id { get; set; }
        public string Opis { get; set; }
        public int Kolicina { get; set; }
        public int FkJedMjere { get; set; }
        public int FkTroskovnikNatjecaj { get; set; }

        public virtual SifJedMjere FkJedMjereNavigation { get; set; }
        public virtual TroskovnikNatjecaj FkTroskovnikNatjecajNavigation { get; set; }
        public virtual ICollection<PonudaStavkaTroskovnik> PonudaStavkaTroskovniks { get; set; }
    }
}
