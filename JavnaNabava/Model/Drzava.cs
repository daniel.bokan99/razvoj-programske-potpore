﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Drzava
    {
        public Drzava()
        {
            Mjestos = new HashSet<Mjesto>();
        }

        [Required(ErrorMessage = "Polje ISO je obavezno"),
            Display(Name = "ISO", Prompt = "Unesite Iso drzave")]
        public string Iso { get; set; }

        [Required(ErrorMessage = "Polje Naziv je obavezno"),
            Display(Name = "Naziv države", Prompt = "Unesite ime drzave")]
        public string Naziv { get; set; }

        public virtual ICollection<Mjesto> Mjestos { get; set; }
    }
}
