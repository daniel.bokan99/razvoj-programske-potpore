﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifDjelatnostTvrtke
    {
        public SifDjelatnostTvrtke()
        {
            JeDjelatnostis = new HashSet<JeDjelatnosti>();
            Natjecajs = new HashSet<Natjecaj>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Naziv je obvezno polje")]
        [Display(Name = "Naziv djelatnosti", Prompt = "Unesite naziv djelatnosti")]
        public string Naziv { get; set; }

        public virtual ICollection<JeDjelatnosti> JeDjelatnostis { get; set; }
        public virtual ICollection<Natjecaj> Natjecajs { get; set; }
    }
}
