﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class NatjecajZakon
    {
        public string EvidencijskiBr { get; set; }
        public int IdZakon { get; set; }

        public virtual Natjecaj EvidencijskiBrNavigation { get; set; }
        public virtual SifZakon IdZakonNavigation { get; set; }
    }
}
