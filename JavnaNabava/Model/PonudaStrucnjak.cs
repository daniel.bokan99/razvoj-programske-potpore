﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class PonudaStrucnjak
    {
        public PonudaStrucnjak()
        {
            Dokuments = new HashSet<Dokument>();
        }

        public int Id { get; set; }
        [Display(Name = "Povezana ponuda", Prompt = "Odaberite ponudu")]
        [Required(ErrorMessage = "Ponuda je obavezno polje")]
        public int FkPonudaId { get; set; }
        [Display(Name = "Povezan natječaj", Prompt = "Odaberite natječaj")]
        [Required(ErrorMessage = "Natječaj je obavezno polje")]
        public int FkNatjecajStrucnjaciId { get; set; }
        [Display(Name = "Povezana osoba", Prompt = "Odaberite osobu")]
        [Required(ErrorMessage = "osoba je obavezno polje")]
        public string FkOsobaOib { get; set; }

        public virtual NatjecajStrucnjaci FkNatjecajStrucnjaci { get; set; }
        public virtual Osoba FkOsobaOibNavigation { get; set; }
        public virtual Ponudum FkPonuda { get; set; }
        public virtual ICollection<Dokument> Dokuments { get; set; }
    }
}
