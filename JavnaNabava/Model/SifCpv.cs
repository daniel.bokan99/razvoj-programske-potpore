﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifCpv
    {
        public SifCpv()
        {
            StavkaNabaves = new HashSet<StavkaNabave>();
        }

        [Display(Name = "CPV šifra", Prompt = "Unesite CPV šifru")]
        [Required(ErrorMessage = "CPV šifra je obvezno polje")]
        public string Cpv { get; set; }

        [Display(Name = "Naziv", Prompt = "Unesite naziv")]
        [Required(ErrorMessage = "Naziv je obvezno polje")]
        public string Naziv { get; set; }

        public virtual ICollection<StavkaNabave> StavkaNabaves { get; set; }
    }
}
