﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Tvrtka
    {
        public Tvrtka()
        {
            JeDjelatnostis = new HashSet<JeDjelatnosti>();
            KonzorcijTvrtkas = new HashSet<KonzorcijTvrtka>();
            Osobas = new HashSet<Osoba>();
            PlanNabaves = new HashSet<PlanNabave>();
            Ponuda = new HashSet<Ponudum>();
            TvrtkaKontaktis = new HashSet<TvrtkaKontakti>();
        }

        [Required(ErrorMessage = "Oib polje je obavezno")]
        [MaxLength(11, ErrorMessage = "Duljina oiba mora biti 11")]
        [MinLength(11, ErrorMessage = "Duljina oiba mora biti 11")]
        public string Oib { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Mjesto je obavezno")]
        [DisplayName("Mjesto")]
        public int FkMjesto { get; set; }
        [Required(ErrorMessage = "Adresa je obavezna")]
        public string Adresa { get; set; }

        public virtual Mjesto FkMjestoNavigation { get; set; }
        public virtual ICollection<JeDjelatnosti> JeDjelatnostis { get; set; }
        public virtual ICollection<KonzorcijTvrtka> KonzorcijTvrtkas { get; set; }
        public virtual ICollection<Osoba> Osobas { get; set; }
        public virtual ICollection<PlanNabave> PlanNabaves { get; set; }
        public virtual ICollection<Ponudum> Ponuda { get; set; }
        public virtual ICollection<TvrtkaKontakti> TvrtkaKontaktis { get; set; }
    }
}
