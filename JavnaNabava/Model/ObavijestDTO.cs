﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Model {
    public class ObavijestDTO {

        public int Id { get; set; }

        public string Naslov { get; set; }

        public string Url { get; set; }

        public string NazivKategorije { get; set; }
    }
}
