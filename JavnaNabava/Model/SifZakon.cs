﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifZakon
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Naziv je obvezno polje")]
        [Display(Name = "Naziv zakona", Prompt = "Unesite naziv zakona")]
        public string Naziv { get; set; }
    }
}
