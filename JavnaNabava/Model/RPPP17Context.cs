﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class RPPP17Context : DbContext
    {


        public RPPP17Context(DbContextOptions<RPPP17Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Dokument> Dokument { get; set; }
        public virtual DbSet<Drzava> Drzava { get; set; }
        public virtual DbSet<JeDjelatnosti> JeDjelatnosti { get; set; }
        public virtual DbSet<Kontakti> Kontakti { get; set; }
        public virtual DbSet<Konzorcij> Konzorcij { get; set; }
        public virtual DbSet<KonzorcijKontakti> KonzorcijKontakti { get; set; }
        public virtual DbSet<KonzorcijTvrtka> KonzorcijTvrtka { get; set; }
        public virtual DbSet<Mjesto> Mjesto { get; set; }
        public virtual DbSet<Natjecaj> Natjecaj { get; set; }
        public virtual DbSet<NatjecajKriterijOcjenjivanja> NatjecajKriterijOcjenjivanja { get; set; }
        public virtual DbSet<NatjecajStrucnjaci> NatjecajStrucnjaci { get; set; }
        public virtual DbSet<NatjecajZakon> NatjecajZakon { get; set; }
        public virtual DbSet<Obavijest> Obavijest { get; set; }
        public virtual DbSet<Osoba> Osoba { get; set; }
        public virtual DbSet<OsobaKontakt> OsobaKontakt { get; set; }
        public virtual DbSet<PlanNabave> PlanNabave { get; set; }
        public virtual DbSet<PonudaOcjenaKriterija> PonudaOcjenaKriterija { get; set; }
        public virtual DbSet<PonudaStavkaTroskovnik> PonudaStavkaTroskovnik { get; set; }
        public virtual DbSet<PonudaStrucnjak> PonudaStrucnjak { get; set; }
        public virtual DbSet<Ponudum> Ponuda { get; set; }
        public virtual DbSet<SifCpv> SifCpv { get; set; }
        public virtual DbSet<SifDjelatnostTvrtke> SifDjelatnostTvrtke { get; set; }
        public virtual DbSet<SifFunkcija> SifFunkcija { get; set; }
        public virtual DbSet<SifJedMjere> SifJedMjere { get; set; }
        public virtual DbSet<SifKategorijeObavijesti> SifKategorijeObavijesti { get; set; }
        public virtual DbSet<SifStrucnaSprema> SifStrucnaSprema { get; set; }
        public virtual DbSet<SifTipKontaktum> SifTipKontakta { get; set; }
        public virtual DbSet<SifValutum> SifValuta { get; set; }
        public virtual DbSet<SifVrstaDokumentum> SifVrstaDokumenta { get; set; }
        public virtual DbSet<SifVrstaKonzorcija> SifVrstaKonzorcija { get; set; }
        public virtual DbSet<SifVrstaPonuditelja> SifVrstaPonuditelja { get; set; }
        public virtual DbSet<SifVrstaPostupka> SifVrstaPostupka { get; set; }
        public virtual DbSet<SifVrstaUgovora> SifVrstaUgovora { get; set; }
        public virtual DbSet<SifZakon> SifZakon { get; set; }
        public virtual DbSet<StavkaNabave> StavkaNabave { get; set; }
        public virtual DbSet<StavkaTroskovnika> StavkaTroskovnika { get; set; }
        public virtual DbSet<TroskovnikNatjecaj> TroskovnikNatjecaj { get; set; }
        public virtual DbSet<TroskovnikPonuda> TroskovnikPonuda { get; set; }
        public virtual DbSet<Tvrtka> Tvrtka { get; set; }
        public virtual DbSet<TvrtkaKontakti> TvrtkaKontakti { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Croatian_CI_AS");

            modelBuilder.Entity<Dokument>(entity =>
            {
                entity.HasKey(e => e.UrudzbeniBr)
                    .HasName("PK__Dokument__486BEB9E8300236B");

                entity.ToTable("Dokument");

                entity.Property(e => e.UrudzbeniBr).HasMaxLength(50);

                entity.Property(e => e.DatumDokumenta).HasColumnType("date");

                entity.Property(e => e.FileBinary).IsRequired();

                entity.Property(e => e.FkNatjecajDokumentEvBr)
                    .HasMaxLength(30)
                    .HasColumnName("FK_NatjecajDokumentEvBr");

                entity.Property(e => e.FkPonudaId).HasColumnName("FK_PonudaId");

                entity.Property(e => e.FkPonudaStrucnjakId).HasColumnName("FK_PonudaStrucnjakId");

                entity.Property(e => e.FkVrstaDokumentaId).HasColumnName("FK_vrstaDokumentaId");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.FkNatjecajDokumentEvBrNavigation)
                    .WithMany(p => p.Dokuments)
                    .HasForeignKey(d => d.FkNatjecajDokumentEvBr)
                    .HasConstraintName("FK__Dokument__FK_Nat__7C1A6C5A");

                entity.HasOne(d => d.FkPonuda)
                    .WithMany(p => p.Dokuments)
                    .HasForeignKey(d => d.FkPonudaId)
                    .HasConstraintName("FK__Dokument__FK_Pon__7A3223E8");

                entity.HasOne(d => d.FkPonudaStrucnjak)
                    .WithMany(p => p.Dokuments)
                    .HasForeignKey(d => d.FkPonudaStrucnjakId)
                    .HasConstraintName("FK__Dokument__FK_Pon__7B264821");

                entity.HasOne(d => d.FkVrstaDokumenta)
                    .WithMany(p => p.Dokuments)
                    .HasForeignKey(d => d.FkVrstaDokumentaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Dokument__FK_vrs__7D0E9093");
            });

            modelBuilder.Entity<Drzava>(entity =>
            {
                entity.HasKey(e => e.Iso)
                    .HasName("PK__Drzava__C4979A221E169198");

                entity.ToTable("Drzava");

                entity.Property(e => e.Iso)
                    .HasMaxLength(50)
                    .HasColumnName("ISO");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<JeDjelatnosti>(entity =>
            {
                entity.HasKey(e => new { e.Oib, e.Id })
                    .HasName("PK__jeDjelat__A81805FFADD9B4FB");

                entity.ToTable("jeDjelatnosti");

                entity.Property(e => e.Oib)
                    .HasMaxLength(50)
                    .HasColumnName("OIB");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.JeDjelatnostis)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__jeDjelatnost__Id__3493CFA7");

                entity.HasOne(d => d.OibNavigation)
                    .WithMany(p => p.JeDjelatnostis)
                    .HasForeignKey(d => d.Oib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__jeDjelatnos__OIB__339FAB6E");
            });

            modelBuilder.Entity<Kontakti>(entity =>
            {
                entity.ToTable("Kontakti");

                entity.Property(e => e.FkSifKontaktaId).HasColumnName("FK_sifKontaktaId");

                entity.Property(e => e.KontaktPodatci)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.FkSifKontakta)
                    .WithMany(p => p.Kontaktis)
                    .HasForeignKey(d => d.FkSifKontaktaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Kontakti__IdKont__282DF8C2");
            });

            modelBuilder.Entity<Konzorcij>(entity =>
            {
                entity.HasKey(e => e.Oib)
                    .HasName("PK__Konzorci__CB394B3F66A83D43");

                entity.ToTable("Konzorcij");

                entity.Property(e => e.Oib)
                    .HasMaxLength(50)
                    .HasColumnName("OIB");

                entity.Property(e => e.AdresaSjedista)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FkMjesto).HasColumnName("FK_Mjesto");

                entity.Property(e => e.FkVrstaKonzorcija).HasColumnName("FK_VrstaKonzorcija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkMjestoNavigation)
                    .WithMany(p => p.Konzorcijs)
                    .HasForeignKey(d => d.FkMjesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Konzorcij__FK_Mj__3864608B");

                entity.HasOne(d => d.FkVrstaKonzorcijaNavigation)
                    .WithMany(p => p.Konzorcijs)
                    .HasForeignKey(d => d.FkVrstaKonzorcija)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK__Konzorcij__FK_Vr__37703C52");
            });

            modelBuilder.Entity<KonzorcijKontakti>(entity =>
            {
                entity.HasKey(e => new { e.FkKontaktId, e.FkKonzorcijOib })
                    .HasName("PK__Konzorci__487A7BD448AFE232");

                entity.ToTable("KonzorcijKontakti");

                entity.Property(e => e.FkKontaktId).HasColumnName("FK_KontaktId");

                entity.Property(e => e.FkKonzorcijOib)
                    .HasMaxLength(50)
                    .HasColumnName("FK_KonzorcijOIB");

                entity.HasOne(d => d.FkKontakt)
                    .WithMany(p => p.KonzorcijKontaktis)
                    .HasForeignKey(d => d.FkKontaktId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Konzorcij__FK_Ko__11158940");

                entity.HasOne(d => d.FkKonzorcijOibNavigation)
                    .WithMany(p => p.KonzorcijKontaktis)
                    .HasForeignKey(d => d.FkKonzorcijOib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Konzorcij__FK_Ko__10216507");
            });

            modelBuilder.Entity<KonzorcijTvrtka>(entity =>
            {
                entity.HasKey(e => new { e.FkOibtvrtka, e.FkOibkonzorcij })
                    .HasName("PK__Konzorci__AB0E36313564ED85");

                entity.ToTable("KonzorcijTvrtka");

                entity.Property(e => e.FkOibtvrtka)
                    .HasMaxLength(50)
                    .HasColumnName("FK_OIBTvrtka");

                entity.Property(e => e.FkOibkonzorcij)
                    .HasMaxLength(50)
                    .HasColumnName("FK_OIBKonzorcij");

                entity.HasOne(d => d.FkOibkonzorcijNavigation)
                    .WithMany(p => p.KonzorcijTvrtkas)
                    .HasForeignKey(d => d.FkOibkonzorcij)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Konzorcij__FK_OI__3F115E1A");

                entity.HasOne(d => d.FkOibtvrtkaNavigation)
                    .WithMany(p => p.KonzorcijTvrtkas)
                    .HasForeignKey(d => d.FkOibtvrtka)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Konzorcij__FK_OI__3E1D39E1");
            });

            modelBuilder.Entity<Mjesto>(entity =>
            {
                entity.HasKey(e => e.PostanskiBroj)
                    .HasName("PK__Mjesto__9FDD638D3EE3CC9F");

                entity.ToTable("Mjesto");

                entity.Property(e => e.PostanskiBroj).ValueGeneratedNever();

                entity.Property(e => e.FkDrzavaIso)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FK_DrzavaISO");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.FkDrzavaIsoNavigation)
                    .WithMany(p => p.Mjestos)
                    .HasForeignKey(d => d.FkDrzavaIso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mjesto__FK_Drzav__1CBC4616");
            });

            modelBuilder.Entity<Natjecaj>(entity =>
            {
                entity.HasKey(e => e.EvidencijskiBr)
                    .HasName("PK__Natjecaj__61031FF8691EF62E");

                entity.ToTable("Natjecaj");

                entity.Property(e => e.EvidencijskiBr).HasMaxLength(30);

                entity.Property(e => e.IdDjelatnost).HasColumnName("Id_Djelatnost");

                entity.Property(e => e.IdStavkaNabave).HasColumnName("Id_StavkaNabave");

                entity.Property(e => e.IdVrstaUgovora).HasColumnName("Id_VrstaUgovora");

                entity.Property(e => e.JePonisten).HasColumnName("jePonisten");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PopisPotrebnihDokumenata)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RokZaDostavu).HasColumnType("date");

                entity.Property(e => e.TrajanjeUgovora)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.IdDjelatnostNavigation)
                    .WithMany(p => p.Natjecajs)
                    .HasForeignKey(d => d.IdDjelatnost)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Natjecaj__Id_Dje__56E8E7AB");

                entity.HasOne(d => d.IdStavkaNabaveNavigation)
                    .WithMany(p => p.Natjecajs)
                    .HasForeignKey(d => d.IdStavkaNabave)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Natjecaj__Id_Sta__55F4C372");

                entity.HasOne(d => d.IdVrstaUgovoraNavigation)
                    .WithMany(p => p.Natjecajs)
                    .HasForeignKey(d => d.IdVrstaUgovora)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Natjecaj__Id_Vrs__57DD0BE4");
            });

            modelBuilder.Entity<NatjecajKriterijOcjenjivanja>(entity =>
            {
                entity.ToTable("NatjecajKriterijOcjenjivanja");

                entity.Property(e => e.EvidencijskiBr)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.HasOne(d => d.EvidencijskiBrNavigation)
                    .WithMany(p => p.NatjecajKriterijOcjenjivanjas)
                    .HasForeignKey(d => d.EvidencijskiBr)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NatjecajK__Evide__5E8A0973");
            });

            modelBuilder.Entity<NatjecajStrucnjaci>(entity =>
            {
                entity.ToTable("NatjecajStrucnjaci");

                entity.Property(e => e.EvidencijskiBr)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.IdStrucnaSprema).HasColumnName("Id_StrucnaSprema");

                entity.Property(e => e.OpisKriterija)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.EvidencijskiBrNavigation)
                    .WithMany(p => p.NatjecajStrucnjacis)
                    .HasForeignKey(d => d.EvidencijskiBr)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NatjecajS__Evide__625A9A57");

                entity.HasOne(d => d.IdStrucnaSpremaNavigation)
                    .WithMany(p => p.NatjecajStrucnjacis)
                    .HasForeignKey(d => d.IdStrucnaSprema)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NatjecajS__Id_St__634EBE90");
            });

            modelBuilder.Entity<NatjecajZakon>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("NatjecajZakon");

                entity.Property(e => e.EvidencijskiBr)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.IdZakon).HasColumnName("Id_Zakon");

                entity.HasOne(d => d.EvidencijskiBrNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.EvidencijskiBr)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NatjecajZ__Evide__65370702");

                entity.HasOne(d => d.IdZakonNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdZakon)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NatjecajZ__Id_Za__662B2B3B");
            });

            modelBuilder.Entity<Obavijest>(entity =>
            {
                entity.ToTable("Obavijest");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("URL");

                entity.HasOne(d => d.IdKategorijeNavigation)
                    .WithMany(p => p.Obavijests)
                    .HasForeignKey(d => d.IdKategorije)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Obavijest__IdKat__3B40CD36");
            });

            modelBuilder.Entity<Osoba>(entity =>
            {
                entity.HasKey(e => e.Oib)
                    .HasName("PK__Osoba__CB394B3F709C0C02");

                entity.ToTable("Osoba");

                entity.Property(e => e.Oib)
                    .HasMaxLength(50)
                    .HasColumnName("OIB");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FkFunkcijaId).HasColumnName("FK_FunkcijaID");

                entity.Property(e => e.FkMjesto).HasColumnName("FK_Mjesto");

                entity.Property(e => e.FkStrucnaSprema).HasColumnName("FK_StrucnaSprema");

                entity.Property(e => e.FkTvrtka)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FK_Tvrtka");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.FkFunkcija)
                    .WithMany(p => p.Osobas)
                    .HasForeignKey(d => d.FkFunkcijaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osoba__FK_Funkci__245D67DE");

                entity.HasOne(d => d.FkMjestoNavigation)
                    .WithMany(p => p.Osobas)
                    .HasForeignKey(d => d.FkMjesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osoba__FK_Mjesto__236943A5");

                entity.HasOne(d => d.FkStrucnaSpremaNavigation)
                    .WithMany(p => p.Osobas)
                    .HasForeignKey(d => d.FkStrucnaSprema)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osoba__FK_Strucn__22751F6C");

                entity.HasOne(d => d.FkTvrtkaNavigation)
                    .WithMany(p => p.Osobas)
                    .HasForeignKey(d => d.FkTvrtka)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osoba__FK_Tvrtka__25518C17");
            });

            modelBuilder.Entity<OsobaKontakt>(entity =>
            {
                entity.HasKey(e => new { e.FkOsobaOib, e.FkKontaktId });

                entity.ToTable("OsobaKontakt");

                entity.Property(e => e.FkOsobaOib)
                    .HasMaxLength(50)
                    .HasColumnName("FK_OsobaOIB");

                entity.Property(e => e.FkKontaktId).HasColumnName("FK_KontaktId");

                entity.HasOne(d => d.FkKontakt)
                    .WithMany(p => p.OsobaKontakts)
                    .HasForeignKey(d => d.FkKontaktId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OsobaKont__FK_Ko__2B0A656D");

                entity.HasOne(d => d.FkOsobaOibNavigation)
                    .WithMany(p => p.OsobaKontakts)
                    .HasForeignKey(d => d.FkOsobaOib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OsobaKont__FK_Ko__2A164134");
            });

            modelBuilder.Entity<PlanNabave>(entity =>
            {
                entity.ToTable("PlanNabave");

                entity.Property(e => e.FkTvrtkaOib)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FK_TvrtkaOIB");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkTvrtkaOibNavigation)
                    .WithMany(p => p.PlanNabaves)
                    .HasForeignKey(d => d.FkTvrtkaOib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PlanNabav__FK_Tv__45BE5BA9");
            });

            modelBuilder.Entity<PonudaOcjenaKriterija>(entity =>
            {
                entity.ToTable("PonudaOcjenaKriterija");

                entity.Property(e => e.FkNatjecajKriterijOcjenjivanjaId).HasColumnName("FK_NatjecajKriterijOcjenjivanjaId");

                entity.Property(e => e.FkPonudaId).HasColumnName("FK_PonudaId");

                entity.HasOne(d => d.FkNatjecajKriterijOcjenjivanja)
                    .WithMany(p => p.PonudaOcjenaKriterijas)
                    .HasForeignKey(d => d.FkNatjecajKriterijOcjenjivanjaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaOcj__FK_Na__76619304");

                entity.HasOne(d => d.FkPonuda)
                    .WithMany(p => p.PonudaOcjenaKriterijas)
                    .HasForeignKey(d => d.FkPonudaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaOcj__FK_Po__7755B73D");
            });

            modelBuilder.Entity<PonudaStavkaTroskovnik>(entity =>
            {
                entity.ToTable("PonudaStavkaTroskovnik");

                entity.Property(e => e.FkNatjecajStavkaTroskovnik).HasColumnName("FK_NatjecajStavkaTroskovnik");

                entity.Property(e => e.FkTroskovnikPonuda).HasColumnName("FK_TroskovnikPonuda");

                entity.Property(e => e.Pdv).HasColumnName("PDV");

                entity.Property(e => e.UkupnaCijenaBezPdv).HasColumnName("UkupnaCijenaBezPDV");

                entity.Property(e => e.UkupnaCijenaSaPdv).HasColumnName("UkupnaCijenaSaPDV");

                entity.HasOne(d => d.FkNatjecajStavkaTroskovnikNavigation)
                    .WithMany(p => p.PonudaStavkaTroskovniks)
                    .HasForeignKey(d => d.FkNatjecajStavkaTroskovnik)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaSta__FK_Na__0C50D423");

                entity.HasOne(d => d.FkTroskovnikPonudaNavigation)
                    .WithMany(p => p.PonudaStavkaTroskovniks)
                    .HasForeignKey(d => d.FkTroskovnikPonuda)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaSta__FK_Tr__0B5CAFEA");
            });

            modelBuilder.Entity<PonudaStrucnjak>(entity =>
            {
                entity.ToTable("PonudaStrucnjak");

                entity.Property(e => e.FkNatjecajStrucnjaciId).HasColumnName("FK_NatjecajStrucnjaciId");

                entity.Property(e => e.FkOsobaOib)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FK_OsobaOIB");

                entity.Property(e => e.FkPonudaId).HasColumnName("FK_PonudaId");

                entity.HasOne(d => d.FkNatjecajStrucnjaci)
                    .WithMany(p => p.PonudaStrucnjaks)
                    .HasForeignKey(d => d.FkNatjecajStrucnjaciId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaStr__FK_Na__72910220");

                entity.HasOne(d => d.FkOsobaOibNavigation)
                    .WithMany(p => p.PonudaStrucnjaks)
                    .HasForeignKey(d => d.FkOsobaOib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaStr__FK_Os__73852659");

                entity.HasOne(d => d.FkPonuda)
                    .WithMany(p => p.PonudaStrucnjaks)
                    .HasForeignKey(d => d.FkPonudaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PonudaStr__FK_Po__719CDDE7");
            });

            modelBuilder.Entity<Ponudum>(entity =>
            {
                entity.Property(e => e.FkKonzorcijOib)
                    .HasMaxLength(50)
                    .HasColumnName("FK_KonzorcijOIB");

                entity.Property(e => e.FkTvrtkaOib)
                    .HasMaxLength(50)
                    .HasColumnName("FK_TvrtkaOIB");

                entity.Property(e => e.FkVrstaPonuditeljaId).HasColumnName("FK_vrstaPonuditeljaId");

                entity.Property(e => e.UkupnaCijenaPonude).HasColumnName("ukupnaCijenaPonude");

                entity.HasOne(d => d.FkKonzorcijOibNavigation)
                    .WithMany(p => p.Ponuda)
                    .HasForeignKey(d => d.FkKonzorcijOib)
                    .HasConstraintName("FK__Ponuda__FK_Konzo__6EC0713C");

                entity.HasOne(d => d.FkTvrtkaOibNavigation)
                    .WithMany(p => p.Ponuda)
                    .HasForeignKey(d => d.FkTvrtkaOib)
                    .HasConstraintName("FK__Ponuda__FK_Tvrtk__6DCC4D03");

                entity.HasOne(d => d.FkVrstaPonuditelja)
                    .WithMany(p => p.Ponuda)
                    .HasForeignKey(d => d.FkVrstaPonuditeljaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Ponuda__FK_vrsta__6CD828CA");
            });

            modelBuilder.Entity<SifCpv>(entity =>
            {
                entity.HasKey(e => e.Cpv)
                    .HasName("PK__sif_CPV__C1F8972024A51829");

                entity.ToTable("sif_CPV");

                entity.Property(e => e.Cpv)
                    .HasMaxLength(50)
                    .HasColumnName("CPV");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifDjelatnostTvrtke>(entity =>
            {
                entity.ToTable("sif_DjelatnostTvrtke");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifFunkcija>(entity =>
            {
                entity.ToTable("sif_Funkcija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SifJedMjere>(entity =>
            {
                entity.ToTable("sif_jedMjere");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifKategorijeObavijesti>(entity =>
            {
                entity.ToTable("sif_KategorijeObavijesti");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifStrucnaSprema>(entity =>
            {
                entity.ToTable("sif_StrucnaSprema");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SifTipKontaktum>(entity =>
            {
                entity.ToTable("sif_tipKontakta");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SifValutum>(entity =>
            {
                entity.HasKey(e => e.Iso)
                    .HasName("PK__sif_valu__C4979A22106B2F7B");

                entity.ToTable("sif_valuta");

                entity.Property(e => e.Iso)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("ISO")
                    .IsFixedLength(true);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifVrstaDokumentum>(entity =>
            {
                entity.ToTable("sif_vrstaDokumenta");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<SifVrstaKonzorcija>(entity =>
            {
                entity.ToTable("sif_VrstaKonzorcija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifVrstaPonuditelja>(entity =>
            {
                entity.ToTable("sif_vrstaPonuditelja");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SifVrstaPostupka>(entity =>
            {
                entity.ToTable("sif_vrstaPostupka");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SifVrstaUgovora>(entity =>
            {
                entity.ToTable("sif_VrstaUgovora");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SifZakon>(entity =>
            {
                entity.ToTable("sif_Zakon");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<StavkaNabave>(entity =>
            {
                entity.ToTable("StavkaNabave");

                entity.Property(e => e.FkCpv)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FK_CPV");

                entity.Property(e => e.FkIdPlanNabave).HasColumnName("FK_IdPlanNabave");

                entity.Property(e => e.FkIdVrstaPostupka).HasColumnName("FK_IdVrstaPostupka");

                entity.Property(e => e.FkIso)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("FK_ISO")
                    .IsFixedLength(true);

                entity.Property(e => e.PredmetNabave)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.VrijediDo).HasColumnType("date");

                entity.Property(e => e.VrijediOd).HasColumnType("date");

                entity.HasOne(d => d.FkCpvNavigation)
                    .WithMany(p => p.StavkaNabaves)
                    .HasForeignKey(d => d.FkCpv)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaNab__FK_CP__4F47C5E3");

                entity.HasOne(d => d.FkIdPlanNabaveNavigation)
                    .WithMany(p => p.StavkaNabaves)
                    .HasForeignKey(d => d.FkIdPlanNabave)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaNab__FK_Id__4E53A1AA");

                entity.HasOne(d => d.FkIdVrstaPostupkaNavigation)
                    .WithMany(p => p.StavkaNabaves)
                    .HasForeignKey(d => d.FkIdVrstaPostupka)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaNab__FK_Id__51300E55");

                entity.HasOne(d => d.FkIsoNavigation)
                    .WithMany(p => p.StavkaNabaves)
                    .HasForeignKey(d => d.FkIso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaNab__FK_IS__503BEA1C");
            });

            modelBuilder.Entity<StavkaTroskovnika>(entity =>
            {
                entity.ToTable("StavkaTroskovnika");

                entity.Property(e => e.FkJedMjere).HasColumnName("FK_jedMjere");

                entity.Property(e => e.FkTroskovnikNatjecaj).HasColumnName("FK_TroskovnikNatjecaj");

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkJedMjereNavigation)
                    .WithMany(p => p.StavkaTroskovnikas)
                    .HasForeignKey(d => d.FkJedMjere)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaTro__FK_je__078C1F06");

                entity.HasOne(d => d.FkTroskovnikNatjecajNavigation)
                    .WithMany(p => p.StavkaTroskovnikas)
                    .HasForeignKey(d => d.FkTroskovnikNatjecaj)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__StavkaTro__FK_Tr__0880433F");
            });

            modelBuilder.Entity<TroskovnikNatjecaj>(entity =>
            {
                entity.ToTable("TroskovnikNatjecaj");

                entity.Property(e => e.FkNatjecajEvBr)
                    .HasMaxLength(30)
                    .HasColumnName("FK_NatjecajEvBr");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkNatjecajEvBrNavigation)
                    .WithMany(p => p.TroskovnikNatjecajs)
                    .HasForeignKey(d => d.FkNatjecajEvBr)
                    .HasConstraintName("FK__Troskovni__FK_Na__01D345B0");
            });

            modelBuilder.Entity<TroskovnikPonuda>(entity =>
            {
                entity.Property(e => e.FkPonudaId).HasColumnName("FK_PonudaId");

                entity.Property(e => e.Naslov)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Opis).HasMaxLength(100);

                entity.Property(e => e.UkCijena).HasColumnName("ukCijena");

                entity.HasOne(d => d.FkPonuda)
                    .WithMany(p => p.TroskovnikPonuda)
                    .HasForeignKey(d => d.FkPonudaId)
                    .HasConstraintName("FK__Troskovni__FK_Po__04AFB25B");
            });

            modelBuilder.Entity<Tvrtka>(entity =>
            {
                entity.HasKey(e => e.Oib)
                    .HasName("PK__Tvrtka__CB394B3F0742962F");

                entity.ToTable("Tvrtka");

                entity.Property(e => e.Oib)
                    .HasMaxLength(50)
                    .HasColumnName("OIB");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FkMjesto).HasColumnName("FK_Mjesto");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.FkMjestoNavigation)
                    .WithMany(p => p.Tvrtkas)
                    .HasForeignKey(d => d.FkMjesto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Tvrtka__FK_Mjest__1F98B2C1");
            });

            modelBuilder.Entity<TvrtkaKontakti>(entity =>
            {
                entity.HasKey(e => new { e.Oib, e.Id })
                    .HasName("PK__TvrtkaKo__A81805FFB5DE81F8");

                entity.ToTable("TvrtkaKontakti");

                entity.Property(e => e.Oib)
                    .HasMaxLength(50)
                    .HasColumnName("OIB");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.TvrtkaKontaktis)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TvrtkaKontak__Id__42E1EEFE");

                entity.HasOne(d => d.OibNavigation)
                    .WithMany(p => p.TvrtkaKontaktis)
                    .HasForeignKey(d => d.Oib)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TvrtkaKonta__OIB__41EDCAC5");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
