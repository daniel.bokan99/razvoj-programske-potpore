﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Model {
    public class AddTvrtkaDTO {

        [Required(ErrorMessage = "Polje je obavezno")]
        [MaxLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [MinLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [DisplayName("OIB")]
        public string OIB { get; set; }
        [Required(ErrorMessage = "Polje je obavezno")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Polje je obavezno")]
        [DisplayName("Mjesto")]
        public int PostanskiBroj { get; set; }
        [Required(ErrorMessage = "Polje je obavezno")]
        public string Adresa { get; set; }
        [Required(ErrorMessage = "Polje je obavezno")]
        [DisplayName("Djelatnost")]
        public int idDjelatnost { get; set; }

    }
}
