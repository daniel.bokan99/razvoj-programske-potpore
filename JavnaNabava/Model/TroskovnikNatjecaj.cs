﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class TroskovnikNatjecaj
    {
        public TroskovnikNatjecaj()
        {
            StavkaTroskovnikas = new HashSet<StavkaTroskovnika>();
        }

        public int Id { get; set; }
        [Display(Name = "Naslov", Prompt = "Unesite naslov")]
        [Required(ErrorMessage = "Naslov je obvezno polje")]
        public string Naslov { get; set; }
        [Display(Name = "Opis", Prompt = "Unesite opis")]
        [Required(ErrorMessage = "Opis je obvezno polje")]
        public string Opis { get; set; }
        public string FkNatjecajEvBr { get; set; }

        public virtual Natjecaj FkNatjecajEvBrNavigation { get; set; }
        public virtual ICollection<StavkaTroskovnika> StavkaTroskovnikas { get; set; }
    }
}
