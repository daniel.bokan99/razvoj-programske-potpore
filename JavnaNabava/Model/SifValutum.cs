﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifValutum
    {
        public SifValutum()
        {
            StavkaNabaves = new HashSet<StavkaNabave>();
        }

        public string Iso { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<StavkaNabave> StavkaNabaves { get; set; }
    }
}
