﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class TroskovnikPonudum
    {
        public TroskovnikPonudum()
        {
            PonudaStavkaTroskovniks = new HashSet<PonudaStavkaTroskovnik>();
        }

        public int Id { get; set; }
        public string Naslov { get; set; }
        public string Opis { get; set; }
        public double UkCijena { get; set; }
        public int? FkPonudaId { get; set; }

        public virtual Ponudum FkPonuda { get; set; }
        public virtual ICollection<PonudaStavkaTroskovnik> PonudaStavkaTroskovniks { get; set; }
    }
}
