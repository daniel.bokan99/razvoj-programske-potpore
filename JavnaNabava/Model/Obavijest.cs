﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Obavijest
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Naslov je obavezan")]
        public string Naslov { get; set; }
        [Required(ErrorMessage = "Url je obavezan")]
        public string Url { get; set; }
        [Required(ErrorMessage = "Naziv kategorije je obavezna")]
        [DisplayName("Naziv kategorije")]
        public int IdKategorije { get; set; }

        public virtual SifKategorijeObavijesti IdKategorijeNavigation { get; set; }
    }
}
