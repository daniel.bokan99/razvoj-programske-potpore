﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifKategorijeObavijesti
    {
        public SifKategorijeObavijesti()
        {
            Obavijests = new HashSet<Obavijest>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Polje je obavezno")]
        [MaxLength(30, ErrorMessage = "Naziv ne smije biti duži od 30 znakova")]
        public string Naziv { get; set; }

        public virtual ICollection<Obavijest> Obavijests { get; set; }
    }
}
