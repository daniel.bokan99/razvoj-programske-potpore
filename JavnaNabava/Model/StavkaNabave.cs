﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class StavkaNabave
    {
        public StavkaNabave()
        {
            Natjecajs = new HashSet<Natjecaj>();
        }

        public int Id { get; set; }
       
        [Display(Name = "Evidencijski broj", Prompt = "Unesite evidencijski broj")]
        [Required(ErrorMessage = "Evidencijski broj je obvezno polje")]
        public int EvidencijskiBroj { get; set; }

        [Display(Name = "Predmet nabave", Prompt = "Unesite predmet nabave")]
        [Required(ErrorMessage = "Predmet nabave je obvezno polje")]
        public string PredmetNabave { get; set; }

        [Display(Name = "Procjenjena vrijednost", Prompt = "Unesite procjenjenu vrijednost")]
        [Required(ErrorMessage = "Procjenjena vrijednost je obvezno polje")]
        [Range(0, int.MaxValue, ErrorMessage ="Vrijednost mora biti pozitivan broj.")]
        public int ProcjenjenaVrijednost { get; set; }

        [Display(Name = "Vrijedi od", Prompt = "Unesite datum")]
        [Required(ErrorMessage = "Datum vrijedi od je obvezno polje")]
        public DateTime VrijediOd { get; set; }

        [Display(Name = "Vrijedi do", Prompt = "Unesite datum")]
        [Required(ErrorMessage = "Datum vrijedi do je obvezno polje")]
        //[mora biti nakon datuma vrijedi Od]
        public DateTime VrijediDo { get; set; }

        [Display(Name = "Plan nabave", Prompt = "Odaberite plan nabave")]
        [Required(ErrorMessage = "Plan nabave je obvezno polje")]
        public int FkIdPlanNabave { get; set; }

        [Display(Name = "CPV", Prompt = "Odaberite CPV")]
        [Required(ErrorMessage = "CPV je obvezno polje")]
        public string FkCpv { get; set; }

        [Display(Name = "Valuta", Prompt = "Odaberite valutu")]
        [Required(ErrorMessage = "Valuta je obvezno polje")]
        public string FkIso { get; set; }

        [Display(Name = "Vrsta postupka", Prompt = "Odaberite vrstu postupka")]
        [Required(ErrorMessage = "Vrsta postupka je obvezno polje")]
        public int FkIdVrstaPostupka { get; set; }

        public virtual SifCpv FkCpvNavigation { get; set; }
        public virtual PlanNabave FkIdPlanNabaveNavigation { get; set; }
        public virtual SifVrstaPostupka FkIdVrstaPostupkaNavigation { get; set; }
        public virtual SifValutum FkIsoNavigation { get; set; }
        public virtual ICollection<Natjecaj> Natjecajs { get; set; }
    }
}
