﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifVrstaPostupka
    {
        public SifVrstaPostupka()
        {
            StavkaNabaves = new HashSet<StavkaNabave>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<StavkaNabave> StavkaNabaves { get; set; }
    }
}
