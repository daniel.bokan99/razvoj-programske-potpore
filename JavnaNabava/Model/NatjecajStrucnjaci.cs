﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class NatjecajStrucnjaci
    {
        public NatjecajStrucnjaci()
        {
            PonudaStrucnjaks = new HashSet<PonudaStrucnjak>();
        }

        public int Id { get; set; }
        public string OpisKriterija { get; set; }
        public int BrPotrebnihStrucnjaka { get; set; }
        public string EvidencijskiBr { get; set; }
        public int IdStrucnaSprema { get; set; }

        public virtual Natjecaj EvidencijskiBrNavigation { get; set; }
        public virtual SifStrucnaSprema IdStrucnaSpremaNavigation { get; set; }
        public virtual ICollection<PonudaStrucnjak> PonudaStrucnjaks { get; set; }
    }
}
