﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class OsobaKontakt
    {
        public string FkOsobaOib { get; set; }
        public int FkKontaktId { get; set; }

        public virtual Kontakti FkKontakt { get; set; }
        public virtual Osoba FkOsobaOibNavigation { get; set; }
    }
}
