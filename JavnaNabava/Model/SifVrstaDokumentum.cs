﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifVrstaDokumentum
    {
        public SifVrstaDokumentum()
        {
            Dokuments = new HashSet<Dokument>();
        }

        public int Id { get; set; }
        [Display(Name = "Naziv", Prompt = "Unesite naziv dokumenta")]
        [Required(ErrorMessage = "Naziv je obavezno polje")]
        [MaxLength(25, ErrorMessage = "Naziv može sadržavati maksimalno 25 znakova")]
        public string Naziv { get; set; }

        public virtual ICollection<Dokument> Dokuments { get; set; }
    }
}
