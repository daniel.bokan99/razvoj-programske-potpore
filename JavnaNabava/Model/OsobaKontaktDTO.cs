﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class OsobaKontaktDTO
    {
        [Required,
            Display(Name = "Osoba", Prompt = "Odaberite osobu")]
        public string FkOsobaOib { get; set; }
        public virtual Kontakti FkKontakt { get; set; }
        public virtual Osoba FkOsobaOibNavigation { get; set; }
    }
}
