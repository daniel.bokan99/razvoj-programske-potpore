﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Dokument
    {
        [Display(Name = "Urudžbeni br.", Prompt = "Unesite urudžbeni broj")]
        [Required(ErrorMessage = "Urudžbeni broj je obavezno polje")]
        [MaxLength(10, ErrorMessage = "Urudžbeni broj može sadržavati maksimalno 10 znakova")]

        public string UrudzbeniBr { get; set; }

        [Display(Name = "Datum dokumenta")]
        [Required(ErrorMessage = "Datum Dokumenta je obavezno polje")]
        public DateTime DatumDokumenta { get; set; }


        [Required(ErrorMessage = "Naslov je obavezno polje")]
        [MaxLength(25, ErrorMessage = "Naslov može sadržavati maksimalno 25 znakova")]
        public string Naslov { get; set; }
        [Display(Name = "Dokument", Prompt = "Prenesite dokument")]
        public string FileBinary { get; set; }

        [Display(Name = "Povezana ponuda")]
        public int? FkPonudaId { get; set; }
        [Display(Name = "Povezan stručnjak")]
        public int? FkPonudaStrucnjakId { get; set; }
        [Display(Name = "Povezan natječaj")]
        public string FkNatjecajDokumentEvBr { get; set; }
        [Required(ErrorMessage = "Vrsta dokumenta je obavezno polje")]
        [Display(Name = "Vrsta dokumenta")]
        public int FkVrstaDokumentaId { get; set; }

        public virtual Natjecaj FkNatjecajDokumentEvBrNavigation { get; set; }
        public virtual Ponudum FkPonuda { get; set; }
        public virtual PonudaStrucnjak FkPonudaStrucnjak { get; set; }
        public virtual SifVrstaDokumentum FkVrstaDokumenta { get; set; }
    }
}
