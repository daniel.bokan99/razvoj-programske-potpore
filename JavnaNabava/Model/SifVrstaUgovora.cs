﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifVrstaUgovora
    {
        public SifVrstaUgovora()
        {
            Natjecajs = new HashSet<Natjecaj>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Naziv je obvezno polje")]
        [Display(Name = "Naziv vrste ugovora", Prompt = "Unesite naziv vrste ugovora")]
        public string Naziv { get; set; }

        public virtual ICollection<Natjecaj> Natjecajs { get; set; }
    }
}
