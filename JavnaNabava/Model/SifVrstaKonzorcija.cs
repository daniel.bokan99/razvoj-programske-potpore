﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifVrstaKonzorcija
    {
        public SifVrstaKonzorcija()
        {
            Konzorcijs = new HashSet<Konzorcij>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }

        public virtual ICollection<Konzorcij> Konzorcijs { get; set; }
    }
}
