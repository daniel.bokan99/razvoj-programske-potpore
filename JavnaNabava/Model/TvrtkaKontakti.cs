﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class TvrtkaKontakti
    {
        public string Oib { get; set; }
        public int Id { get; set; }

        public virtual Kontakti IdNavigation { get; set; }
        public virtual Tvrtka OibNavigation { get; set; }
    }
}
