﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Natjecaj
    {
        public Natjecaj()
        {
            Dokuments = new HashSet<Dokument>();
            NatjecajKriterijOcjenjivanjas = new HashSet<NatjecajKriterijOcjenjivanja>();
            NatjecajStrucnjacis = new HashSet<NatjecajStrucnjaci>();
            TroskovnikNatjecajs = new HashSet<TroskovnikNatjecaj>();
        }

        public string EvidencijskiBr { get; set; }
        [Required(ErrorMessage = "Naziv je obvezno polje")]
        [Display(Name = "Naziv natječaja", Prompt = "Unesite naziv natječaja")]
        public string Naziv { get; set; }

        public int IdStavkaNabave { get; set; }

        [Required(ErrorMessage = "Rok za dostavu je obvezno polje")]
        [Display(Name = "Rok za dostavu", Prompt = "Unesite rok za dostavu")]
        public DateTime RokZaDostavu { get; set; }

        [Required(ErrorMessage = "Trajanje ugovora je obvezno polje")]
        [Display(Name = "Trajanje ugovora", Prompt = "Unesite trajanje ugovora")]
        public string TrajanjeUgovora { get; set; }

        public int IdVrstaUgovora { get; set; }

        public int SlanjeElPostom { get; set; }

        [Required(ErrorMessage = "Popis potrebnih dokumenata je obvezno polje")]
        [Display(Name = "Popis potrebnih dokumenata", Prompt = "Unesite popis potrebnih dokumenata")]
        public string PopisPotrebnihDokumenata { get; set; }

        public int IdDjelatnost { get; set; }

        public int JePonisten { get; set; }

        public virtual SifDjelatnostTvrtke IdDjelatnostNavigation { get; set; }
        public virtual StavkaNabave IdStavkaNabaveNavigation { get; set; }
        public virtual SifVrstaUgovora IdVrstaUgovoraNavigation { get; set; }
        public virtual ICollection<Dokument> Dokuments { get; set; }
        public virtual ICollection<NatjecajKriterijOcjenjivanja> NatjecajKriterijOcjenjivanjas { get; set; }
        public virtual ICollection<NatjecajStrucnjaci> NatjecajStrucnjacis { get; set; }
        public virtual ICollection<TroskovnikNatjecaj> TroskovnikNatjecajs { get; set; }
    }
}
