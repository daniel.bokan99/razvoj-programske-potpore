﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifTipKontaktum
    {
        public SifTipKontaktum()
        {
            Kontaktis = new HashSet<Kontakti>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Polje Naziv je obavezno"),
            Display(Name = "Naziv")]
        public string Naziv { get; set; }

        public virtual ICollection<Kontakti> Kontaktis { get; set; }
    }
}
