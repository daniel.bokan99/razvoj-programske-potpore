﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class JeDjelatnosti
    {
        public string Oib { get; set; }
        public int Id { get; set; }

        public virtual SifDjelatnostTvrtke IdNavigation { get; set; }
        public virtual Tvrtka OibNavigation { get; set; }
    }
}
