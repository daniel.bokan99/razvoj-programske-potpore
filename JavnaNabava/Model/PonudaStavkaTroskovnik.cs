﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class PonudaStavkaTroskovnik
    {
        public int Id { get; set; }
        public double JedinicnaCijena { get; set; }
        public double UkupnaCijenaBezPdv { get; set; }
        public double Pdv { get; set; }
        public double UkupnaCijenaSaPdv { get; set; }
        public int FkTroskovnikPonuda { get; set; }
        public int FkNatjecajStavkaTroskovnik { get; set; }

        public virtual StavkaTroskovnika FkNatjecajStavkaTroskovnikNavigation { get; set; }
        public virtual TroskovnikPonuda FkTroskovnikPonudaNavigation { get; set; }
    }
}
