﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Mjesto
    {
        public Mjesto()
        {
            Konzorcijs = new HashSet<Konzorcij>();
            Osobas = new HashSet<Osoba>();
            Tvrtkas = new HashSet<Tvrtka>();
        }

        [Required(ErrorMessage = "Polje Poštanski broj je obavezno"),
            Range(10, 100000, ErrorMessage = "Raspon može biti od 10 do 100 000"),
            Display(Name = "Poštanski broj", Prompt = "Unesite poštanski broj")]
        public int PostanskiBroj { get; set; }

        [Required(ErrorMessage = "Polje Naziv je obavezno"),
            Display(Name = "Naziv", Prompt = "Unesite naziv mjesta")]
        public string Naziv { get; set; }

        [Required(ErrorMessage = "Polje Država je obavezno"),
            Display(Name = "Država", Prompt = "Odaberite državu")]
        public string FkDrzavaIso { get; set; }

        public virtual Drzava FkDrzavaIsoNavigation { get; set; }
        public virtual ICollection<Konzorcij> Konzorcijs { get; set; }
        public virtual ICollection<Osoba> Osobas { get; set; }
        public virtual ICollection<Tvrtka> Tvrtkas { get; set; }
    }
}
