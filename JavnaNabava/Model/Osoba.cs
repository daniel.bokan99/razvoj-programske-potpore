﻿using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Osoba
    {
        public Osoba()
        {
            OsobaKontakts = new HashSet<OsobaKontakt>();
            PonudaStrucnjaks = new HashSet<PonudaStrucnjak>();
        }

        [Required(ErrorMessage = "Polje OIB je obavezno"),
            MinLength(11, ErrorMessage = "OIB mora imati 11 znakova"),
            MaxLength(11, ErrorMessage = "OIB mora imati 11 znakova"),
            Display(Name = "OIB", Prompt = "Unesite OIB")]
        public string Oib { get; set; }

        [Required(ErrorMessage = "Polje Ime je obavezno"),
            Display(Name = "Ime", Prompt = "Unesite ime osobe")]
        public string Ime { get; set; }

        [Required(ErrorMessage = "Polje Prezime je obavezno"),
            Display(Name = "Prezime", Prompt = "Unesite prezime osobe")]
        public string Prezime { get; set; }

        [Required(ErrorMessage = "Polje Tvrtka je obavezno"),
            Display(Name = "Tvrtka", Prompt = "Odaberite tvrtku osobe")]
        public string FkTvrtka { get; set; }

        [Required,
            Range(1, Int32.MaxValue, ErrorMessage = "Polje Funkcija je obavezno"),
            Display(Name = "Funkcija", Prompt = "Odaberite funkciju osobe")]
        public int FkFunkcijaId { get; set; }

        [Required(ErrorMessage = "Polje Adresa je obavezno"),
            Display(Name = "Adresa", Prompt = "Unesite adresu osobe")]
        public string Adresa { get; set; }

        [Required,
            Range(1, Int32.MaxValue, ErrorMessage = "Polje Stručna sprema je obavezno"),
            Display(Name = "Stručna sprema", Prompt = "Odaberite stručnu spremu osobe")]
        public int FkStrucnaSprema { get; set; }

        [Required,
            Range(1, Int32.MaxValue, ErrorMessage = "Polje Mjesto je obavezno"),
            Display(Name = "Mjesto", Prompt = "Odaberite mjesto osobe")]
        public int FkMjesto { get; set; }

        public virtual SifFunkcija FkFunkcija { get; set; }
        public virtual Mjesto FkMjestoNavigation { get; set; }
        public virtual SifStrucnaSprema FkStrucnaSpremaNavigation { get; set; }
        public virtual Tvrtka FkTvrtkaNavigation { get; set; }
        public virtual ICollection<OsobaKontakt> OsobaKontakts { get; set; }
        public virtual ICollection<PonudaStrucnjak> PonudaStrucnjaks { get; set; }

        //public IEnumerable<OsobaKontaktViewModel> OsobaKontakti { get; set; }

        public string PodatciOsobe => Oib + ", " + Ime + ", " + Prezime;
    }
}
