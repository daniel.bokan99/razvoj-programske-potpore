﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Model {
    public class KonzorcijDetailsDTO {

        public Konzorcij konzorcij { get; set; }

        public Tvrtka tvrtka { get; set; }

        public KonzorcijTvrtka konzorcijTvrtka { get; set; }

        public Mjesto mjesto { get; set; }

        public SifDjelatnostTvrtke djelatnost { get; set; }

    }
}
