﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class PonudaOcjenaKriterija
    {
        public int Id { get; set; }
        [Display(Name = "Ostvareni bodovi", Prompt = "Unesite bodove")]
        [Required(ErrorMessage = "Ostvareni bodovi su obavezno polje")]
        [Range(0, 100, ErrorMessage = "Maksimalni iznos bodova je 100")]

        public int OstvareniBodovi { get; set; }
        [Display(Name = "Povezani natječaj", Prompt = "odaberite natječaj")]
        [Required(ErrorMessage = "Natječaj je obavezno polje")]
        public int FkNatjecajKriterijOcjenjivanjaId { get; set; }
        [Display(Name = "Povezana ponuda", Prompt = "odaberite ponudu")]
        [Required(ErrorMessage = "Ponuda je obavezno polje")]
        public int FkPonudaId { get; set; }

        public virtual NatjecajKriterijOcjenjivanja FkNatjecajKriterijOcjenjivanja { get; set; }
        public virtual Ponudum FkPonuda { get; set; }
    }
}
