﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Ponudum
    {
        public Ponudum()
        {
            Dokuments = new HashSet<Dokument>();
            PonudaOcjenaKriterijas = new HashSet<PonudaOcjenaKriterija>();
            PonudaStrucnjaks = new HashSet<PonudaStrucnjak>();
            TroskovnikPonuda = new HashSet<TroskovnikPonuda>();
        }

        public int Id { get; set; }
        [Display(Name = "Ukupna cijena ponuda", Prompt = "Unesite cijenu")]
        [Required(ErrorMessage = "Ukupna cijena je obavezno polje")]
        [Range(0, Double.MaxValue, ErrorMessage = "Ukupna cijena ponude ne može biti manja od 0")]
        public double UkupnaCijenaPonude { get; set; }

        [Display(Name = "Vrsta ponuditelja", Prompt = "Odaberite vrstu")]
        [Required(ErrorMessage = "Vrsta je obavezno polje")]
        public int FkVrstaPonuditeljaId { get; set; }
        [Display(Name = "Povezana tvrtka", Prompt = "Odaberite tvrtku")]
        public string FkTvrtkaOib { get; set; }
        [Display(Name = "Povezan konzorcij", Prompt = "Odaberite konzorcij")]
        public string FkKonzorcijOib { get; set; }

        public virtual Konzorcij FkKonzorcijOibNavigation { get; set; }
        public virtual Tvrtka FkTvrtkaOibNavigation { get; set; }
        public virtual SifVrstaPonuditelja FkVrstaPonuditelja { get; set; }
        public virtual ICollection<Dokument> Dokuments { get; set; }
        public virtual ICollection<PonudaOcjenaKriterija> PonudaOcjenaKriterijas { get; set; }
        public virtual ICollection<PonudaStrucnjak> PonudaStrucnjaks { get; set; }
        public virtual ICollection<TroskovnikPonuda> TroskovnikPonuda { get; set; }
    }
}
