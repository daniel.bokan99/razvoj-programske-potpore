﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class KonzorcijKontakti
    {
        public int FkKontaktId { get; set; }
        public string FkKonzorcijOib { get; set; }

        public virtual Kontakti FkKontakt { get; set; }
        public virtual Konzorcij FkKonzorcijOibNavigation { get; set; }
    }
}
