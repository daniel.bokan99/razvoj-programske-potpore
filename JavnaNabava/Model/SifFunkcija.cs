﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifFunkcija
    {
        public SifFunkcija()
        {
            Osobas = new HashSet<Osoba>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Polje Naziv je obavezno"),
            Display(Name = "Naziv")]
        public string Naziv { get; set; }

        public virtual ICollection<Osoba> Osobas { get; set; }
    }
}
