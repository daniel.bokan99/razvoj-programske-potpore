﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class SifJedMjere
    {
        public SifJedMjere()
        {
            StavkaTroskovnikas = new HashSet<StavkaTroskovnika>();
        }

        public string Naziv { get; set; }
        public int Id { get; set; }

        public virtual ICollection<StavkaTroskovnika> StavkaTroskovnikas { get; set; }
    }
}
