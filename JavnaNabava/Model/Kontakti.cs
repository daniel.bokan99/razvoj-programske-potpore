﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Kontakti
    {
        public Kontakti()
        {
            KonzorcijKontaktis = new HashSet<KonzorcijKontakti>();
            OsobaKontakts = new HashSet<OsobaKontakt>();
            TvrtkaKontaktis = new HashSet<TvrtkaKontakti>();
        }

        [Required,
            Range(1, Int32.MaxValue, ErrorMessage = "Polje Tip kontakta")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Polje Kontakt podatci je obavezno"),
            Display(Name = "Kontakt podatci", Prompt = "Unesite kontakt")]
        public string KontaktPodatci { get; set; }

        [Display(Name = "Tip kontakta", Prompt = "Odaberite tip kontakta")]
        public int FkSifKontaktaId { get; set; }

        public virtual SifTipKontaktum FkSifKontakta { get; set; }
        public virtual ICollection<KonzorcijKontakti> KonzorcijKontaktis { get; set; }
        public virtual ICollection<OsobaKontakt> OsobaKontakts { get; set; }
        public virtual ICollection<TvrtkaKontakti> TvrtkaKontaktis { get; set; }
    }
}
