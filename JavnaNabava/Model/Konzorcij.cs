﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class Konzorcij
    {
        public Konzorcij()
        {
            KonzorcijKontaktis = new HashSet<KonzorcijKontakti>();
            KonzorcijTvrtkas = new HashSet<KonzorcijTvrtka>();
            Ponuda = new HashSet<Ponudum>();
        }

        [Required(ErrorMessage = "OIB polje je obavezno")]
        [MaxLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [MinLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [DisplayName("OIB")]
        public string Oib { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Adresa sjedišta je obavezna")]
        public string AdresaSjedista { get; set; }
        [Required(ErrorMessage = "Mjesto je obavezno")]
        [DisplayName("Mjesto")]
        public int FkMjesto { get; set; }
        [Required]
        [DisplayName("Vrsta Konzorcija")]
        public int FkVrstaKonzorcija { get; set; }

        public virtual Mjesto FkMjestoNavigation { get; set; }
        public virtual SifVrstaKonzorcija FkVrstaKonzorcijaNavigation { get; set; }
        public virtual ICollection<KonzorcijKontakti> KonzorcijKontaktis { get; set; }
        public virtual ICollection<KonzorcijTvrtka> KonzorcijTvrtkas { get; set; }
        public virtual ICollection<Ponudum> Ponuda { get; set; }
    }
}
