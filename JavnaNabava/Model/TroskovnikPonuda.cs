﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class TroskovnikPonuda
    {
        public TroskovnikPonuda()
        {
            PonudaStavkaTroskovniks = new HashSet<PonudaStavkaTroskovnik>();
        }

        public int Id { get; set; }
        [Display(Name = "Naslov", Prompt = "Unesite naslov")]
        [Required(ErrorMessage = "Naslov je obvezno polje")]
        public string Naslov { get; set; }
        [Display(Name = "Opis", Prompt = "Unesite opis")]
        [Required(ErrorMessage = "Opis je obvezno polje")]
        public string Opis { get; set; }
        [Display(Name = "Ukupna cijena", Prompt = "Unesite ukupnu cijenu")]
        [Required(ErrorMessage = "Ukupna cijena je obvezno polje")]
        public double UkCijena { get; set; }
        public int? FkPonudaId { get; set; }

        public virtual Ponudum FkPonuda { get; set; }
        public virtual ICollection<PonudaStavkaTroskovnik> PonudaStavkaTroskovniks { get; set; }
    }
}
