﻿using System;
using System.Collections.Generic;

#nullable disable

namespace JavnaNabava.Model
{
    public partial class NatjecajKriterijOcjenjivanja
    {
        public NatjecajKriterijOcjenjivanja()
        {
            PonudaOcjenaKriterijas = new HashSet<PonudaOcjenaKriterija>();
        }

        public int Id { get; set; }
        public string Opis { get; set; }
        public int Ponder { get; set; }
        public string EvidencijskiBr { get; set; }

        public virtual Natjecaj EvidencijskiBrNavigation { get; set; }
        public virtual ICollection<PonudaOcjenaKriterija> PonudaOcjenaKriterijas { get; set; }
    }
}
