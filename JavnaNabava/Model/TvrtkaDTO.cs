﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Model {
    public class TvrtkaDTO {

        public Tvrtka tvrtka { get; set; }

        public Mjesto mjesto { get; set; }

        public SifDjelatnostTvrtke djelatnost { get; set; }

    }
}
