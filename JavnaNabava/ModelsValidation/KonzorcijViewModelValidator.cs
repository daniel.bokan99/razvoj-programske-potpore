﻿using FluentValidation;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ModelsValidation {
    public class KonzorcijViewModelValidator : AbstractValidator<KonzorcijViewModel3>{

        public KonzorcijViewModelValidator() {

            RuleFor(m => m.Naziv).NotEmpty().WithMessage("Potrebno je unijeti naziv konzorcija");

        }

    }
}
