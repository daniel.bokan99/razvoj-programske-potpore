﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers
{

    /// <summary>
    /// Web API servis za rad s planovima nabave
    /// </summary>

    [ApiController]
    [Route("api/[controller]")]
    public class ApiPlanNabaveController : ControllerBase, ICustomController<int, PlanNabaveViewModel2>
    {

        private readonly RPPP17Context ctx;

        private static Dictionary<string, Expression<Func<PlanNabave, object>>> orderSelectors = new Dictionary<string, Expression<Func<PlanNabave, object>>>
        {
            [nameof(PlanNabaveViewModel2.Id).ToLower()] = m => m.Id,
            [nameof(PlanNabaveViewModel2.Naziv).ToLower()] = m => m.Naziv,
            [nameof(PlanNabaveViewModel2.NazivTvrtke).ToLower()] = m => m.FkTvrtkaOibNavigation.Naziv,
        };

        public ApiPlanNabaveController(RPPP17Context ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Vraća broj svih planova nabave filtriran prema id-u plana nabave
        /// </summary>
        /// <param name="filter">Opcionalni filter za naziv plana nabave</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojPlanovaNabave")]
        public async Task<int> Count([FromQuery] string filter)
        {

            var query = ctx.PlanNabave.Include(k => k.FkTvrtkaOibNavigation).AsQueryable();

            if (!string.IsNullOrWhiteSpace(filter))
            {
                query = query.Where(k => k.Naziv.Contains(filter));
            }

            int count = await query.CountAsync();

            return count;
        }

        /// <summary>
        /// Dohvat planova nabave (opcionalno filtrirano po nazivu plana nabave).
        /// Broj planova nabave, poredak, početna pozicija određeni s loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiPlanoveNabave")]
        public async Task<List<PlanNabaveViewModel2>> GetAll([FromQuery] LoadParams loadParams)
        {

            var query = ctx.PlanNabave.Include(k => k.FkTvrtkaOibNavigation).AsQueryable();
            if (!string.IsNullOrWhiteSpace(loadParams.Filter))
            {
                query = query.Where(k => k.Naziv.Contains(loadParams.Filter));
            }

            if (loadParams.SortColumn != null)
            {
                if (orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr))
                {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var list = await query.Select(k => new PlanNabaveViewModel2
            {
                Id = k.Id,
                Naziv = k.Naziv,
                NazivTvrtke = k.FkTvrtkaOibNavigation.Naziv
            })
                                   .Skip(loadParams.StartIndex)
                                   .Take(loadParams.Rows)
                                   .ToListAsync();

            return list;
        }

        /// <summary>
        /// Vraća plan nabave čiji je id jednak vrijednosti parametra id
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "DohvatiPlanNabave")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PlanNabaveViewModel2>> Get(int Id)
        {

            var planNabave = await ctx.PlanNabave
                                    .Include(k => k.FkTvrtkaOibNavigation)
                                    .Where(k => k.Id == Id)
                                    .Select(k => new PlanNabaveViewModel2
                                    {
                                        Id = k.Id,
                                        Naziv = k.Naziv,
                                        NazivTvrtke = k.FkTvrtkaOibNavigation.Naziv
                                    })
                                    .FirstOrDefaultAsync();

            if (planNabave == null)
            {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data for id = {Id}");
            }
            else
            {
                return planNabave;
            }
        }

        /// <summary>
        /// Brisanje plana nabave određenog s id
        /// </summary>
        /// <param name="Id">Vrijednost primarnog ključa (id plana nabave)</param>
        /// <returns></returns>
        /// <response code="204">Ako je plan nabave uspješno obrisan</response>
        /// <response code="404">Ako plan nabave s poslanim id-em ne postoji</response>      
        [HttpDelete("{Id}", Name = "ObrisiPlanNabave")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int Id)
        {

            var planNabave = await ctx.PlanNabave.FindAsync(Id);
            if (planNabave == null)
            {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid id = {Id}");
            }
            else
            {
                ctx.Remove(planNabave);
                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Ažurira plan nabave
        /// </summary>
        /// <param name="Id">parametar čija vrijednost jednoznačno identificira plan nabave</param>
        /// <param name="model">Podaci o planu nabave. Id se mora podudarati s parametrom id</param>
        /// <returns></returns>
        [HttpPut("{Id}", Name = "AzurirajPlanNabave")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int Id, PlanNabaveViewModel2 model)
        {

            if (model.Id != Id)
            {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Different Ids {Id} vs {model.Id}");
            }
            else
            {
                var planNabave = await ctx.PlanNabave.FindAsync(Id);
                if (planNabave == null)
                {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid id = {Id}");
                }

                planNabave.Naziv = model.Naziv;
                planNabave.FkTvrtkaOib = await ctx.Tvrtka.Where(m => m.Naziv == model.NazivTvrtke).Select(m => m.Oib).FirstOrDefaultAsync();
                await ctx.SaveChangesAsync();
                return NoContent();
            }

        }

        /// <summary>
        /// Stvara novi plan nabave opisan poslanim modelom
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "DodajPlanNabave")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(PlanNabaveViewModel2 model)
        {

            PlanNabave planNabave = new PlanNabave
            {
                Id = model.Id,
                Naziv = model.Naziv,
                FkTvrtkaOib = await ctx.Tvrtka.Where(m => m.Naziv == model.NazivTvrtke).Select(m => m.Oib).FirstOrDefaultAsync()
            };

            ctx.Add(planNabave);
            await ctx.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { Id = planNabave.Id }, planNabave);

        }

    }
}
