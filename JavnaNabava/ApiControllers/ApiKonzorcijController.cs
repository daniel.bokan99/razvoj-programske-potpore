﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers {

    /// <summary>
    /// Web API servis za rad s konzorcijima
    /// </summary>

    [ApiController]
    [Route("api/[controller]")]
    public class ApiKonzorcijController : ControllerBase, ICustomController<string, KonzorcijViewModel3> {

        private readonly RPPP17Context ctx;

        private static Dictionary<string, Expression<Func<Konzorcij, object>>> orderSelectors = new Dictionary<string, Expression<Func<Konzorcij, object>>> {
            [nameof(KonzorcijViewModel3.Oib).ToLower()] = m => m.Oib,
            [nameof(KonzorcijViewModel3.Naziv).ToLower()] = m => m.Naziv,
            [nameof(KonzorcijViewModel3.FkMjesto).ToLower()] = m => m.FkMjestoNavigation.Naziv,
            [nameof(KonzorcijViewModel3.FkVrstaKonzorcija).ToLower()] = m => m.FkVrstaKonzorcijaNavigation.Naziv,
            [nameof(KonzorcijViewModel3.AdresaSjedista).ToLower()] = m => m.AdresaSjedista,
        };

        public ApiKonzorcijController(RPPP17Context ctx) {
            this.ctx = ctx;
        }

        /// <summary>
        /// Vraća broj svih konzorcija filtriran prema nazivu konzorcija
        /// </summary>
        /// <param name="filter">Opcionalni filter za naziv konzorcija</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojKonzorcija")]
        public async Task<int> Count([FromQuery] string filter) {

            var query = ctx.Konzorcij.Include(k => k.FkVrstaKonzorcijaNavigation).Include(k => k.FkMjestoNavigation).AsQueryable();

            if (!string.IsNullOrWhiteSpace(filter)) {
                query = query.Where(k => k.Naziv.Contains(filter));
            }

            int count = await query.CountAsync();

            return count;
        }

        /// <summary>
        /// Dohvat konzorcija (opcionalno filtrirano po nazivu konzorcija).
        /// Broj konzorcija, poredak, početna pozicija određeni s loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiKonzorcije")]
        public async Task<List<KonzorcijViewModel3>> GetAll([FromQuery] LoadParams loadParams) {

            var query = ctx.Konzorcij.Include(k => k.FkVrstaKonzorcijaNavigation).Include(k => k.FkMjestoNavigation).AsQueryable();
            if (!string.IsNullOrWhiteSpace(loadParams.Filter)) {
                query = query.Where(k => k.Naziv.Contains(loadParams.Filter));
            }

            if(loadParams.SortColumn != null) {
                if(orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr)) {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var list = await query.Select(k => new KonzorcijViewModel3 {
                                        Oib = k.Oib,
                                        Naziv = k.Naziv,
                                        AdresaSjedista = k.AdresaSjedista,
                                        FkMjesto = k.FkMjestoNavigation.Naziv,
                                        FkVrstaKonzorcija = k.FkVrstaKonzorcijaNavigation.Naziv
                                    })
                                   .Skip(loadParams.StartIndex)
                                   .Take(loadParams.Rows)
                                   .ToListAsync();

            return list;
        }

        /// <summary>
        /// Vraća konzorcij čiji je Oib jednak vrijednosti parametra id
        /// </summary>
        /// <param name="Oib">Oib</param>
        /// <returns></returns>
        [HttpGet("{Oib}", Name = "DohvatiKonzorcij")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<KonzorcijViewModel3>> Get(string Oib) {

            var konzorij = await ctx.Konzorcij
                                    .Include(k => k.FkVrstaKonzorcijaNavigation)
                                    .Include(k => k.FkMjestoNavigation)
                                    .Where(k => k.Oib == Oib)
                                    .Select(k => new KonzorcijViewModel3 {
                                        Oib = k.Oib,
                                        Naziv = k.Naziv,
                                        AdresaSjedista = k.AdresaSjedista,
                                        FkMjesto = k.FkMjestoNavigation.Naziv,
                                        FkVrstaKonzorcija = k.FkVrstaKonzorcijaNavigation.Naziv
                                    })
                                    .FirstOrDefaultAsync();

            if(konzorij == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data for oib = {Oib}");
            } else {
                return konzorij;
            }
        }

        /// <summary>
        /// Brisanje konzorcija određenog s Oib
        /// </summary>
        /// <param name="Oib">Vrijednost primarnog ključa (Oib konzorcija)</param>
        /// <returns></returns>
        /// <response code="204">Ako je konzorcij uspješno obrisan</response>
        /// <response code="404">Ako konzorcij s poslanim Oib-om ne postoji</response>      
        [HttpDelete("{Oib}", Name = "ObrisiKonzorcij")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(string Oib) {

            var konzorcij = await ctx.Konzorcij.FindAsync(Oib);
            if(konzorcij == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid Oib = {Oib}");
            } else {
                ctx.Remove(konzorcij);
                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Ažurira konzorcij
        /// </summary>
        /// <param name="Oib">parametar čija vrijednost jednoznačno identificira konzorcij</param>
        /// <param name="model">Podaci o konzorciju. Oib se mora podudarati s parametrom Oib</param>
        /// <returns></returns>
        [HttpPut("{Oib}", Name = "AzurirajKonzorcij")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task <IActionResult> Update(string Oib, KonzorcijViewModel3 model) {

            if(model.Oib != Oib) {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Different Oibs {Oib} vs {model.Oib}");
            } else {
                var konzorcij = await ctx.Konzorcij.FindAsync(Oib);
                if(konzorcij == null) {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid Oib = {Oib}");
                }

                konzorcij.Naziv = model.Naziv;
                konzorcij.AdresaSjedista = model.AdresaSjedista;
                konzorcij.FkMjesto = await ctx.Mjesto.Where(m => m.Naziv == model.FkMjesto).Select(m => m.PostanskiBroj).FirstOrDefaultAsync();
                konzorcij.FkVrstaKonzorcija = await ctx.SifVrstaKonzorcija.Where(v => v.Naziv == model.FkVrstaKonzorcija).Select(m => m.Id).FirstOrDefaultAsync();

                await ctx.SaveChangesAsync();
                return NoContent();
            }

        }

        /// <summary>
        /// Stvara novi konzorcij opisan poslanim modelom
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "DodajKonzorcij")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(KonzorcijViewModel3 model) {

            Konzorcij konzorcij = new Konzorcij {
                Oib = model.Oib,
                Naziv = model.Naziv,
                AdresaSjedista = model.AdresaSjedista,
                FkMjesto = await ctx.Mjesto.Where(m => m.Naziv == model.FkMjesto).Select(m => m.PostanskiBroj).FirstOrDefaultAsync(),
                FkVrstaKonzorcija = await ctx.SifVrstaKonzorcija.Where(v => v.Naziv == model.FkVrstaKonzorcija).Select(m => m.Id).FirstOrDefaultAsync()
            };

            ctx.Add(konzorcij);
            await ctx.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { Oib = konzorcij.Oib }, konzorcij);

        }

    }
}
