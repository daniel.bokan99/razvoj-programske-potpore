﻿using JavnaNabava.ViewModels;
using JavnaNabava.ViewModels.JTable;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers.JTable {
    [Route("jtable/mjesto/[action]")]
    public class MjestoJTableController : JTableController<MjestoController, int, MjestoViewModel> {

        public MjestoJTableController(MjestoController controller) : base(controller) {

        }

        [HttpPost]
        public async Task<JTableAjaxResult> Update([FromForm] MjestoViewModel model) {
            return await base.UpdateItem(model.PostanskiBroj, model);
        }

        [HttpPost]
        public async Task<JTableAjaxResult> Delete([FromForm] int PostanskiBroj) {
            return await base.DeleteItem(PostanskiBroj);
        }

    }
}
