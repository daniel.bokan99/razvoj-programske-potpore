﻿using JavnaNabava.ViewModels;
using JavnaNabava.ViewModels.JTable;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers.JTable {
    [Route("jtable/djelatnost/[action]")]
    public class SifDjelatnostTvrtkeJTableController : JTableController<SifDjelatnostTvrtkeController, int, SifDjelatnostTvrtkeViewModel2> {

        public SifDjelatnostTvrtkeJTableController(SifDjelatnostTvrtkeController controller) : base(controller) {

        }

        [HttpPost]
        public async Task<JTableAjaxResult> Update([FromForm] SifDjelatnostTvrtkeViewModel2 model) {
            return await base.UpdateItem(model.Id, model);
        }

        [HttpPost]
        public async Task<JTableAjaxResult> Delete([FromForm] int Id) {
            return await base.DeleteItem(Id);
        }

    }
}
