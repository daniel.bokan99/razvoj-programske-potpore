﻿using JavnaNabava.ViewModels;
using JavnaNabava.ViewModels.JTable;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers.JTable {
    [Route("jtable/konzorcij/[action]")]
    public class KonzorcijJTableController : JTableController<ApiKonzorcijController, string, KonzorcijViewModel3> {

        public KonzorcijJTableController(ApiKonzorcijController controller) : base(controller) {

        }

        [HttpPost]
        public async Task<JTableAjaxResult> Update([FromForm] KonzorcijViewModel3 model) {
            return await base.UpdateItem(model.Oib, model);
        }

        [HttpPost]
        public async Task<JTableAjaxResult> Delete([FromForm] string Oib) {
            return await base.DeleteItem(Oib);
        }

    }
}
