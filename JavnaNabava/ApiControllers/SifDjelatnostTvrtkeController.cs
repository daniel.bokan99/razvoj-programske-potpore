﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers {

    /// <summary>
    /// Web API servis za rad s šifrarničkom tablicom djelatnosti tvrtki
    /// </summary>

    [ApiController]
    [Route("api/[controller]")]
    public class SifDjelatnostTvrtkeController : ControllerBase, ICustomController<int, SifDjelatnostTvrtkeViewModel2> {

        private readonly RPPP17Context ctx;

        private static Dictionary<string, Expression<Func<SifDjelatnostTvrtke, object>>> orderSelectors = new Dictionary<string, Expression<Func<SifDjelatnostTvrtke, object>>> {
            [nameof(SifDjelatnostTvrtkeViewModel2.Id).ToLower()] = d => d.Id,
            [nameof(SifDjelatnostTvrtkeViewModel2.Naziv).ToLower()] = d => d.Naziv,
        };

        public SifDjelatnostTvrtkeController(RPPP17Context ctx) {
            this.ctx = ctx;
        }

        /// <summary>
        /// Vraća broj svih djelatnosti filtriran prema nazivu djelatnosti
        /// </summary>
        /// <param name="filter">Opcionalni filter za naziv djelatnosti</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojDjelatnosti")]
        public async Task<int> Count([FromQuery] string filter) {

            var query = ctx.SifDjelatnostTvrtke.AsQueryable();

            if (!string.IsNullOrWhiteSpace(filter)) {
                query = query.Where(d => d.Naziv.Contains(filter));
            }

            int count = await query.CountAsync();

            return count;
        }

        /// <summary>
        /// Dohvat djelatnosti (opcionalno filtrirano po nazivu djelatnosti).
        /// Broj djelatnosti, poredak, početna pozicija određeni s loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiDjelatnosti")]
        public async Task<List<SifDjelatnostTvrtkeViewModel2>> GetAll([FromQuery] LoadParams loadParams) {

            var query = ctx.SifDjelatnostTvrtke.AsQueryable();

            if (!string.IsNullOrWhiteSpace(loadParams.Filter)) {
                query = query.Where(d => d.Naziv.Contains(loadParams.Filter));
            }

            if(loadParams.SortColumn != null) {
                if(orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr)) {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var list = await query.Select(d => new SifDjelatnostTvrtkeViewModel2{
                                        Id = d.Id,
                                        Naziv = d.Naziv,
                                    })
                                   .Skip(loadParams.StartIndex)
                                   .Take(loadParams.Rows)
                                   .ToListAsync();

            return list;
        }

        /// <summary>
        /// Vraća djelatnost čiji je Id jednak vrijednosti parametra id
        /// </summary>
        /// <param name="id">Oib</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "DohvatiDjelatnost")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<SifDjelatnostTvrtkeViewModel2>> Get(int id) {

            var djelatnost = await ctx.SifDjelatnostTvrtke
                                    .Where(d => d.Id == id)
                                    .Select(d => new SifDjelatnostTvrtkeViewModel2
                                    {
                                        Naziv = d.Naziv
                                    })
                                    .FirstOrDefaultAsync();

            if(djelatnost == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data for id = {id}");
            } else {
                return djelatnost;
            }
        }

        /// <summary>
        /// Brisanje djelatnosti određenog s Id
        /// </summary>
        /// <param name="id">Vrijednost primarnog ključa (Id djelatnosti)</param>
        /// <returns></returns>
        /// <response code="204">Ako je djelatnost uspješno obrisana</response>
        /// <response code="404">Ako djelatnost s poslanim Id-om ne postoji</response>      
        [HttpDelete("{id}", Name = "ObrisiDjelatnost")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id) {

            var djelatnost = await ctx.SifDjelatnostTvrtke.FindAsync(id);
            if(djelatnost == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid id = {id}");
            } else {
                ctx.Remove(djelatnost);
                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Ažurira djelatnost
        /// </summary>
        /// <param name="id">parametar čija vrijednost jednoznačno identificira djelatnost</param>
        /// <param name="model">Podaci o djelatnosti. Oib se mora podudarati s parametrom id</param>
        /// <returns></returns>
        [HttpPut("{id}", Name = "AzurirajDjelatnost")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task <IActionResult> Update(int id, SifDjelatnostTvrtkeViewModel2 model) {

            if(model.Id != id) {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Different ids {id} vs {model.Id}");
            } else {
                var djelatnost = await ctx.SifDjelatnostTvrtke.FindAsync(id);
                if(djelatnost == null) {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid id = {id}");
                }

                djelatnost.Naziv = model.Naziv;

                await ctx.SaveChangesAsync();
                return NoContent();
            }

        }

        /// <summary>
        /// Stvara novu djelatnost opisanu poslanim modelom
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "DodajDjelatnost")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(SifDjelatnostTvrtkeViewModel2 model) {
            

            SifDjelatnostTvrtke djelatnost = new SifDjelatnostTvrtke {
                Naziv = model.Naziv
            };

            ctx.Add(djelatnost);
            await ctx.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = djelatnost.Id }, djelatnost);

        }

    }
}
