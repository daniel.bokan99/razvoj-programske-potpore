﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers {

    /// <summary>
    /// Ponuda Web API servis
    /// </summary>

    [ApiController]
    [Route("api/[controller]")]
    public class PonudaController : ControllerBase, ICustomController<int, PonudaViewModel> {

        private readonly RPPP17Context ctx;

        private static Dictionary<string, Expression<Func<Ponudum, object>>> orderSelectors = new Dictionary<string, Expression<Func<Ponudum, object>>> {
            [nameof(PonudaViewModel.Id).ToLower()] = m => m.Id,
            [nameof(PonudaViewModel.ukupnaCijenaPonude).ToLower()] = m => m.UkupnaCijenaPonude,
            [nameof(PonudaViewModel.vrstaPonuditelja).ToLower()] = m => m.FkVrstaPonuditelja.Naziv,
            [nameof(PonudaViewModel.tvrtka).ToLower()] = m => m.FkTvrtkaOibNavigation.Naziv,
            [nameof(PonudaViewModel.konzorcij).ToLower()] = m => m.FkKonzorcijOibNavigation.Naziv,            
        };

        public PonudaController(RPPP17Context ctx) {
            this.ctx = ctx;
        }

        /// <summary>
        /// Vraća broj ponuda (opcionalno) filtriran prema max. cijeni ponude
        /// </summary>
        /// <param name="filter">Opcionalni filter prema max. cijeni ponude</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojPonuda")]
        public async Task<int> Count([FromQuery] string filter) {

            var query = ctx.Ponuda.Include(k => k.FkTvrtkaOibNavigation).Include(k => k.FkKonzorcijOibNavigation).Include(k => k.FkVrstaPonuditelja).AsQueryable();

            if (filter != null) {
                query = query.Where(k => k.UkupnaCijenaPonude <= double.Parse(filter));
            }

            int count = await query.CountAsync();

            return count;
        }

        /// <summary>
        /// Dohvat ponuda (opcionalno) filtriran prema max. cijeni ponude
        /// Broj prikazanih ponuda, preskok i poredak određeni u loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiSvePonude")]
        public async Task<List<PonudaViewModel>> GetAll([FromQuery] LoadParams loadParams) {

            var query = ctx.Ponuda.Include(k => k.FkTvrtkaOibNavigation).Include(k => k.FkKonzorcijOibNavigation).Include(k => k.FkVrstaPonuditelja).AsQueryable();
            if (!string.IsNullOrWhiteSpace(loadParams.Filter)) {
                query = query.Where(k => k.UkupnaCijenaPonude <= double.Parse(loadParams.Filter));
            }

            if(loadParams.SortColumn != null) {
                if(orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr)) {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var ponude = await query.Select(k => new PonudaViewModel {
                                        Id = k.Id,
                                        ukupnaCijenaPonude = k.UkupnaCijenaPonude,
                                        vrstaPonuditelja = k.FkVrstaPonuditelja.Naziv,
                                        tvrtka = k.FkTvrtkaOibNavigation.Naziv,
                                        konzorcij = k.FkKonzorcijOibNavigation.Naziv
                                    })
                                   .Skip(loadParams.StartIndex)
                                   .Take(loadParams.Rows)
                                   .ToListAsync();

            return ponude;
        }

        /// <summary>
        /// Vraća sve podatke ponude čiji id odgovara poslanom
        /// </summary>
        /// <param name="Id">Id - jedinstveni identifikator ponude</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "DohvatiPonudu")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PonudaViewModel>> Get(int Id) {

            var ponuda = await ctx.Ponuda
                                    .Include(k => k.FkTvrtkaOibNavigation)
                                    .Include(k => k.FkKonzorcijOibNavigation)
                                    .Include(k => k.FkVrstaPonuditelja)
                                    .Where(k => k.Id == Id)
                                    .Select(k => new PonudaViewModel {
                                        Id = k.Id,
                                        ukupnaCijenaPonude = k.UkupnaCijenaPonude,
                                        vrstaPonuditelja = k.FkVrstaPonuditelja.Naziv,
                                        tvrtka = k.FkTvrtkaOibNavigation.Naziv,
                                        konzorcij = k.FkKonzorcijOibNavigation.Naziv
                                    })
                                    .FirstOrDefaultAsync();

            if(ponuda == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data found");
            } else {
                return ponuda;
            }
        }

        /// <summary>
        /// Brisanje ponude čiji id odgovara poslanom
        /// </summary>
        /// <param name="Id">Id - jedinstveni identifikator ponude</param>
        /// <returns></returns>
        /// <response code="204">Uspješno brisanje</response>
        /// <response code="404">Ponuda pod s tim Id-em ne postoji</response>      
        [HttpDelete("{Id}", Name = "ObrisiPonudu")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int Id) {

            var tp = ctx.TroskovnikPonuda.Where(o => o.FkPonudaId == Id).ToList();

            foreach (var element in tp)
            {
                var st = ctx.PonudaStavkaTroskovnik.Where(o => o.FkTroskovnikPonuda == element.Id).ToList();
                foreach (var stElement in st)
                {
                    ctx.Remove(stElement);
                }
                ctx.Remove(element);
            }


            var ps = ctx.PonudaStrucnjak.Where(o => o.FkPonudaId == Id).ToList();

            foreach (var element in ps)
            {
                var dc = ctx.Dokument.Where(d => d.FkPonudaStrucnjakId == element.Id).ToList();
                foreach (var st in dc)
                {
                    ctx.Remove(st);
                }
                ctx.Remove(element);
            }

            var dok = ctx.Dokument.Where(o => o.FkPonudaId == Id).ToList();
            foreach (var element in dok)
            {
                ctx.Remove(element);
            }

            var pok = ctx.PonudaOcjenaKriterija.Where(o => o.FkPonudaId == Id).ToList();
            foreach (var element in pok)
            {
                ctx.Remove(element);
            }

            var ponuda = await ctx.Ponuda.FindAsync(Id);
            if(ponuda == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Ne postoji ponuda s Id = {Id}");
            } else {
                ctx.Remove(ponuda);
                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Ažuriranje postojeće ponude
        /// </summary>
        /// <param name="Id">Id - jedinstveni identifikator ponude</param>
        /// <param name="updatedPonuda">Ažurirani podaci ponude. ID-evi trebaju odgovarati</param>
        /// <response code="400">Id ažurirane ponude se ne poklapa s poslanim Id-em</response>
        /// <response code="404">Ponuda pod s tim Id-em ne postoji</response>     
        /// <returns></returns>
        [HttpPut("{Id}", Name = "AzurirajPonudu")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task <IActionResult> Update(int Id, PonudaViewModel updatedPonuda) {

            if(updatedPonuda.Id != Id) {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Id ažurirane ponude ({updatedPonuda.Id}) ne odgovara poslanom Id-u({Id})");
            } else {
                var ponuda = await ctx.Ponuda.FindAsync(Id);
                if(ponuda == null) {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Ponuda s Id = {Id} ne postoji");
                }

                ponuda.UkupnaCijenaPonude = updatedPonuda.ukupnaCijenaPonude;
                ponuda.FkVrstaPonuditeljaId = await ctx.SifVrstaPonuditelja.Where(m => m.Naziv == updatedPonuda.vrstaPonuditelja).Select(m => m.Id).FirstOrDefaultAsync();
                ponuda.FkTvrtkaOib = await ctx.Tvrtka.Where(v => v.Naziv == updatedPonuda.tvrtka).Select(m => m.Oib).FirstOrDefaultAsync();
                ponuda.FkKonzorcijOib = await ctx.Konzorcij.Where(v => v.Naziv == updatedPonuda.konzorcij).Select(m => m.Oib).FirstOrDefaultAsync();

                await ctx.SaveChangesAsync();
                return NoContent();
            }

        }

        /// <summary>
        /// Stvara novu ponudu
        /// </summary>
        /// <param name="novaPonuda">Podaci nove ponude</param>
        /// <returns></returns>
        [HttpPost(Name = "DodajPonudu")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(PonudaViewModel novaPonuda) {

            Ponudum ponuda = new Ponudum{
                UkupnaCijenaPonude = novaPonuda.ukupnaCijenaPonude,
                FkVrstaPonuditeljaId = await ctx.SifVrstaPonuditelja.Where(m => m.Naziv == novaPonuda.vrstaPonuditelja).Select(m => m.Id).FirstOrDefaultAsync(),
                FkTvrtkaOib = await ctx.Tvrtka.Where(v => v.Naziv == novaPonuda.tvrtka).Select(m => m.Oib).FirstOrDefaultAsync(),
                FkKonzorcijOib = await ctx.Konzorcij.Where(v => v.Naziv == novaPonuda.konzorcij).Select(m => m.Oib).FirstOrDefaultAsync()
        };

            ctx.Add(ponuda);
            await ctx.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { Id = ponuda.Id }, ponuda);

        }

    }
}
