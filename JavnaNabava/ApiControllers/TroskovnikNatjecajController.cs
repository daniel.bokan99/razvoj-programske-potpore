﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JavnaNabava.ApiControllers {

    /// <summary>
    /// Web API servis za rad s troškovnicima natječaja
    /// </summary>

    [ApiController]
    [Route("api/[controller]")]
    public class TroskovnikNatjecajController : ControllerBase, ICustomController<int, TroskovnikNatjecajViewModel1> {

        private readonly RPPP17Context ctx;

        private static Dictionary<string, Expression<Func<TroskovnikNatjecaj, object>>> orderSelectors = new Dictionary<string, Expression<Func<TroskovnikNatjecaj, object>>> {
            [nameof(TroskovnikNatjecajViewModel1.Id).ToLower()] = m => m.Id,
            [nameof(TroskovnikNatjecajViewModel1.Naslov).ToLower()] = m => m.Naslov,
            [nameof(TroskovnikNatjecajViewModel1.NazivNat).ToLower()] = m => m.FkNatjecajEvBrNavigation.Naziv,
            [nameof(TroskovnikNatjecajViewModel1.Opis).ToLower()] = m => m.Opis,
        };

        public TroskovnikNatjecajController(RPPP17Context ctx) {
            this.ctx = ctx;
        }

        /// <summary>
        /// Vraća broj svih troškovnika filtriran prema nazivu troškovnika
        /// </summary>
        /// <param name="filter">Opcionalni filter za naziv troškovnika</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojTroskovnika")]
        public async Task<int> Count([FromQuery] string filter) {

            var query = ctx.TroskovnikNatjecaj.Include(k => k.FkNatjecajEvBrNavigation).AsQueryable();

            if (!string.IsNullOrWhiteSpace(filter)) {
                query = query.Where(k => k.Opis.Contains(filter));
            }

            int count = await query.CountAsync();

            return count;
        }

        /// <summary>
        /// Dohvat troškovnika (opcionalno filtrirano po nazivu troškovnika).
        /// Broj troškovnika, poredak, početna pozicija određeni s loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiTroškovnike")]
        public async Task<List<TroskovnikNatjecajViewModel1>> GetAll([FromQuery] LoadParams loadParams) {

            var query = ctx.TroskovnikNatjecaj.Include(k => k.FkNatjecajEvBrNavigation).AsQueryable();
            if (!string.IsNullOrWhiteSpace(loadParams.Filter)) {
                query = query.Where(k => k.Opis.Contains(loadParams.Filter));
            }

            if(loadParams.SortColumn != null) {
                if(orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr)) {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var list = await query.Select(k => new TroskovnikNatjecajViewModel1 {
                                        Id = k.Id,
                                        Naslov = k.Naslov,
                                        Opis = k.Opis,
                                        NazivNat = k.FkNatjecajEvBrNavigation.Naziv
                                    })
                                   .Skip(loadParams.StartIndex)
                                   .Take(loadParams.Rows)
                                   .ToListAsync();

            return list;
        }

        /// <summary>
        /// Vraća troškovnik čiji je Id jednak vrijednosti parametra id
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns></returns>
        [HttpGet("{Id}", Name = "DohvatiTroškovnik")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TroskovnikNatjecajViewModel1>> Get(int Id) {

            var troskovnik = await ctx.TroskovnikNatjecaj
                                    .Include(k => k.FkNatjecajEvBrNavigation)
                                    .Where(k => k.Id == Id)
                                    .Select(k => new TroskovnikNatjecajViewModel1 {
                                        Id = k.Id,
                                        Naslov = k.Naslov,
                                        Opis = k.Opis,
                                        NazivNat = k.FkNatjecajEvBrNavigation.Naziv
                                    })
                                    .FirstOrDefaultAsync();

            if(troskovnik == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data for id = {Id}");
            } else {
                return troskovnik;
            }
        }

        /// <summary>
        /// Brisanje troskovnika određenog s Id-om
        /// </summary>
        /// <param name="Id">Vrijednost primarnog ključa (Id troškovnika)</param>
        /// <returns></returns>
        /// <response code="204">Ako je troškovnik uspješno obrisan</response>
        /// <response code="404">Ako troškovink s poslanim Id-om ne postoji</response>      
        [HttpDelete("{Id}", Name = "ObrisiTroskovnik")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int Id) {

            var troskovnik = await ctx.TroskovnikNatjecaj.FindAsync(Id);
            if(troskovnik == null) {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid Id = {Id}");
            } else {
                ctx.Remove(troskovnik);
                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Ažurira troškovnik
        /// </summary>
        /// <param name="Id">parametar čija vrijednost jednoznačno identificira troškovnik</param>
        /// <param name="model">Podaci o troškovniku. Id se mora podudarati s parametrom Id</param>
        /// <returns></returns>
        [HttpPut("{Id}", Name = "AzurirajTroskovnik")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task <IActionResult> Update(int Id, TroskovnikNatjecajViewModel1 model) {

            if(model.Id != Id) {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Different Id {Id} vs {model.Id}");
            } else {
                var troskovnik = await ctx.TroskovnikNatjecaj.FindAsync(Id);
                if(troskovnik == null) {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid Id = {Id}");
                }

                troskovnik.Naslov = model.Naslov;
                troskovnik.Opis = model.Opis;
                troskovnik.FkNatjecajEvBr = await ctx.Natjecaj.Where(m => m.Naziv == model.NazivNat).Select(m => m.EvidencijskiBr).FirstOrDefaultAsync();
                troskovnik.Id = model.Id;

                await ctx.SaveChangesAsync();
                return NoContent();
            }

        }

        /// <summary>
        /// Stvara novi troškovnik opisan poslanim modelom
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "DodajTroskovnik")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(TroskovnikNatjecajViewModel1 model) {

            TroskovnikNatjecaj troskovnik = new TroskovnikNatjecaj {
                Naslov = model.Naslov,
                Opis = model.Opis,
                FkNatjecajEvBr = await ctx.Natjecaj.Where(m => String.Equals(m.Naziv, model.NazivNat)).Select(m => m.EvidencijskiBr).FirstOrDefaultAsync()
            };

            ctx.Add(troskovnik);
            await ctx.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { Id = troskovnik.Id }, troskovnik);

        }

    }
}
