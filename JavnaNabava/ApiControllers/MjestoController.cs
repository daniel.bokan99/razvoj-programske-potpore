﻿using JavnaNabava.ApiControllers;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace JavnaNabava.ApiControllers
{
    /// <summary>
    /// Web API servis za rad s mjestima
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MjestoController : ControllerBase, ICustomController<int, MjestoViewModel>
    {
        private readonly RPPP17Context ctx;
        private static Dictionary<string, Expression<Func<Mjesto, object>>> orderSelectors = new Dictionary<string, Expression<Func<Mjesto, object>>>
        {
            [nameof(MjestoViewModel.PostanskiBroj).ToLower()] = m => m.PostanskiBroj,
            [nameof(MjestoViewModel.Naziv).ToLower()] = m => m.Naziv,
            [nameof(MjestoViewModel.NazivDrzave).ToLower()] = m => m.FkDrzavaIsoNavigation.Naziv
        };

        public MjestoController(RPPP17Context ctx)
        {
            this.ctx = ctx;
        }


        /// <summary>
        /// Vraća broj svih mjesta filtriran prema nazivu mjesta 
        /// </summary>
        /// <param name="filter">Opcionalni filter za naziv mjesta</param>
        /// <returns></returns>
        [HttpGet("count", Name = "BrojMjesta")]
        public async Task<int> Count([FromQuery] string filter)
        {
            var query = ctx.Mjesto.AsQueryable();
            if (!string.IsNullOrWhiteSpace(filter))
            {
                query = query.Where(m => m.Naziv.Contains(filter));
            }
            int count = await query.CountAsync();
            return count;
        }

        /// <summary>
        /// Dohvat mjesta (opcionalno filtrirano po nazivu mjesta).
        /// Broj mjesta, poredak, početna pozicija određeni s loadParams.
        /// </summary>
        /// <param name="loadParams">Postavke za straničenje i filter</param>
        /// <returns></returns>
        [HttpGet(Name = "DohvatiMjesta")]
        public async Task<List<MjestoViewModel>> GetAll([FromQuery] LoadParams loadParams)
        {
            var query = ctx.Mjesto.AsQueryable();

            if (!string.IsNullOrWhiteSpace(loadParams.Filter))
            {
                query = query.Where(m => m.Naziv.Contains(loadParams.Filter));
            }

            if (loadParams.SortColumn != null)
            {
                if (orderSelectors.TryGetValue(loadParams.SortColumn.ToLower(), out var expr))
                {
                    query = loadParams.Descending ? query.OrderByDescending(expr) : query.OrderBy(expr);
                }
            }

            var list = await query.Select(m => new MjestoViewModel
            {
                PostanskiBroj = m.PostanskiBroj,
                NazivDrzave = m.FkDrzavaIsoNavigation.Naziv,
                Naziv = m.Naziv
            })
                .Skip(loadParams.StartIndex)
                .Take(loadParams.Rows)
                .ToListAsync();
            return list;
        }

        /// <summary>
        /// Vraća grad čiji je PostanskiBroj jednak vrijednosti parametra PostanskiBroj
        /// </summary>
        /// <param name="PostanskiBroj">PostanskiBroj</param>
        /// <returns></returns>
        [HttpGet("{PostanskiBroj}", Name = "DohvatiMjesto")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MjestoViewModel>> Get(int PostanskiBroj)
        {
            var mjesto = await ctx.Mjesto
                                  .Where(m => m.PostanskiBroj == PostanskiBroj)
                                  .Select(m => new MjestoViewModel
                                  {
                                      PostanskiBroj = m.PostanskiBroj,
                                      NazivDrzave = m.FkDrzavaIsoNavigation.Naziv,
                                      Naziv = m.Naziv
                                  })
                                  .FirstOrDefaultAsync();
            if (mjesto == null)
            {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"No data for PostanskiBroj = {PostanskiBroj}");
            }
            else
            {
                return mjesto;
            }
        }


        /// <summary>
        /// Brisanje mjesta određenog s PostanskiBroj
        /// </summary>
        /// <param name="PostanskiBroj">Vrijednost primarnog ključa (PostanskiBroj mjesta)</param>
        /// <returns></returns>
        /// <response code="204">Ako je mjesto uspješno obrisano</response>
        /// <response code="404">Ako mjesto s poslanim PostanskiBroj-em ne postoji</response>      
        [HttpDelete("{PostanskiBroj}", Name = "ObrisiMjesto")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int PostanskiBroj)
        {
            var mjesto = await ctx.Mjesto.FindAsync(PostanskiBroj);
            if (mjesto == null)
            {
                return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid PostanskiBroj = {PostanskiBroj}");
            }
            else
            {
                ctx.Remove(mjesto);
                await ctx.SaveChangesAsync();
                return NoContent();
            };
        }

        /// <summary>
        /// Ažurira mjesto
        /// </summary>
        /// <param name="PostanskiBroj">parametar čija vrijednost jednoznačno identificira mjesto</param>
        /// <param name="model">Podaci o mjestu. PostanskiBroj mora se podudarati s parametrom PostanskiBroj</param>
        /// <returns></returns>
        [HttpPut("{PostanskiBroj}", Name = "AzurirajMjesto")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Update(int PostanskiBroj, MjestoViewModel model)
        {
            if (model.PostanskiBroj != PostanskiBroj) //ModelState.IsValid i model != null provjera se automatski zbog [ApiController]
            {
                return Problem(statusCode: StatusCodes.Status400BadRequest, detail: $"Different PostanskiBrojs {PostanskiBroj} vs {model.PostanskiBroj}");
            }
            else
            {
                var mjesto = await ctx.Mjesto.FindAsync(PostanskiBroj);
                if (mjesto == null)
                {
                    return Problem(statusCode: StatusCodes.Status404NotFound, detail: $"Invalid PostanskiBroj = {PostanskiBroj}");
                }

                mjesto.Naziv = model.Naziv;
                mjesto.FkDrzavaIso = await ctx.Drzava
                    .Where(m => m.Naziv == model.NazivDrzave)
                    .Select(m => m.Iso)
                    .FirstOrDefaultAsync();
                mjesto.PostanskiBroj = model.PostanskiBroj;

                await ctx.SaveChangesAsync();
                return NoContent();
            }
        }

        /// <summary>
        /// Stvara novo mjesto opisom poslanim modelom
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "DodajMjesto")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create(MjestoViewModel model)
        {
            Mjesto mjesto = new Mjesto
            {
                Naziv = model.Naziv,
                PostanskiBroj = model.PostanskiBroj,
                FkDrzavaIso = await ctx.Drzava
                    .Where(m => m.Naziv == model.NazivDrzave.ToUpper())
                    .Select(m => m.Iso)
                    .FirstOrDefaultAsync()
            };
            ctx.Add(mjesto);
            await ctx.SaveChangesAsync();

            var addedItem = await Get(mjesto.PostanskiBroj);

            return CreatedAtAction(nameof(Get), new { id = mjesto.PostanskiBroj }, addedItem.Value);
        }
    }
}
