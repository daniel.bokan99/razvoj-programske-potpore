﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class ObavijestController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public ObavijestController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = (from o in ctx.Obavijest
                         join s in ctx.SifKategorijeObavijesti
                         on o.IdKategorije equals s.Id
                         select new ObavijestDTO {
                             Id = o.Id,
                             Naslov = o.Naslov,
                             Url = o.Url,
                             NazivKategorije = s.Naziv
                         });
            //var query = ctx.Obavijest.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<ObavijestDTO, object>> orderSelector = null;
            switch (sort) {
                case 1:
                    orderSelector = o => o.Naslov;
                    break;
                case 2:
                    orderSelector = o => o.Url;
                    break;
                case 3:
                    orderSelector = o => o.NazivKategorije;
                    break;
            }
            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var obavijest = query.Skip((page - 1) * pagesize)
                              .Take(appSettings.PageSize)
                              .ToList();

            var model = new ObavijestViewModel {
                Obavijest = obavijest,
                PagingInfo = pagingInfo
            };

            return View("Obavijest", model);
        }

        public IActionResult Create() {

            var kategorije = ctx.SifKategorijeObavijesti.Select(k => new { k.Id, k.Naziv }).ToList();

            ViewBag.IdKategorije = new SelectList(kategorije, "Id", "Naziv");

            return View("AddObavijest");
        }

        public IActionResult ProcessCreate(Obavijest novaObavijest) {

            var obavijest = ctx.Obavijest.SingleOrDefault(o => o.Naslov == novaObavijest.Naslov);

            if (obavijest == null) {
                try {
                    ctx.Add(novaObavijest);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Obavijest {novaObavijest.Naslov} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    TempData[Constants.Message] = "Greška prilikom dodavanja obavijesti u bazu: " + e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;

                    var kategorije = ctx.SifKategorijeObavijesti.Select(k => new { k.Id, k.Naziv }).ToList();

                    ViewBag.IdKategorije = new SelectList(kategorije, "Id", "Naziv");
                    return View("AddObavijest", novaObavijest);
                }
            } else {
                try {
                    obavijest.Url = novaObavijest.Url;
                    obavijest.IdKategorije = novaObavijest.IdKategorije;

                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Obavijest {novaObavijest.Naslov} uspješno ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    TempData[Constants.Message] = "Greška prilikom dodavanja obavijesti u bazu: " + e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;

                    var kategorije = ctx.SifKategorijeObavijesti.Select(k => new { k.Id, k.Naziv }).ToList();

                    ViewBag.IdKategorije = new SelectList(kategorije, "Id", "Naziv");
                    return View("AddObavijest", novaObavijest);
                }
            }

        }

        public IActionResult Edit(int id) {
            var obavijest = ctx.Obavijest.Find(id);

            if (obavijest == null) {
                TempData[Constants.Message] = "Obavijest ne postoji";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index");
            } else {
                var kategorije = ctx.SifKategorijeObavijesti.Select(k => new { k.Id, k.Naziv }).ToList();

                ViewBag.IdKategorije = new SelectList(kategorije, "Id", "Naziv");
                return View("AddObavijest", obavijest);
            }
        }

        public IActionResult Delete(int id) {
            var obavijest = ctx.Obavijest.Find(id);

            try {
                string naziv = obavijest.Naslov;
                ctx.Remove(obavijest);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Obavijest {naziv} uspješno obrisana.";
                TempData[Constants.ErrorOccurred] = false;
            } catch (Exception e) {
                TempData[Constants.Message] = "Pogreška prilikom brisanja obavijest: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }

            return RedirectToAction("Index");
        }
    }
}
