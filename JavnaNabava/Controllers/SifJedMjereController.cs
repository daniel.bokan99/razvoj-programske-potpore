﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class SifJedMjereController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifJedMjereController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot){
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifJedMjere mjera)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(mjera);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Mjera {mjera.Naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(mjera);
                }
            }
            else
            {
                return View(mjera);
            }
        }

        [HttpGet]
        public IActionResult Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            var mjera = ctx.SifJedMjere
                           .AsNoTracking()
                           .Where(d => d.Id == id)
                           .SingleOrDefault();
            if (mjera == null)
            {
                return NotFound($"Ne postoji mjera s oznakom {id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(mjera);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifJedMjere mjera = await ctx.SifJedMjere.FindAsync(id);
                if (mjera == null)
                {
                    return NotFound($"Ne postoji mjera s oznakom {id}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifJedMjere>(mjera, "", d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Mjera {mjera.Naziv} uspješno ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch(Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(mjera);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o mjeri nije moguće povezati s forme");
                    return View(mjera);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Edit), new { id, page, sort, ascending });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var mjera = ctx.SifJedMjere.Find(Id);
            if (mjera == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = mjera.Naziv;
                    ctx.Remove(mjera);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Mjera {naziv} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja mjere " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifJedMjere.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending } );
            }

            System.Linq.Expressions.Expression<Func<SifJedMjere, object>> orderSelector = null;
            switch(sort)
            {
                case 1:
                    orderSelector = d => d.Naziv;
                    break;
                case 2:
                    orderSelector = d => d.Id;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var SifJedMjere = query
                              .Skip((page - 1) * pagesize)
                              .Take(pagesize)
                              .ToList();
            var model = new SifJedMjereViewModel
            {
                Sif_jedMj = SifJedMjere,
                PagingInfo = pagingInfo
            };
            return View(model);
        }
    }
}
