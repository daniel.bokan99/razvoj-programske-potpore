﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class PlanNabaveController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public PlanNabaveController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View("CreatePlanNabave");
        }

        private void PrepareDropDownLists()
        {
            var tvrtke = ctx.Tvrtka
                .OrderBy(d => d.Naziv)
                .Select(d => new { d.Naziv, d.Oib })
                .ToList();
            ViewBag.Tvrtke = new SelectList(tvrtke, nameof(Tvrtka.Oib), nameof(Tvrtka.Naziv));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PlanNabave planNabave)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(planNabave);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Plan nabave {planNabave.Naziv} uspješno dodan.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View("CreatePlanNabave", planNabave);
                }

            }
            else
            {
                PrepareDropDownLists();
                return View("CreatePlanNabave", planNabave);
            }

        }


        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var plan = ctx.PlanNabave
                .AsNoTracking()
                .Where(d => d.Id == Id)
                .SingleOrDefault();
            if (plan == null)
            {
                return NotFound($"Ne postoji plan nabave s oznakom {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                PrepareDropDownLists();
                return View("EditPlanNabave", plan);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                PlanNabave plan = await ctx.PlanNabave.FindAsync(Id);
                if (plan == null)
                {
                    return NotFound($"Ne postoji plan nabave s oznakom {Id}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<PlanNabave>(plan, "", d => d.Naziv, d => d.FkTvrtkaOib);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Plan nabave {plan.Naziv} uspješno ažuriran.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View("EditPlanNabave", plan);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o planu nabave nije moguće povezati s forme");
                    return View("EditPlanNabave", plan);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        public async Task<IActionResult> Edit2(int Id)
        {
            var plan = await ctx.PlanNabave.Where(k => k.Id == Id)
                                                     .Include(k => k.FkTvrtkaOibNavigation)
                                                     .Select(k => new PlanNabaveViewModel
                                                     {
                                                         Id = k.Id,
                                                         Naziv = k.Naziv,
                                                         NazivTvrtke = k.FkTvrtkaOibNavigation.Naziv
                                                     })
                                                     .FirstOrDefaultAsync();

            if (plan == null)
            {
                TempData[Constants.Message] = "plan je null";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index2");
            }
            else
            {
                var stavkeID = await ctx.StavkaNabave.Where(t => t.FkIdPlanNabave == Id)
                                                         .Include(t => t.FkCpvNavigation)
                                                         .Include(t => t.FkIsoNavigation)
                                                         .Include(t => t.FkIdVrstaPostupkaNavigation)
                                                         .Include(t => t.FkIdPlanNabaveNavigation)
                                                         .ToListAsync();

                var stavke = new List<StavkaNabaveViewModel>();

                foreach (StavkaNabave stavka in stavkeID)
                {
                    stavke.Add(new StavkaNabaveViewModel
                    {
                        Id = stavka.Id,
                        EvidencijskiBroj = stavka.EvidencijskiBroj,
                        PredmetNabave = stavka.PredmetNabave,
                        ProcjenjenaVrijednost = stavka.ProcjenjenaVrijednost,
                        VrijediOd = stavka.VrijediOd,
                        VrijediDo = stavka.VrijediDo,
                        PlanNabave = stavka.FkIdPlanNabaveNavigation.Naziv,
                        CPV = stavka.FkCpvNavigation.Naziv,
                        Valuta = stavka.FkIsoNavigation.Naziv,
                        VrstaPostupka = stavka.FkIdVrstaPostupkaNavigation.Naziv
                    });
                }

                plan.Stavke = stavke;

                //foreach(PropertyDescriptor descriptor in TypeDescriptor.GetProperties(konzorcij)) {
                //    string name = descriptor.Name;
                //    object value = descriptor.GetValue(konzorcij);
                //    Console.WriteLine("{0}={1}", name, value);
                //    if(name == "Tvrtke") {
                //        foreach(TvrtkaViewModel2 m in konzorcij.Tvrtke) {
                //            Console.WriteLine(m.Naziv);
                //        }
                //    }
                //}

                return View("Edit2", plan);
            }

        }

        public async Task<IActionResult> ProcessEdit(PlanNabaveViewModel plan)
        {

            try
            {
                var pl = await ctx.PlanNabave.Include(k => k.StavkaNabaves)
                                              .Where(k => k.Id == plan.Id)
                                              .FirstOrDefaultAsync();
                pl.Naziv = plan.Naziv;
                var nazivtvrtke = await ctx.Tvrtka.Where(m => m.Naziv == plan.NazivTvrtke).FirstOrDefaultAsync();
                pl.FkTvrtkaOib = nazivtvrtke.Oib;

                List<int> idStavki = plan.Stavke
                                               .Where(t => t.Id != 0)
                                               .Select(s => s.Id)
                                               .ToList();

                ctx.RemoveRange(pl.StavkaNabaves.Where(s => !idStavki.Contains(s.FkIdPlanNabave)));

                var stavkeUPlanu = await ctx.StavkaNabave.Where(k => k.FkIdPlanNabave == plan.Id)
                                                                 .Select(s => s.Id)
                                                                 .ToListAsync();

                foreach (var stavka in plan.Stavke)
                {

                    StavkaNabave novaStavka;
                    if (stavkeUPlanu.Contains(stavka.Id))
                    {
                        novaStavka = pl.StavkaNabaves.Where(k => k.FkIdPlanNabave == plan.Id).FirstOrDefault();
                    }
                    else
                    {

                        novaStavka = await ctx.StavkaNabave.Where(t => t.Id == stavka.Id).FirstOrDefaultAsync();

                        if (novaStavka == null)
                        {
                            novaStavka = new StavkaNabave();
                            novaStavka.Id = stavka.Id;
                            await ctx.StavkaNabave.AddAsync(novaStavka);
                        }

                        pl.StavkaNabaves.Add(new StavkaNabave
                        {
                            FkIdPlanNabave = plan.Id,
                            Id = novaStavka.Id
                        });

                    }


                    novaStavka.EvidencijskiBroj = stavka.EvidencijskiBroj;
                    novaStavka.PredmetNabave = stavka.PredmetNabave;
                    novaStavka.ProcjenjenaVrijednost = stavka.ProcjenjenaVrijednost;
                    novaStavka.VrijediOd = stavka.VrijediOd;
                    novaStavka.VrijediDo = stavka.VrijediDo;
                    //novaStavka.FkCpv = stavka.CPV;
                    //novaStavka.FkIso = stavka.Valuta;
                    var cpv = await ctx.SifCpv.Where(m => m.Naziv == stavka.CPV).FirstOrDefaultAsync();
                    novaStavka.FkCpv = cpv.Cpv;
                    var valuta = await ctx.SifValuta.Where(m => m.Naziv == stavka.Valuta).FirstOrDefaultAsync();
                    novaStavka.FkIso = valuta.Iso;
                    var vrstaPostupka = await ctx.SifVrstaPostupka.Where(m => m.Naziv == stavka.VrstaPostupka).FirstOrDefaultAsync();
                    novaStavka.FkIdVrstaPostupka = vrstaPostupka.Id;
                    await ctx.SaveChangesAsync();
                }


            }
            catch (Exception e)
            {
                TempData[Constants.Message] = e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index2");
            }

            TempData[Constants.Message] = "Plan nabave uspješno ažuriran.";
            TempData[Constants.ErrorOccurred] = false;

            return RedirectToAction("Edit2", plan);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var plan = ctx.PlanNabave.Find(Id);
            if (plan == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = plan.Naziv;
                    ctx.Remove(plan);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Plan nabave {naziv} uspješno obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = $"Pogreška prilikom brisanja plana nabave." + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.PlanNabave.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.PlanNabave, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Id;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;
                case 3:
                    orderSelector = d => d.FkTvrtkaOibNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var planovi = query
                .Select(o => new PlanNabaveViewModel
                {
                    Id = o.Id,
                    Naziv = o.Naziv,
                    NazivTvrtke = o.FkTvrtkaOibNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            foreach (PlanNabaveViewModel plan in planovi)
            {
                var stavka = ctx.StavkaNabave
                    .Where(o => o.FkIdPlanNabave == plan.Id)
                    .OrderBy(o => o.Id)
                    .Select(o => new StavkaNabaveViewModel
                    {
                        Id = o.Id,
                        EvidencijskiBroj = o.EvidencijskiBroj,
                        PredmetNabave = o.PredmetNabave,
                        ProcjenjenaVrijednost = o.ProcjenjenaVrijednost,
                        VrijediOd = o.VrijediOd,
                        VrijediDo = o.VrijediDo,
                        PlanNabave = o.FkIdPlanNabaveNavigation.Naziv,
                        CPV = o.FkCpvNavigation.Naziv,
                        Valuta = o.FkIsoNavigation.Naziv,
                        VrstaPostupka = o.FkIdVrstaPostupkaNavigation.Naziv
                    })
                    .ToList();
                plan.Stavke = stavka;
            }

            var model = new PlanoviNabaveViewModel
            {
                Planovi = planovi,
                PagingInfo = pagingInfo
            };
            return View("PlanNabave", model);
        }

        public IActionResult Index2(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.PlanNabave.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index2), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.PlanNabave, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Id;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;
                case 3:
                    orderSelector = d => d.FkTvrtkaOibNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var planovi = query
                .Select(o => new PlanNabaveViewModel
                {
                    Id = o.Id,
                    Naziv = o.Naziv,
                    NazivTvrtke = o.FkTvrtkaOibNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            foreach (PlanNabaveViewModel plan in planovi)
            {
                var stavka = ctx.StavkaNabave
                    .Where(o => o.FkIdPlanNabave == plan.Id)
                    .OrderBy(o => o.Id)
                    .Select(o => new StavkaNabaveViewModel
                    {
                        Id = o.Id,
                        EvidencijskiBroj = o.EvidencijskiBroj,
                        PredmetNabave = o.PredmetNabave,
                        ProcjenjenaVrijednost = o.ProcjenjenaVrijednost,
                        VrijediOd = o.VrijediOd,
                        VrijediDo = o.VrijediDo,
                        PlanNabave = o.FkIdPlanNabaveNavigation.Naziv,
                        CPV = o.FkCpvNavigation.Naziv,
                        Valuta = o.FkIsoNavigation.Naziv,
                        VrstaPostupka = o.FkIdVrstaPostupkaNavigation.Naziv
                    })
                    .ToList();
                plan.Stavke = stavka;
            }

            var model = new PlanoviNabaveViewModel
            {
                Planovi = planovi,
                PagingInfo = pagingInfo
            };
            return View("PlanNabave2", model);
        }

        public IActionResult Show(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var plan = ctx.PlanNabave
                .Where(o => o.Id == Id)
                .Select(o => new PlanNabaveViewModel
                {
                    Id = o.Id,
                    Naziv = o.Naziv,
                    NazivTvrtke = o.FkTvrtkaOibNavigation.Naziv
                })
                .FirstOrDefault();
            if (plan == null)
            {
                return NotFound($"Plan nabave sa Id {Id} ne postoji.");
            }
            else
            {
                var stavke = ctx.StavkaNabave
                    .Where(o => o.FkIdPlanNabave == plan.Id)
                    .OrderBy(o => o.FkIso)
                    .Select(o => new StavkaNabaveViewModel
                    {
                        Id = o.Id,
                        EvidencijskiBroj = o.EvidencijskiBroj,
                        PredmetNabave = o.PredmetNabave,
                        ProcjenjenaVrijednost = o.ProcjenjenaVrijednost,
                        VrijediOd = o.VrijediOd,
                        VrijediDo = o.VrijediDo,
                        PlanNabave = o.FkIdPlanNabaveNavigation.Naziv,
                        CPV = o.FkCpvNavigation.Naziv,
                        Valuta = o.FkIsoNavigation.Naziv,
                        VrstaPostupka = o.FkIdVrstaPostupkaNavigation.Naziv
                    })
                    .ToList();
                plan.Stavke = stavke;
                return View(plan);
            }
        }

        public IActionResult Show2(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var plan = ctx.PlanNabave
                .Where(o => o.Id == Id)
                .Select(o => new PlanNabaveViewModel
                {
                    Id = o.Id,
                    Naziv = o.Naziv,
                    NazivTvrtke = o.FkTvrtkaOibNavigation.Naziv
                })
                .FirstOrDefault();
            if (plan == null)
            {
                return NotFound($"Plan nabave sa Id {Id} ne postoji.");
            }
            else
            {
                var stavke = ctx.StavkaNabave
                    .Where(o => o.FkIdPlanNabave == plan.Id)
                    .OrderBy(o => o.FkIso)
                    .Select(o => new StavkaNabaveViewModel
                    {
                        Id = o.Id,
                        EvidencijskiBroj = o.EvidencijskiBroj,
                        PredmetNabave = o.PredmetNabave,
                        ProcjenjenaVrijednost = o.ProcjenjenaVrijednost,
                        VrijediOd = o.VrijediOd,
                        VrijediDo = o.VrijediDo,
                        PlanNabave = o.FkIdPlanNabaveNavigation.Naziv,
                        CPV = o.FkCpvNavigation.Naziv,
                        Valuta = o.FkIsoNavigation.Naziv,
                        VrstaPostupka = o.FkIdVrstaPostupkaNavigation.Naziv
                    })
                    .ToList();
                plan.Stavke = stavke;
                return View(plan);
            }
        }

    }
}
