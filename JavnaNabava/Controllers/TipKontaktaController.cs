﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class TipKontaktaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public TipKontaktaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifTipKontaktum tipKontakta)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(tipKontakta);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Tip kontakta { tipKontakta.Naziv} uspješno dodan";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(tipKontakta);
                }
            }
            else
            {
                return View(tipKontakta);
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var tipKontakta = ctx.SifTipKontakta.AsNoTracking().Where(d => d.Id == Id).FirstOrDefault();
            if (tipKontakta == null)
            {
                return NotFound($"Ne postoji tip kontakta sa tim Id {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(tipKontakta);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifTipKontaktum tipKontakta = await ctx.SifTipKontakta.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (tipKontakta == null)
                {
                    return NotFound($"Ne postoji tipKontakta sa tim Id {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifTipKontaktum>(tipKontakta, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Tip kontakta { tipKontakta.Naziv} uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(tipKontakta);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o tipu kontakta nije moguće povezati s forme");
                    return View(tipKontakta);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var tipKontakta = ctx.SifTipKontakta.Find(Id);
            if (tipKontakta == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = tipKontakta.Naziv;
                    ctx.Remove(tipKontakta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Tip kontakta { tipKontakta.Naziv} uspješno obrisan";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja tipa kontakta: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifTipKontakta.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifTipKontaktum, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var tipoviKontakata = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new TipoviKontakataViewModel
            {
                TipoviKontakata = tipoviKontakata,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
