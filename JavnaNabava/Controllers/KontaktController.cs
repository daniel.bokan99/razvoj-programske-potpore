﻿using JavnaNabava;
using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class KontaktController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public KontaktController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public async Task<IActionResult> CreateAsync()
        {
            await PrepareDropdownLists();
            return View();
        }

        private Task PrepareDropdownLists()
        {
            var vrsteKontakta = ctx.SifTipKontakta.OrderBy(d => d.Naziv).Select(d => new { d.Naziv, d.Id }).ToList();
            ViewBag.VrsteKontakta = new SelectList(vrsteKontakta, nameof(SifTipKontaktum.Id), nameof(SifTipKontaktum.Naziv));

            var osobe = ctx.Osoba.OrderBy(o => o.Oib).Select(o => new { o.PodatciOsobe, o.Oib }).ToList();
            ViewBag.Osobe = new SelectList(osobe, nameof(Osoba.Oib), nameof(Osoba.PodatciOsobe));
            return Task.CompletedTask;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OsobaKontaktDTO osobaKontaktDTO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await PrepareDropdownLists();

                    Kontakti kontakt = new Kontakti();
                    kontakt.KontaktPodatci = osobaKontaktDTO.FkKontakt.KontaktPodatci;
                    kontakt.FkSifKontaktaId = osobaKontaktDTO.FkKontakt.Id;
                    ctx.Add(kontakt);
                    ctx.SaveChanges();

                    OsobaKontakt osobaKontakt = new OsobaKontakt();
                    osobaKontakt.FkOsobaOib = osobaKontaktDTO.FkOsobaOib;
                    var trazeniKontakt = ctx.Kontakti.Where(k => k.KontaktPodatci == osobaKontaktDTO.FkKontakt.KontaktPodatci).SingleOrDefault();
                    osobaKontakt.FkKontaktId = trazeniKontakt.Id;
                    ctx.Add(osobaKontakt);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Kontakt { kontakt.KontaktPodatci} uspješno dodan.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(osobaKontaktDTO);
                }
            }
            else
            {
                await PrepareDropdownLists();
                return View(osobaKontaktDTO);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var kontakt = ctx.Kontakti.AsNoTracking().Where(m => m.Id == Id).FirstOrDefault();
            if (kontakt != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(kontakt);
            }
            else
            {
                return NotFound($"Neispravan ID kontakta: {Id}");
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Kontakti kontakt = await ctx.Kontakti.Where(m => m.Id == Id).FirstOrDefaultAsync();
                if (kontakt == null)
                {
                    return NotFound($"Ne postoji kontakt sa ID: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<Kontakti>(kontakt, "", m => m.Id, m => m.KontaktPodatci, m => m.FkSifKontaktaId);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Kontakt { kontakt.KontaktPodatci} uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(kontakt);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o kontaktu nije moguće povezati s forme");
                    return View(kontakt);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var kontakt = await ctx.Kontakti.FindAsync(Id);
            if (kontakt != null)
            {
                try
                {
                    string naziv = kontakt.KontaktPodatci;
                    ctx.Remove(kontakt);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Kontakt {naziv} obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja kontakta: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji kontakt sa ID: {Id}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Kontakti.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Kontakti, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = m => m.KontaktPodatci;
                    break;
                case 2:
                    orderSelector = m => m.FkSifKontakta;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var kontakti = query
                .Select(m => new KontaktViewModel
                {
                    Id = m.Id,
                    KontaktPodaci = m.KontaktPodatci,
                    NazivKontakta = m.FkSifKontakta.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new KontaktiViewModel
            {
                Kontakti = kontakti,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
