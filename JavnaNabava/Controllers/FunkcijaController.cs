﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class FunkcijaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public FunkcijaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifFunkcija funkcija)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(funkcija);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Funkcija { funkcija.Naziv} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(funkcija);
                }
            }
            else
            {
                return View(funkcija);
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var funkcija = ctx.SifFunkcija.AsNoTracking().Where(d => d.Id == Id).FirstOrDefault();
            if (funkcija == null)
            {
                return NotFound($"Ne postoji funkcija sa tim Id {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(funkcija);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifFunkcija funkcija = await ctx.SifFunkcija.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (funkcija == null)
                {
                    return NotFound($"Ne postoji funkcija sa tim Id {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifFunkcija>(funkcija, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Funkcija { funkcija.Naziv} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(funkcija);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o funkciji nije moguće povezati s forme");
                    return View(funkcija);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var funkcija = ctx.SifFunkcija.Find(Id);
            if (funkcija == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = funkcija.Naziv;
                    ctx.Remove(funkcija);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Funkcija { funkcija.Naziv} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja funkcije: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifFunkcija.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifFunkcija, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var funkcije = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new FunkcijeViewModel
            {
                Funkcije = funkcije,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
