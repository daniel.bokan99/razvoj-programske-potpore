﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class PonudaStrucnjakController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public PonudaStrucnjakController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        private Task getViewBags()
        {
            var natjecajStrucnjaci = ctx.NatjecajStrucnjaci.OrderBy(d => d.Id).Select(d => new { d.Id, d.OpisKriterija }).ToList();
            var ponude = ctx.Ponuda.OrderBy(d => d.Id).Select(d => new { d.Id }).ToList();
            var osobe = ctx.Osoba.OrderBy(d => d.Oib).Select(d => new { d.Oib, d.PodatciOsobe }).ToList();


            ViewBag.osobe = new SelectList(osobe, nameof(Osoba.Oib), nameof(Osoba.PodatciOsobe));
            ViewBag.natjecajStrucnjaci = new SelectList(natjecajStrucnjaci, nameof(NatjecajStrucnjaci.Id), nameof(NatjecajStrucnjaci.OpisKriterija));
            ViewBag.ponude = new SelectList(ponude, nameof(Ponudum.Id), nameof(Ponudum.Id));

            return Task.CompletedTask;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.PonudaStrucnjak.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<PonudaStrucnjak, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.Id;
                    break;
                case 2:
                    orderSelector = p => p.FkPonudaId;
                    break;
                case 3:
                    orderSelector = p => p.FkNatjecajStrucnjaciId;
                    break;
                case 4:
                    orderSelector = p => p.FkOsobaOib;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var strucnjaci = query.Select(p => new StrucnjakViewModel
            {
                Id = p.Id,
                ponuda = p.FkPonudaId.ToString(),
                natjecaj = p.FkNatjecajStrucnjaci.OpisKriterija,
                osoba = p.FkOsobaOibNavigation.Ime + " " + p.FkOsobaOibNavigation.Prezime


            }).Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var model = new StrucnjaciViewModel
            {
                Strucnjaci = strucnjaci,
                PagingInfo = pagingInfo
            };

            return View("PonudaStrucnjak", model);
        }

        [HttpGet]
        public IActionResult Create()
        {

            getViewBags();

            return View("CreatePonudaStrucnjak");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessCreate(PonudaStrucnjak strucnjak)
        {

            PonudaStrucnjak str = ctx.PonudaStrucnjak.SingleOrDefault(str => str.Id == strucnjak.Id);

            if (str == null)
            {
                try
                {
                    ctx.Add(strucnjak);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Stručnjak uspješno stvoren.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    getViewBags();
                    return View("CreatePonudaStrucnjak", strucnjak);
                }
            }
            else
            {

                try
                {
                    str.Id = strucnjak.Id;
                    str.FkPonudaId = strucnjak.FkPonudaId;
                    str.FkNatjecajStrucnjaciId = strucnjak.FkNatjecajStrucnjaciId;
                    str.FkOsobaOib = strucnjak.FkOsobaOib;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = false;
                    getViewBags();
                    return View("CreatePonudaStrucnjak", strucnjak);
                }

            }

        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {

            var strucnjak = ctx.PonudaStrucnjak.Find(Id);
            if (strucnjak != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                getViewBags();
                return View("EditPonudaStrucnjak", strucnjak);
            }
            else
            {
                return NotFound($"Neispravan id strucnjaka: {Id}");
            }

        }


        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                PonudaStrucnjak struc = await ctx.PonudaStrucnjak.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (struc == null)
                {
                    return NotFound($"Ne postoji vrsta ponuditelja s Id: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<PonudaStrucnjak>(struc, "", d => d.Id, d => d.FkPonudaId, d => d.FkNatjecajStrucnjaciId, d => d.FkOsobaOib);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Stručnjak s OIB: { struc.FkOsobaOib} uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(struc);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Greška prilikom ažuriranja stručnjaka na ponudi");
                    return View(struc);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var strucnjak = ctx.PonudaStrucnjak.Find(Id);
            if (strucnjak != null)
            {
                try
                {
                    string oib = strucnjak.FkOsobaOib;
                    ctx.Remove(strucnjak);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Strucnjak s oib: {oib} je obrisan s PonudaStrucnjak.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom PonudaStrucnjak: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne PonudaStrucnjak s id: {Id}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }
    }
}
