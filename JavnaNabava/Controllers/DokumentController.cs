﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class DokumentController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public DokumentController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        private Task getViewBags()
        {
            var ponude = ctx.Ponuda.OrderBy(d => d.Id).Select(d => new { d.Id }).ToList();
            var ponudaStrucnjaci = ctx.PonudaStrucnjak.OrderBy(d => d.Id).Select(d => new { d.Id, d.FkOsobaOib }).ToList();
            var natjecaji = ctx.Natjecaj.OrderBy(d => d.EvidencijskiBr).Select(d => new { d.EvidencijskiBr, d.Naziv }).ToList();
            var vrste = ctx.SifVrstaDokumenta.OrderBy(d => d.Id).Select(d => new { d.Id, d.Naziv }).ToList();

            ViewBag.ponude = new SelectList(ponude, nameof(Ponudum.Id), nameof(Ponudum.Id));
            ViewBag.ponudaStrucnjaci = new SelectList(ponudaStrucnjaci, nameof(PonudaStrucnjak.Id), nameof(PonudaStrucnjak.FkOsobaOib));
            ViewBag.natjecaji = new SelectList(natjecaji, nameof(Natjecaj.EvidencijskiBr), nameof(Natjecaj.Naziv));
            ViewBag.vrste = new SelectList(vrste, nameof(SifVrstaDokumentum.Id), nameof(SifVrstaDokumentum.Naziv));

            return Task.CompletedTask;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Dokument.AsNoTracking();
            int count = query.Count();
            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Dokument, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.UrudzbeniBr;
                    break;
                case 2:
                    orderSelector = d => d.FkVrstaDokumenta;
                    break;
                case 3:
                    orderSelector = d => d.DatumDokumenta;
                    break;
                case 4:
                    orderSelector = d => d.Naslov;
                    break;
                case 5:
                    orderSelector = d => d.FkPonudaId;
                    break;
                case 6:
                    orderSelector = d => d.FkPonudaStrucnjakId;
                    break;
                case 7:
                    orderSelector = d => d.FkNatjecajDokumentEvBr;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var dokumenti = query.Select(p => new DokumentiViewModel
            {
                urudzbeniBr = p.UrudzbeniBr,
                datumDokumenta = p.DatumDokumenta,
                naslov = p.Naslov,
                fileBinary = p.FileBinary,
                ponuda = p.FkPonudaId.HasValue ? p.FkPonudaId.ToString() : "-",
                ponudaStrucnjak = p.FkPonudaStrucnjakId.HasValue ? p.FkPonudaStrucnjak.FkOsobaOibNavigation.Ime+ p.FkPonudaStrucnjak.FkOsobaOibNavigation.Prezime : "-",
                natjecaj = p.FkNatjecajDokumentEvBr.Length > 0 ? p.FkNatjecajDokumentEvBrNavigation.Naziv : "-",
                vrsta = p.FkVrstaDokumenta.Naziv,


            }).Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var model = new DokumentViewModel
            {
                Dokumenti = dokumenti,
                PagingInfo = pagingInfo
            };

            return View("Dokument", model);
        }

        [HttpGet]
        public IActionResult Create()
        {

            getViewBags();

            return View("CreateDokumentForm");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessCreate(Dokument dokument)
        {

            Dokument dok = ctx.Dokument.SingleOrDefault(dok => dok.UrudzbeniBr == dokument.UrudzbeniBr);

            if (dok == null)
            {
                try
                {
                    ctx.Add(dokument);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Dokument {dokument.Naslov} uspješno stvoren.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    getViewBags();
                    return View("CreateDokumentForm", dokument);
                }
            }
            else
            {

                try
                {
                    dok.UrudzbeniBr = dokument.UrudzbeniBr;
                    dok.Naslov = dokument.Naslov;
                    dok.DatumDokumenta = dokument.DatumDokumenta;
                    dok.FileBinary = dokument.FileBinary;
                    dok.FkPonudaId = dokument.FkPonudaId;
                    dok.FkPonudaStrucnjakId = dokument.FkPonudaStrucnjakId;
                    dok.FkNatjecajDokumentEvBr = dokument.FkNatjecajDokumentEvBr;
                    dok.FkVrstaDokumentaId = dokument.FkVrstaDokumentaId;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = false;
                    getViewBags();
                    return View("CreateDokumentForm", dokument);
                }

            }

        }
        [HttpGet]
        public IActionResult Edit(string UrudzbeniBr, int page = 1, int sort = 1, bool ascending = true)
        {
            var dokument = ctx.Dokument.Find(UrudzbeniBr);

            if (dokument == null)
            {
                return NotFound($"Neispravan urudzbeniBr: {UrudzbeniBr}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                getViewBags();
                return View("EditDokument", dokument);
            }

        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string UrudzbeniBr, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Dokument dokument = await ctx.Dokument.Where(d => d.UrudzbeniBr == UrudzbeniBr).FirstOrDefaultAsync();
                if (dokument == null)
                {
                    return NotFound($"Ne postoji mjesto sa poštanskim brojem {UrudzbeniBr}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<Dokument>(dokument, "", d => d.UrudzbeniBr, d => d.DatumDokumenta, d => d.Naslov, d => d.FileBinary, d => d.FkPonudaId, d => d.FkPonudaStrucnjakId, d => d.FkVrstaDokumentaId, d => d.FkNatjecajDokumentEvBr);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Dokument { dokument.UrudzbeniBr} uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(dokument);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Greška prilikom ažuriranja dokumenta");
                    return View(dokument);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { UrudzbeniBr, page, sort, ascending });
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string UrudzbeniBr, int page = 1, int sort = 1, bool ascending = true)
        {
            var dokument = ctx.Dokument.Find(UrudzbeniBr);
            if (dokument != null)
            {
                try
                {
                    string naziv = dokument.Naslov;
                    ctx.Remove(dokument);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Dokument {naziv} je obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja dokumenta: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji dokument s nazivom {UrudzbeniBr}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }


    }
}


