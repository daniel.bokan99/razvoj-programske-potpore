﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class SifVrstaPonuditeljaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public SifVrstaPonuditeljaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {

            int pagesize = appSettings.PageSize;
            var query = ctx.SifVrstaPonuditelja.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifVrstaPonuditelja, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = s => s.Id;
                    break;
                case 2:
                    orderSelector = s => s.Naziv;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var vrste = query
                                .Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var model = new VrstePonuditeljaViewModel
            {
                Vrste = vrste,
                PagingInfo = pagingInfo
            };


            return View("sifVrstaPonuditelja", model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("CreateSifVrstaPonuditelja");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessCreate(SifVrstaPonuditelja vrsta)
        {

            var vrstaPon = ctx.SifVrstaPonuditelja.SingleOrDefault(v => v.Id == vrsta.Id);

            if (vrstaPon == null)
            {

                try
                {
                    string naziv = vrsta.Naziv;
                    ctx.Add(vrsta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta ponuditelja {naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("CreateSifVrstaPonuditelja", vrsta);

                }

            }
            else
            {

                try
                {
                    vrstaPon.Naziv = vrsta.Naziv;
                    vrstaPon.Id = vrsta.Id;
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta ponuditelja {vrstaPon.Naziv} uspješno ažurirana u {vrsta.Naziv}.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("CreateSifVrstaPonuditelja", vrsta);
                }

            }

        }
        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {

            var vrsta = ctx.SifVrstaPonuditelja.Find(Id);
            if (vrsta != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View("EditSifVrstaPonuditelja", vrsta);
            }
            else
            {
                return NotFound($"Neispravan id vrste: {Id}");
            }

        }


        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifVrstaPonuditelja sif = await ctx.SifVrstaPonuditelja.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (sif == null)
                {
                    return NotFound($"Ne postoji vrsta ponuditelja s Id: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifVrstaPonuditelja>(sif, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Vrsta ponuditelja { sif.Naziv} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(sif);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Greška prilikom ažuriranja vrste");
                    return View(sif);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {

            try
            {
                var vrsta = ctx.SifVrstaPonuditelja.Find(Id);
                if (vrsta == null)
                {
                    return NotFound();
                }
                else
                {
                    ctx.Remove(vrsta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta ponuditelja {vrsta.Naziv} uspješno izbrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }

            }
            catch (Exception e)
            {
                TempData[Constants.Message] = "Greška prilikom brisanja vrste ponuditelja: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }

            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }

    }
}

