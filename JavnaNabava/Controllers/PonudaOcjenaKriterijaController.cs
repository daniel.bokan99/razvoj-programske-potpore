﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace JavnaNabava.Controllers
{
    public class PonudaOcjenaKriterijaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public PonudaOcjenaKriterijaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        private Task getViewBags()
        {
            var kriteriji = ctx.NatjecajKriterijOcjenjivanja.OrderBy(d => d.Id).Select(d => new { d.Id, d.Opis }).ToList();
            var ponude = ctx.Ponuda.OrderBy(d => d.Id).Select(d => new { d.Id }).ToList();

            ViewBag.kriteriji = new SelectList(kriteriji, nameof(NatjecajKriterijOcjenjivanja.Id), nameof(NatjecajKriterijOcjenjivanja.Opis));
            ViewBag.ponude = new SelectList(ponude, nameof(Ponudum.Id), nameof(Ponudum.Id));

            return Task.CompletedTask;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.PonudaOcjenaKriterija.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<PonudaOcjenaKriterija, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.Id;
                    break;
                case 2:
                    orderSelector = p => p.FkPonudaId;
                    break;
                case 3:
                    orderSelector = p => p.FkNatjecajKriterijOcjenjivanjaId;
                    break;
                case 4:
                    orderSelector = p => p.OstvareniBodovi;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var ocjene = query.Select(p => new PonudaOcjenaViewModel
            {
                Id = p.Id,
                ostvareniBodovi = p.OstvareniBodovi,
                natjecajKriterij = p.FkNatjecajKriterijOcjenjivanja.Opis,
                ponuda = p.FkPonudaId.ToString(),


            }).Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var model = new PonudaOcjeneViewModel
            {
                Ocjene = ocjene,
                PagingInfo = pagingInfo
            };

            return View("ponudaOcjenaKriterija", model);
        }

        [HttpGet]
        public IActionResult Create()
        {

            getViewBags();

            return View("CreatePonudaOcjenaKriterija");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessCreate(PonudaOcjenaKriterija ocjena)
        {

            PonudaOcjenaKriterija ocj = ctx.PonudaOcjenaKriterija.SingleOrDefault(ocj => ocj.Id == ocjena.Id);

            if (ocj == null)
            {
                try
                {
                    ctx.Add(ocjena);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ocjena kriterija uspješno stvorena.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    getViewBags();
                    return View("CreatePonudaOcjenaKriterija", ocjena);
                }
            }
            else
            {

                try
                {
                    ocj.Id = ocjena.Id;
                    ocj.OstvareniBodovi = ocjena.OstvareniBodovi;
                    ocj.FkNatjecajKriterijOcjenjivanjaId = ocjena.FkNatjecajKriterijOcjenjivanjaId;
                    ocj.FkPonudaId = ocjena.FkPonudaId;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = false;
                    getViewBags();
                    return View("CreatePonudaOcjenaKriterija", ocjena);
                }

            }

        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {

            var ocjena = ctx.PonudaOcjenaKriterija.Find(Id);
            if (ocjena != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                getViewBags();
                return View("EditPonudaOcjenaKriterija", ocjena);
            }
            else
            {
                return NotFound($"Neispravan id ocjene: {Id}");
            }

        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                PonudaOcjenaKriterija ponuda = await ctx.PonudaOcjenaKriterija.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (ponuda == null)
                {
                    return NotFound($"Ne postoji ocjena s Id: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<PonudaOcjenaKriterija>(ponuda, "", d => d.Id, d => d.OstvareniBodovi, d => d.FkNatjecajKriterijOcjenjivanjaId, d => d.FkPonudaId);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Ocjena s Id: { ponuda.Id} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(ponuda);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Greška prilikom ažuriranja ocjene");
                    return View(ponuda);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var ocjena = ctx.PonudaOcjenaKriterija.Find(Id);
            if (ocjena != null)
            {
                try
                {
                    int id = ocjena.Id;
                    ctx.Remove(ocjena);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ocjena s id: {id} je obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ocjene: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji ocjena s id: {Id}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }
    }
}






