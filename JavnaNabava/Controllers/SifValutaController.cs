﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class SifValutaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifValutaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("CreateValuta");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifValutum valuta)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(valuta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Valuta {valuta.Naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View("CreateValuta", valuta);
                }

            }
            else
            {
                return View("CreateValuta", valuta);
            }

        }


        [HttpGet]
        public IActionResult Edit(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            var valuta = ctx.SifValuta
                .AsNoTracking()
                .Where(d => d.Iso == Iso)
                .SingleOrDefault();
            if (valuta == null)
            {
                return NotFound($"Ne postoji valuta s oznakom {Iso}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View("EditValuta", valuta);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Debug.WriteLine("Ovdje: " + Iso);
                SifValutum valuta = await ctx.SifValuta.FindAsync(Iso);
                if (valuta == null)
                {
                    return NotFound($"Ne postoji valuta s oznakom {Iso}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifValutum>(valuta, "", d => d.Iso, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Valuta {valuta.Naziv} uspješno ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View("EditValuta", valuta);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o valuti nije moguće povezati s forme");
                    return View("EditValuta", valuta);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Iso, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            var valuta = ctx.SifValuta.Find(Iso);
            if (valuta == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = valuta.Naziv;
                    ctx.Remove(valuta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Valuta {naziv} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = $"Pogreška prilikom brisanja valute." + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifValuta.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.SifValutum, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Iso;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;

            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var valute = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();
            var model = new ValuteViewModel
            {
                Valute = (System.Collections.Generic.IEnumerable<SifValutum>)valute,
                PagingInfo = pagingInfo
            };
            return View("SifValuta", model);
        }
    }
}
