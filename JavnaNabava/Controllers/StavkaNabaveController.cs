﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class StavkaNabaveController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public StavkaNabaveController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View("CreateStavkaNabave");
        }

        private void PrepareDropDownLists()
        {
            var planovi = ctx.PlanNabave
                .OrderBy(d => d.Naziv)
                .Select(d => new { d.Naziv, d.Id })
                .ToList();
            ViewBag.Planovi = new SelectList(planovi, nameof(PlanNabave.Id), nameof(PlanNabave.Naziv));
            var cpvovi = ctx.SifCpv
                .OrderBy(d => d.Naziv)
                .Select(d => new { d.Cpv, d.Naziv })
                .ToList();
            ViewBag.CPVovi = new SelectList(cpvovi, nameof(SifCpv.Cpv), nameof(SifCpv.Naziv));
            var valute = ctx.SifValuta
                .OrderBy(d => d.Naziv)
                .Select(d => new { d.Iso, d.Naziv })
                .ToList();
            ViewBag.Valute = new SelectList(valute, nameof(SifValutum.Iso), nameof(SifValutum.Naziv));
            var vrstePostupka = ctx.SifVrstaPostupka
                .OrderBy(d => d.Naziv)
                .Select(d => new { d.Id, d.Naziv })
                .ToList();
            ViewBag.VrstePostupka = new SelectList(vrstePostupka, nameof(SifVrstaPostupka.Id), nameof(SifVrstaPostupka.Naziv));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(StavkaNabave stavkaNabave)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(stavkaNabave);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Stavka nabave {stavkaNabave.PredmetNabave} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View("CreateStavkaNabave", stavkaNabave);
                }

            }
            else
            {
                PrepareDropDownLists();
                return View("CreateStavkaNabave", stavkaNabave);
            }

        }


        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var stavka = ctx.StavkaNabave
                .AsNoTracking()
                .Where(d => d.Id == Id)
                .SingleOrDefault();
            if (stavka == null)
            {
                return NotFound($"Ne postoji stavka nabave s oznakom {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                PrepareDropDownLists();
                return View("EditStavkaNabave", stavka);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                StavkaNabave stavka = await ctx.StavkaNabave.FindAsync(Id);
                if (stavka == null)
                {
                    return NotFound($"Ne postoji stavka nabave s oznakom {Id}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<StavkaNabave>(stavka, "", d => d.EvidencijskiBroj, d => d.PredmetNabave, d => d.ProcjenjenaVrijednost, 
                            d => d.VrijediOd, d => d.VrijediDo, d => d.FkIdPlanNabave, d => d.FkCpv, d => d.FkIso, d => d.FkIdVrstaPostupka);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Stavka nabave {stavka.PredmetNabave} uspješno ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View("EditStavkaNabave", stavka);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o stavci nabave nije moguće povezati s forme");
                    return View("EditStavkaNabave", stavka);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var stavka = ctx.StavkaNabave.Find(Id);
            if (stavka == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = stavka.PredmetNabave;
                    ctx.Remove(stavka);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Stavka nabave {naziv} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = $"Pogreška prilikom brisanja stavke." + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.StavkaNabave.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.StavkaNabave, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Id;
                    break;
                case 2:
                    orderSelector = d => d.EvidencijskiBroj;
                    break;
                case 3:
                    orderSelector = d => d.PredmetNabave;
                    break;
                case 4:
                    orderSelector = d => d.ProcjenjenaVrijednost;
                    break;
                case 5:
                    orderSelector = d => d.VrijediOd;
                    break;
                case 6:
                    orderSelector = d => d.VrijediDo;
                    break;
                case 7:
                    orderSelector = d => d.FkIdPlanNabaveNavigation.Naziv;
                    break;
                case 8:
                    orderSelector = d => d.FkCpvNavigation.Naziv;
                    break;
                case 9:
                    orderSelector = d => d.FkIsoNavigation.Naziv;
                    break;
                case 10:
                    orderSelector = d => d.FkIdVrstaPostupkaNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var stavke = query
                .Select(o => new StavkaNabaveViewModel
                {
                    Id = o.Id,
                    EvidencijskiBroj = o.EvidencijskiBroj,
                    PredmetNabave = o.PredmetNabave,
                    ProcjenjenaVrijednost = o.ProcjenjenaVrijednost,
                    VrijediOd = o.VrijediOd,
                    VrijediDo = o.VrijediDo,
                    PlanNabave = o.FkIdPlanNabaveNavigation.Naziv,
                    CPV = o.FkCpvNavigation.Naziv,
                    Valuta = o.FkIsoNavigation.Naziv,
                    VrstaPostupka = o.FkIdVrstaPostupkaNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new StavkeNabaveViewModel
            {
                Stavke = stavke,
                PagingInfo = pagingInfo
            };
            return View("StavkaNabave", model);
        }
    }
}
