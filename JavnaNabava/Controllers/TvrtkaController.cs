﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class TvrtkaController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public TvrtkaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = ctx.Tvrtka.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Tvrtka, object>> orderSelector = null;
            switch (sort) {
                case 1:
                    orderSelector = k => k.Naziv;
                    break;
                case 2:
                    orderSelector = k => k.Adresa;
                    break;
                case 3:
                    orderSelector = k => k.FkMjesto;
                    break;
                case 4:
                    orderSelector = k => k.JeDjelatnostis;
                    break;
            }
            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var tvrtka = query.Skip((page - 1) * pagesize)
                              .Take(appSettings.PageSize)
                              .ToList();

            var mjesto = ctx.Mjesto.ToList();

            var tvrtke = from t in tvrtka
                         join m in mjesto on t.FkMjesto equals m.PostanskiBroj into table1
                         from m in table1.ToList()
                         select new TvrtkaDTO {
                             tvrtka = t,
                             mjesto = m
                         };

           

            var detalji = tvrtke.ToList();

            for (int i = 0; i < detalji.Count(); i++) {
                var idDjelatnosti = ctx.JeDjelatnosti.Where(d => d.Oib == detalji[i].tvrtka.Oib).SingleOrDefault();
                var djelatnost = ctx.SifDjelatnostTvrtke.Where(d => d.Id == idDjelatnosti.Id).SingleOrDefault();
                detalji[i].djelatnost = djelatnost;
            }

            var model = new TvrtkaViewModel {
                tvrtka = detalji,
                PagingInfo = pagingInfo
            };

            return View("Tvrtka", model);
        }

        public IActionResult Create() {

            var mjesto = ctx.Mjesto
                               .OrderBy(m => m.Naziv)
                               .Select(m => new { m.Naziv, m.PostanskiBroj })
                               .ToList();

            ViewBag.FkMjesto = new SelectList(mjesto, "PostanskiBroj", "Naziv");

            var djelatnost = ctx.SifDjelatnostTvrtke.OrderBy(d => d.Naziv)
                                                    .Select(d => new { d.Naziv, d.Id })
                                                    .ToList();

            ViewBag.sifDjelatnost = new SelectList(djelatnost, "Id", "Naziv");

            return View("AddTvrtka");
        }

        [HttpPost]
        public IActionResult ProcessCreate(AddTvrtkaDTO dto) {

            var tvrtka = ctx.Tvrtka.SingleOrDefault(t => t.Oib == dto.OIB);

            if(tvrtka == null) {
                try {
                    Tvrtka tv = new Tvrtka {
                        Oib = dto.OIB,
                        Naziv = dto.Naziv,
                        FkMjesto = dto.PostanskiBroj,
                        Adresa = dto.Adresa
                    };
                    ctx.Add(tv);
                    ctx.SaveChanges();

                    JeDjelatnosti dj = new JeDjelatnosti {
                        Oib = dto.OIB,
                        Id = dto.idDjelatnost
                    };

                    ctx.Add(dj);
                    ctx.SaveChanges();   

                    TempData[Constants.Message] = $"Tvrtka {dto.Naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch(Exception e) {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());

                    var mjesto = ctx.Mjesto
                               .OrderBy(m => m.Naziv)
                               .Select(m => new { m.Naziv, m.PostanskiBroj })
                               .ToList();

                    ViewBag.FkMjesto = new SelectList(mjesto, "PostanskiBroj", "Naziv");

                    var djelatnost = ctx.SifDjelatnostTvrtke.OrderBy(d => d.Naziv)
                                                            .Select(d => new { d.Naziv, d.Id })
                                                            .ToList();

                    ViewBag.sifDjelatnost = new SelectList(djelatnost, "Id", "Naziv");

                    return View("AddTvrtka", dto);
                }
            } else {

                try {
                    tvrtka.Naziv = dto.Naziv;
                    tvrtka.FkMjesto = dto.PostanskiBroj;
                    tvrtka.Adresa = dto.Adresa;

                    ctx.SaveChanges();

                    var dj = ctx.JeDjelatnosti.SingleOrDefault(dj => dj.Oib == dto.OIB);
                    dj.Id = dto.idDjelatnost;
                    ctx.SaveChanges();

                    return RedirectToAction("Index");
                }catch(Exception e) {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;

                    var mjesto = ctx.Mjesto
                               .OrderBy(m => m.Naziv)
                               .Select(m => new { m.Naziv, m.PostanskiBroj })
                               .ToList();

                    ViewBag.FkMjesto = new SelectList(mjesto, "PostanskiBroj", "Naziv");

                    var djelatnost = ctx.SifDjelatnostTvrtke.OrderBy(d => d.Naziv)
                                                            .Select(d => new { d.Naziv, d.Id })
                                                            .ToList();

                    ViewBag.sifDjelatnost = new SelectList(djelatnost, "Id", "Naziv");

                    return View("AddTvrtka", dto);
                }
                
            }

        }

        public IActionResult Edit(string oib) {

            var tvrtka = ctx.Tvrtka
                                .Find(oib);

            if (tvrtka == null) {
                TempData[Constants.Message] = "Tvrkta ne postoji";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index");
            } else {

                var mjesto = ctx.Mjesto
                               .OrderBy(m => m.Naziv)
                               .Select(m => new { m.Naziv, m.PostanskiBroj })
                               .ToList();

                ViewBag.FkMjesto = new SelectList(mjesto, "PostanskiBroj", "Naziv");

                var djelatnost = ctx.SifDjelatnostTvrtke.OrderBy(d => d.Naziv)
                                                        .Select(d => new { d.Naziv, d.Id })
                                                        .ToList();

                ViewBag.sifDjelatnost = new SelectList(djelatnost, "Id", "Naziv");

                var tvrtkaDTO = new AddTvrtkaDTO {
                    OIB = tvrtka.Oib,
                    Adresa = tvrtka.Adresa,
                    Naziv = tvrtka.Naziv,
                    PostanskiBroj = tvrtka.FkMjesto,
                };

                var sifDj = ctx.JeDjelatnosti.SingleOrDefault(dj => dj.Oib == oib);

                tvrtkaDTO.idDjelatnost = sifDj.Id;

                return View("AddTvrtka", tvrtkaDTO);
            }

        }

        public IActionResult Delete(string oib) {

            var sifDj = ctx.JeDjelatnosti.SingleOrDefault(dj => dj.Oib == oib);
            ctx.Remove(sifDj);
            ctx.SaveChanges();

            var tvrtka = ctx.Tvrtka.Find(oib);

            try {
                string naziv = tvrtka.Naziv;
                ctx.Remove(tvrtka);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Tvrtka {naziv} uspješno obrisana.";
                TempData[Constants.ErrorOccurred] = false;
            } catch(Exception e) {
                TempData[Constants.Message] = "Pogreška prilikom brisanja tvrtke: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction("Index");
        }
    }
}
