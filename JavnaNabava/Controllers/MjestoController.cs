﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class MjestoController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public MjestoController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public async Task<IActionResult> CreateAsync()
        {
            await PrepareDropdownLists();
            return View();
        }

        private Task PrepareDropdownLists()
        {
            var drzave = ctx.Drzava.Where(d => d.Iso != "HR").OrderBy(d => d.Naziv).Select(d => new { d.Naziv, d.Iso }).ToList();
            var hr = ctx.Drzava.Where(d => d.Iso == "HR").OrderBy(d => d.Naziv).Select(d => new { d.Naziv, d.Iso }).FirstOrDefault();
            if (hr != null)
            {
                drzave.Insert(0, hr);
            }
            ViewBag.Drzave = new SelectList(drzave, nameof(Drzava.Iso), nameof(Drzava.Naziv));
            return Task.CompletedTask;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Mjesto mjesto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(mjesto);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Mjesto { mjesto.Naziv} uspješno dodano. Poštanski broj mjesta = {mjesto.PostanskiBroj}";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(mjesto);
                }
            }
            else
            {
                await PrepareDropdownLists();
                return View(mjesto);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int PostanskiBroj, int page = 1, int sort = 1, bool ascending = true)
        {
            var mjesto = ctx.Mjesto.AsNoTracking().Where(m => m.PostanskiBroj == PostanskiBroj).FirstOrDefault();
            if (mjesto != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(mjesto);
            }
            else
            {
                return NotFound($"Neispravan postanski broj mjesta: {PostanskiBroj}");
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int PostanskiBroj, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Mjesto mjesto = await ctx.Mjesto.Where(m => m.PostanskiBroj == PostanskiBroj).FirstOrDefaultAsync();
                if (mjesto == null)
                {
                    return NotFound($"Ne postoji mjesto sa poštanskim brojem {PostanskiBroj}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<Mjesto>(mjesto, "", m => m.PostanskiBroj, m => m.Naziv, m => m.FkDrzavaIso);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Mjesto { mjesto.PostanskiBroj} uspješno ažurirano";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(mjesto);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o mjestu nije moguće povezati s forme");
                    return View(mjesto);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { PostanskiBroj, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int PostanskiBroj, int page = 1, int sort = 1, bool ascending = true)
        {
            var mjesto = await ctx.Mjesto.FindAsync(PostanskiBroj);
            if (mjesto != null)
            {
                try
                {
                    string naziv = mjesto.Naziv;
                    ctx.Remove(mjesto);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Mjesto {naziv} sa poštanskim brojem {PostanskiBroj} obrisano.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja mjesta: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji mjesto sa poštanskim brojem {PostanskiBroj}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Mjesto.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Mjesto, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = m => m.PostanskiBroj;
                    break;
                case 2:
                    orderSelector = m => m.Naziv;
                    break;
                case 3:
                    orderSelector = m => m.FkDrzavaIsoNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var mjesta = query
                .Select(m => new MjestoViewModel
                {
                    PostanskiBroj = m.PostanskiBroj,
                    Naziv = m.Naziv,
                    NazivDrzave = m.FkDrzavaIsoNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new MjestaViewModel
            {
                Mjesta = mjesta,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
