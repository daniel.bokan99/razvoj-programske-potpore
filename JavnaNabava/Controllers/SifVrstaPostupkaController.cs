﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class SifVrstaPostupkaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifVrstaPostupkaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("CreateVrstaPostupka");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifVrstaPostupka vrsta)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(vrsta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta postupka {vrsta.Naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View("CreateVrstaPostupka", vrsta);
                }

            }
            else
            {
                return View("CreateVrstaPostupka", vrsta);
            }

        }


        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var vrsta = ctx.SifVrstaPostupka
                .AsNoTracking()
                .Where(d => d.Id == Id)
                .SingleOrDefault();
            if (vrsta == null)
            {
                return NotFound($"Ne postoji vrsta postupka s oznakom {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View("EditVrstaPostupka", vrsta);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                //Debug.WriteLine("Ovdje: " + Id);
                SifVrstaPostupka vrsta = await ctx.SifVrstaPostupka.FindAsync(Id);
                if (vrsta == null)
                {
                    return NotFound($"Ne postoji vrsta postupka s oznakom {Id}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifVrstaPostupka>(vrsta, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Vrsta postupka {vrsta.Naziv} uspješno ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View("EditVrstaPostupka", vrsta);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o vrsti postupka nije moguće povezati s forme");
                    return View("EditVrstaPostupka", vrsta);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var vrsta = ctx.SifVrstaPostupka.Find(Id);
            if (vrsta == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = vrsta.Naziv;
                    ctx.Remove(vrsta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta postupka {naziv} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = $"Pogreška prilikom brisanja vrste postupka." + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifVrstaPostupka.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.SifVrstaPostupka, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Id;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;

            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var vrste = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();
            var model = new VrstePostupkaViewModel
            {
                Vrste = (System.Collections.Generic.IEnumerable<SifVrstaPostupka>)vrste,
                PagingInfo = pagingInfo
            };
            return View("VrstaPostupka", model);
        }
    }
}
