﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class TroskovnikPonudaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public TroskovnikPonudaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View();
        }

        private void PrepareDropDownLists()
        {
            var natj = ctx.TroskovnikPonuda
                            .Select(d => new { d.FkPonuda.FkTvrtkaOibNavigation.Naziv, d.FkPonudaId })
                            .OrderBy(d => d.Naziv)
                            .ToList();

            ViewBag.NatjecajiPo = new SelectList(natj, nameof(TroskovnikPonuda.FkPonudaId), nameof(TroskovnikPonuda.FkPonuda.FkTvrtkaOibNavigation.Naziv));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TroskovnikPonuda stavka)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(stavka);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuda {stavka.Naslov} uspješno dodana. Id ponude = {stavka.Id}";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View(stavka);
                }
            }
            else
            {
                PrepareDropDownLists();
                return View(stavka);
            }

        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.TroskovnikPonuda.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<TroskovnikPonuda, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naslov;
                    break;
                case 2:
                    orderSelector = d => d.Opis;
                    break;
                case 3:
                    orderSelector = d => d.UkCijena;
                    break;
                case 4:
                    orderSelector = d => d.FkPonuda.FkTvrtkaOibNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var stavke = query
                              .Select(s => new TroskovnikPonudaViewModel
                              {
                                  Id = s.Id,
                                  Naslov = s.Naslov,
                                  Opis = s.Opis,
                                  UkCijena = s.UkCijena,
                                  Ponuditelj = s.FkPonuda.FkTvrtkaOibNavigation.Naziv
                              })
                              .Skip((page - 1) * pagesize)
                              .Take(pagesize)
                              .ToList();
            var model = new TroskovnikPonudeViewModel
            {
                ponude = stavke,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int Id)
        {
            var pst = ctx.PonudaStavkaTroskovnik.Where(pst => pst.FkTroskovnikPonuda == Id).ToList();
            foreach(var el in pst)
            {
                ctx.Remove(el);
                ctx.SaveChanges();
            }
            var stavka = await ctx.TroskovnikPonuda.FindAsync(Id);
            if (stavka != null)
            {
                try
                {
                    string naziv = stavka.Naslov;
                    ctx.Remove(stavka);
                    await ctx.SaveChangesAsync();
                    var result = new
                    {
                        message = $"{naziv} sa šifrom {Id} obrisano",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = $"Pogreška prilikom brisanja {exc.CompleteExceptionMessage()}",
                        successful = true
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Ponuda sa id-om: {Id} ne postoji");
            }
        }
        [HttpGet]

        public async Task<IActionResult> Edit(int id)
        {
            var pst = ctx.PonudaStavkaTroskovnik.Where(pst => pst.FkTroskovnikPonuda == id).ToList();
            foreach (var el in pst)
            {
                ctx.Update(el);
                ctx.SaveChanges();
            }
            var stavka = await ctx.TroskovnikPonuda.FindAsync(id);
            if (stavka != null)
            {
                PrepareDropDownLists();
                return PartialView(stavka);
            }
            else
            {
                return NotFound($"Neispravan id ponude: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TroskovnikPonuda stavka)
        {
            if (stavka == null)
            {
                return NotFound("Nema poslanih podataka");
            }

            bool checkId = ctx.TroskovnikPonuda.Any(s => s.Id == stavka.Id);
            if (!checkId)
            {
                return NotFound($"Nispravan id ponude: {stavka.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(stavka);
                    ctx.SaveChanges();
                    return StatusCode(302, Url.Action(nameof(Row), new { id = stavka.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return PartialView(stavka);
                }

            }
            else
            {
                PrepareDropDownLists();
                return PartialView(stavka);
            }
        }
        public PartialViewResult Row(int id)
        {
            var stavka = ctx.TroskovnikPonuda
                            .Where(s => s.Id == id)
                            .Select(s => new TroskovnikPonudaViewModel
                            {
                                Id = s.Id,
                                Naslov = s.Naslov,
                                Opis = s.Opis,
                                UkCijena = s.UkCijena,
                                Ponuditelj = s.FkPonuda.FkTvrtkaOibNavigation.Naziv
                            })
                           .SingleOrDefault();
            if (stavka != null)
            {
                return PartialView(stavka);
            }
            else
            {
                return PartialView("ErrorMessageRow", $"Neispravan id ponude : {id}");
            }
        }
    }
}
