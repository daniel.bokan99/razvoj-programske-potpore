﻿using System.Linq;
using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using JavnaNabava.Exceptions;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class NatjecajKriterijOcjenjivanjaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public NatjecajKriterijOcjenjivanjaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.NatjecajKriterijOcjenjivanja.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<NatjecajKriterijOcjenjivanja, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Id;
                    break;
                case 2:
                    orderSelector = d => d.EvidencijskiBr;
                    break;
                case 3:
                    orderSelector = d => d.Opis;
                    break;
                case 4:
                    orderSelector = d => d.Ponder;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var kriterij = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new NatjecajKriterijOcjenjivanjaTableViewModel
            {
                NatjecajKriterij = kriterij,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(NatjecajKriterijOcjenjivanja kriterij)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(kriterij);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Kriterij uspješno dodan";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(kriterij);
                }
            }
            else
            {
                return View(kriterij);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var zakon = ctx.NatjecajKriterijOcjenjivanja.Find(Id);
            if (zakon == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    ctx.Remove(zakon);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Kriterij uspješno obrisan";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja kriterija: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var kriterij = ctx.NatjecajKriterijOcjenjivanja
                .AsNoTracking()
                .Where(d => d.Id == Id)
                .FirstOrDefault();
            if (kriterij == null)
            {
                return NotFound($"Ne postoji zakon sa tim Id {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(kriterij);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                NatjecajKriterijOcjenjivanja kriterij = await ctx.NatjecajKriterijOcjenjivanja.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (kriterij == null)
                {
                    return NotFound($"Ne postoji kriterij sa tim Id {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<NatjecajKriterijOcjenjivanja>(kriterij, "", d => d.Id, d => d.EvidencijskiBr,
                    d => d.Opis, d => d.Ponder);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Kriterij uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(kriterij);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o kriteriju nije moguće povezati s forme");
                    return View(kriterij);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }


    }
}
