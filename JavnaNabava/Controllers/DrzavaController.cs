﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class DrzavaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public DrzavaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Drzava drzava)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(drzava);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Država { drzava.Iso} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(drzava);
                }
            }
            else
            {
                return View(drzava);
            }
        }

        [HttpGet]
        public IActionResult Edit(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            var drzava = ctx.Drzava.AsNoTracking().Where(d => d.Iso == Iso).FirstOrDefault();
            if (drzava == null)
            {
                return NotFound($"Ne postoji država sa tim ISO {Iso}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(drzava);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Drzava drzava = await ctx.Drzava.Where(d => d.Iso == Iso).FirstOrDefaultAsync();
                if (drzava == null)
                {
                    return NotFound($"Ne postoji država sa tim ISO {Iso}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<Drzava>(drzava, "", d => d.Iso, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Država { drzava.Iso} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(drzava);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o državi nije moguće povezati s forme");
                    return View(drzava);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Iso, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string Iso, int page = 1, int sort = 1, bool ascending = true)
        {
            var drzava = ctx.Drzava.Find(Iso);
            if (drzava == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = drzava.Naziv;
                    ctx.Remove(drzava);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Država { drzava.Naziv} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja države: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Drzava.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Drzava, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Iso;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var drzave = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new DrzaveViewModel
            {
                Drzave = drzave,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
