﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class KonzorcijController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="optionsSnapshot"></param>
        public KonzorcijController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = ctx.Konzorcij.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if(page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Konzorcij, object>> orderSelector = null;
            switch (sort) {
                case 1:
                    orderSelector = k => k.Naziv;
                    break;
                case 2:
                    orderSelector = k => k.AdresaSjedista;
                    break;
                case 3:
                    orderSelector = k => k.FkMjesto;
                    break;
                case 4:
                    orderSelector = k => k.FkVrstaKonzorcija;
                    break;
            }
            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var konzorciji = query
                                .Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var mjesto = ctx.Mjesto.ToList();

            var sifVrstaKonzorcija = ctx.SifVrstaKonzorcija.ToList();

            var konzorcij = from e in konzorciji
                            join d in mjesto on e.FkMjesto equals d.PostanskiBroj into table1
                            from d in table1.ToList()
                            join i in sifVrstaKonzorcija on e.FkVrstaKonzorcija equals i.Id into table2
                            from i in table2.ToList()
                            select new KonzorcijDTO {
                                konzorcij = e,
                                mjesto = d,
                                sifVrstaKonzorcija = i
                            };

            var model = new KonzorcijViewModel {
                Dto = konzorcij,
                PagingInfo = pagingInfo
            };

            return View("Konzorcij", model);
        }

        [HttpGet]
        public IActionResult Create() {

            PrepareDropDownList();

            return View("CreateKonzorcijForm");
        }

        [HttpPost]
        public IActionResult ProcessCreate(Konzorcij konzorcij) {

            Konzorcij kon = ctx.Konzorcij.SingleOrDefault(k => k.Oib == konzorcij.Oib);

            if (kon == null) {
                try {
                    ctx.Add(konzorcij);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Konzorcij {konzorcij.Naziv} uspješno dodan.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction("Index");
                } catch (Exception e) {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    PrepareDropDownList();
                    return View("CreateKonzorcijForm", konzorcij);
                }
            } else {

                try {
                    kon.Oib = konzorcij.Oib;
                    kon.Naziv = konzorcij.Naziv;
                    kon.AdresaSjedista = konzorcij.AdresaSjedista;
                    kon.FkMjesto = konzorcij.FkMjesto;
                    kon.FkVrstaKonzorcija = konzorcij.FkVrstaKonzorcija;

                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = false;
                    PrepareDropDownList();
                    return View("CreateKonzorcijForm", konzorcij);
                }

            }

        }

        public IActionResult Delete(string oib) {

            var kt = ctx.KonzorcijTvrtka.Where(kt => kt.FkOibkonzorcij == oib).ToList();

            foreach(var element in kt){
                ctx.Remove(element);
                ctx.SaveChanges();
            }


            var konzorcij = ctx.Konzorcij
                                .Find(oib);

            if (konzorcij == null) {
                TempData[Constants.Message] = "konzorcij je null";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index");
            } else {
                try {
                    string naziv = konzorcij.Naziv;
                    ctx.Remove(konzorcij);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Konzorcij {naziv} uspješno obrisan.";
                    TempData[Constants.ErrorOccurred] = false;

                } catch (Exception e) {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja. " + e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction("Index");
            }


        }

        public IActionResult AddTvrtka(string oib) {

            var konzorciji = ctx.Konzorcij
                                .Select(k => new { k.Naziv, k.Oib })
                                .ToList();

            ViewBag.FkOibkonzorcij = new SelectList(konzorciji, "Oib", "Naziv");

            var tvrtke = ctx.Tvrtka
                            .Select(t => new { t.Naziv, t.Oib })
                            .ToList();

            ViewBag.FkOibtvrtka = new SelectList(tvrtke, "Oib", "Naziv");

            var konzorcijTvrtka = new KonzorcijTvrtka {
                FkOibkonzorcij = oib
            };

            return View("AddTvrtkaToKonzorcijForm", konzorcijTvrtka);

        }

        [HttpPost]
        public IActionResult ProcessAddTvrtka(KonzorcijTvrtka konzorcijTvrtka) {

            ctx.Add(konzorcijTvrtka);
            ctx.SaveChanges();
            TempData[Constants.Message] = "Tvrtka uspješno dodana u konzorcij.";
            TempData[Constants.ErrorOccurred] = false;

            return RedirectToAction("Details", konzorcijTvrtka.FkOibkonzorcij);

        }

        private void PrepareDropDownList() {
            var mjesto = ctx.Mjesto
                               .OrderBy(m => m.Naziv)
                               .Select(m => new { m.Naziv, m.PostanskiBroj })
                               .ToList();

            ViewBag.FkMjesto = new SelectList(mjesto, "PostanskiBroj", "Naziv");


            var sifVrstaKonzorcija = ctx.SifVrstaKonzorcija
                                        .OrderBy(sif => sif.Naziv)
                                        .Select(sif => new { sif.Naziv, sif.Id })
                                        .ToList();

            ViewBag.FkVrstaKonzorcija = new SelectList(sifVrstaKonzorcija, "Id", "Naziv");
        }

        public IActionResult Edit(string oib) {
            var konzorcij = ctx.Konzorcij
                                .Find(oib);

            if (konzorcij == null) {
                TempData[Constants.Message] = "konzorcij je null";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index");
            } else {
                PrepareDropDownList();
                return View("CreateKonzorcijForm", konzorcij);
            }
            
        }

        public async Task<IActionResult> Edit2(string oib) {
            var konzorcij = await ctx.Konzorcij.Where(k => k.Oib == oib)
                                                     .Include(k => k.FkMjestoNavigation)
                                                     .Include(k => k.FkVrstaKonzorcijaNavigation)
                                                     .Select(k => new KonzorcijViewModel2 {
                                                         Oib = k.Oib,
                                                         Naziv = k.Naziv,
                                                         AdresaSjedista = k.AdresaSjedista,
                                                         FkMjesto = k.FkMjestoNavigation.Naziv,
                                                         FkVrstaKonzorcija = k.FkVrstaKonzorcijaNavigation.Naziv
                                                     })
                                                     .FirstOrDefaultAsync();

            if (konzorcij == null) {
                TempData[Constants.Message] = "konzorcij je null";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index2");
            } else {

                var tvrtkeOIB = await ctx.KonzorcijTvrtka.Where(t => t.FkOibkonzorcij == oib)
                                                         .Include(t => t.FkOibtvrtkaNavigation)
                                                         .ThenInclude(t => t.FkMjestoNavigation)
                                                         .Include(t => t.FkOibtvrtkaNavigation)
                                                         .ThenInclude(t => t.JeDjelatnostis)
                                                         .ThenInclude(d => d.IdNavigation)
                                                         .ToListAsync();

                var tvrtke = new List<TvrtkaViewModel2>();

                foreach (KonzorcijTvrtka tvrtka in tvrtkeOIB) {
                    tvrtke.Add(new TvrtkaViewModel2 {
                        Oib = tvrtka.FkOibtvrtka,
                        Naziv = tvrtka.FkOibtvrtkaNavigation.Naziv,
                        Adresa = tvrtka.FkOibtvrtkaNavigation.Adresa,
                        FkMjesto = tvrtka.FkOibtvrtkaNavigation.FkMjestoNavigation.Naziv,
                        Djelatnost = tvrtka.FkOibtvrtkaNavigation.JeDjelatnostis.FirstOrDefault().IdNavigation.Naziv
                    });
                }

                konzorcij.Tvrtke = tvrtke;

                //foreach(PropertyDescriptor descriptor in TypeDescriptor.GetProperties(konzorcij)) {
                //    string name = descriptor.Name;
                //    object value = descriptor.GetValue(konzorcij);
                //    Console.WriteLine("{0}={1}", name, value);
                //    if(name == "Tvrtke") {
                //        foreach(TvrtkaViewModel2 m in konzorcij.Tvrtke) {
                //            Console.WriteLine(m.Naziv);
                //        }
                //    }
                //}

                return View("Edit", konzorcij);
            }

        }

        public IActionResult Details(string oib) {

            var konzorciji = ctx.Konzorcij.Where(k => k.Oib == oib).ToList();
            var konzorcijTvrtka = ctx.KonzorcijTvrtka.ToList();
            var tvrtke = ctx.Tvrtka.ToList();
            var mjesto = ctx.Mjesto.ToList();
            var sifDjelatnost = ctx.SifDjelatnostTvrtke.ToList();

            var detalji = from k in konzorciji
                          join kT in konzorcijTvrtka on k.Oib equals kT.FkOibkonzorcij into table1
                          from kT in table1.ToList()
                          join t in tvrtke on kT.FkOibtvrtka equals t.Oib into table2
                          from t in table2.ToList()
                          join m in mjesto on t.FkMjesto equals m.PostanskiBroj into table3
                          from m in table3.ToList()
                          select new KonzorcijDetailsDTO {
                              konzorcij = k,
                              tvrtka = t,
                              konzorcijTvrtka = kT,
                              mjesto = m
                          };

            ViewBag.Naziv = konzorciji.ElementAt(0).Naziv;

            var detalji2 = detalji.ToList();

            for(int i = 0; i < detalji.Count(); i++) {
                var idDjelatnosti = ctx.JeDjelatnosti.Where(d => d.Oib == detalji2[i].tvrtka.Oib).SingleOrDefault();
                var djelatnost = ctx.SifDjelatnostTvrtke.Where(d => d.Id == idDjelatnosti.Id).SingleOrDefault();
                detalji2[i].djelatnost = djelatnost;
            }

            ViewBag.Oib = oib;

            return View("KonzorcijDetails", detalji2);

        }

        public IActionResult Index2(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = ctx.Konzorcij.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index2), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Konzorcij, object>> orderSelector = null;
            switch (sort) {
                case 1:
                    orderSelector = k => k.Naziv;
                    break;
                case 2:
                    orderSelector = k => k.AdresaSjedista;
                    break;
                case 3:
                    orderSelector = k => k.FkMjesto;
                    break;
                case 4:
                    orderSelector = k => k.FkVrstaKonzorcija;
                    break;
            }
            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var konzorciji = query
                                .Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();

            var mjesto = ctx.Mjesto.ToList();

            var sifVrstaKonzorcija = ctx.SifVrstaKonzorcija.ToList();

            var konzorcij = from e in konzorciji
                            join d in mjesto on e.FkMjesto equals d.PostanskiBroj into table1
                            from d in table1.ToList()
                            join i in sifVrstaKonzorcija on e.FkVrstaKonzorcija equals i.Id into table2
                            from i in table2.ToList()
                            select new KonzorcijDTO {
                                konzorcij = e,
                                mjesto = d,
                                sifVrstaKonzorcija = i
                            };

            var model = new KonzorcijViewModel {
                Dto = konzorcij,
                PagingInfo = pagingInfo
            };

            return View("Konzorcij2", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oib"></param>
        /// <returns></returns>
        public IActionResult Details2(string oib) {

            var konzorciji = ctx.Konzorcij.Where(k => k.Oib == oib).ToList();
            var konzorcijTvrtka = ctx.KonzorcijTvrtka.ToList();
            var tvrtke = ctx.Tvrtka.ToList();
            var mjesto = ctx.Mjesto.ToList();
            var sifDjelatnost = ctx.SifDjelatnostTvrtke.ToList();

            var detalji = from k in konzorciji
                          join kT in konzorcijTvrtka on k.Oib equals kT.FkOibkonzorcij into table1
                          from kT in table1.ToList()
                          join t in tvrtke on kT.FkOibtvrtka equals t.Oib into table2
                          from t in table2.ToList()
                          join m in mjesto on t.FkMjesto equals m.PostanskiBroj into table3
                          from m in table3.ToList()
                          select new KonzorcijDetailsDTO {
                              konzorcij = k,
                              tvrtka = t,
                              konzorcijTvrtka = kT,
                              mjesto = m
                          };

            ViewBag.Naziv = konzorciji.ElementAt(0).Naziv;

            var detalji2 = detalji.ToList();

            for (int i = 0; i < detalji.Count(); i++) {
                var idDjelatnosti = ctx.JeDjelatnosti.Where(d => d.Oib == detalji2[i].tvrtka.Oib).SingleOrDefault();
                var djelatnost = ctx.SifDjelatnostTvrtke.Where(d => d.Id == idDjelatnosti.Id).SingleOrDefault();
                detalji2[i].djelatnost = djelatnost;
            }

            ViewBag.Oib = oib;

            return View("KonzorcijDetails2", detalji2);

        }

        [HttpPost]
        public async Task<IActionResult> ProcessEdit(KonzorcijViewModel2 konzorcij) {

            try {
                var konz = await ctx.Konzorcij.Include(k => k.KonzorcijTvrtkas)
                                              .ThenInclude(k => k.FkOibtvrtkaNavigation)
                                              .Where(k => k.Oib == konzorcij.Oib)
                                              .FirstOrDefaultAsync();
                konz.Naziv = konzorcij.Naziv;
                konz.AdresaSjedista = konzorcij.AdresaSjedista;
                var fkmjesto = await ctx.Mjesto.Where(m => m.Naziv == konzorcij.FkMjesto).FirstOrDefaultAsync();
                konz.FkMjesto = fkmjesto.PostanskiBroj;
                var dj = await ctx.SifVrstaKonzorcija.Where(d => d.Naziv == konzorcij.FkVrstaKonzorcija).FirstOrDefaultAsync();
                konz.FkVrstaKonzorcija = dj.Id;

                List<string> oibiTvrtki = konzorcij.Tvrtke
                                               .Where(t => t.Oib != "")
                                               .Select(s => s.Oib)
                                               .ToList();

                ctx.RemoveRange(konz.KonzorcijTvrtkas.Where(s => !oibiTvrtki.Contains(s.FkOibtvrtka)));

                var tvrtkeUKonzorciju = await ctx.KonzorcijTvrtka.Where(k => k.FkOibkonzorcij == konzorcij.Oib)
                                                                 .Select(s => s.FkOibtvrtka)
                                                                 .ToListAsync();

                foreach (var tvrtka in konzorcij.Tvrtke) {

                    Tvrtka novaTvrtka;
                    if (tvrtkeUKonzorciju.Contains(tvrtka.Oib)) {
                        novaTvrtka = konz.KonzorcijTvrtkas.Where(k => k.FkOibtvrtka == tvrtka.Oib).FirstOrDefault().FkOibtvrtkaNavigation;
                    } else {

                        novaTvrtka = await ctx.Tvrtka.Where(t => t.Oib == tvrtka.Oib).FirstOrDefaultAsync();

                        if (novaTvrtka == null) {
                            novaTvrtka = new Tvrtka();
                            novaTvrtka.Oib = tvrtka.Oib;
                            await ctx.Tvrtka.AddAsync(novaTvrtka);
                        }

                        konz.KonzorcijTvrtkas.Add(new KonzorcijTvrtka {
                            FkOibkonzorcij = konzorcij.Oib,
                            FkOibtvrtka = novaTvrtka.Oib
                        });

                    }

                    
                    novaTvrtka.Naziv = tvrtka.Naziv;
                    novaTvrtka.Adresa = tvrtka.Adresa;

                    var mjesto = await ctx.Mjesto.Where(m => m.Naziv == tvrtka.FkMjesto).FirstOrDefaultAsync();
                    novaTvrtka.FkMjesto = mjesto.PostanskiBroj;

                    var djelatnost = await ctx.SifDjelatnostTvrtke.Where(d => d.Naziv == tvrtka.Djelatnost).FirstOrDefaultAsync();
                    if(djelatnost == null) {
                        await ctx.SifDjelatnostTvrtke.AddAsync(new SifDjelatnostTvrtke {
                            Naziv = tvrtka.Djelatnost
                        });
                        await ctx.SaveChangesAsync();
                        djelatnost = await ctx.SifDjelatnostTvrtke.Where(d => d.Naziv == tvrtka.Djelatnost).FirstOrDefaultAsync();
                    }

                    var jeDjelatnosti = await ctx.JeDjelatnosti.Where(d => d.Oib == tvrtka.Oib).FirstOrDefaultAsync();

                    if(jeDjelatnosti == null) {

                        await ctx.JeDjelatnosti.AddAsync(new JeDjelatnosti {
                            Id = djelatnost.Id,
                            Oib = novaTvrtka.Oib
                        });

                    } else {
                        ctx.JeDjelatnosti.Remove(jeDjelatnosti);
                        await ctx.SaveChangesAsync();
                        jeDjelatnosti.Id = djelatnost.Id;
                        jeDjelatnosti.Oib = tvrtka.Oib;
                        await ctx.JeDjelatnosti.AddAsync(jeDjelatnosti);
                    }

                    
                    await ctx.SaveChangesAsync();
                }

                
            } catch(Exception e) {
                TempData[Constants.Message] = e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction("Index2");
            }

            TempData[Constants.Message] = "Konzorcij uspješno ažuriran.";
            TempData[Constants.ErrorOccurred] = false;

            return RedirectToAction("edit2", konzorcij);
        }

    }

}
