﻿using JavnaNabava;
using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class NatjecajStrucnjaciController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public NatjecajStrucnjaciController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public async Task<IActionResult> CreateAsync()
        {
            await PrepareDropdownLists();
            return View();
        }

        private Task PrepareDropdownLists()
        {
            var strucnaSprema = ctx.SifStrucnaSprema.OrderBy(d => d.Naziv).Select(d => new { d.Naziv, d.Id }).ToList();
            ViewBag.StrucnaSprema = new SelectList(strucnaSprema, "Id", "Naziv");

            return Task.CompletedTask;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.NatjecajStrucnjaci.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<NatjecajStrucnjaci, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = m => m.Id;
                    break;
                case 2:
                    orderSelector = m => m.OpisKriterija;
                    break;
                case 3:
                    orderSelector = m => m.BrPotrebnihStrucnjaka;
                    break;
                case 4:
                    orderSelector = m => m.EvidencijskiBr;
                    break;
                case 5:
                    orderSelector = m => m.IdStrucnaSpremaNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var strucnjaci = query
                .Select(m => new NatjecajStrucnjaciViewModel
                {
                    Id = m.Id,
                    OpisKriterija = m.OpisKriterija,
                    BrPotrebnihStrucnjaka = m.BrPotrebnihStrucnjaka,
                    EvidencijskiBr = m.EvidencijskiBr,
                    StrucnaSprema = m.IdStrucnaSpremaNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new NatjecajStrucnjaciTableViewModel
            {
                NatjecajStrucnjaci = strucnjaci,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


    [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(NatjecajStrucnjaci natjecajStrucnjaci)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(natjecajStrucnjaci);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Potražnja za stručnjacima { natjecajStrucnjaci.IdStrucnaSpremaNavigation.Naziv} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(natjecajStrucnjaci);
                }
            }
            else
            {
                await PrepareDropdownLists();
                return View(natjecajStrucnjaci);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var natjecajStrucnjaci = ctx.NatjecajStrucnjaci.AsNoTracking().Where(m => m.Id == Id).FirstOrDefault();
            if (natjecajStrucnjaci != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(natjecajStrucnjaci);
            }
            else
            {
                return NotFound($"Neispravan Id stručnjaka: {Id}");
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                NatjecajStrucnjaci strucnjaci = await ctx.NatjecajStrucnjaci.Where(m => m.Id == Id).FirstOrDefaultAsync();
                if (strucnjaci == null)
                {
                    return NotFound($"Ne postoji kontakt sa ID: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<NatjecajStrucnjaci>(strucnjaci, "", m => m.Id, m => m.OpisKriterija, m => m.BrPotrebnihStrucnjaka,
                    m => m.EvidencijskiBr, m => m.IdStrucnaSprema);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Zahtjev za stručnjake uspješno ažuriran";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(strucnjaci);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o stručnjacima nije moguće povezati s forme");
                    return View(strucnjaci);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var strucnjaci = await ctx.NatjecajStrucnjaci.FindAsync(Id);
            if (strucnjaci != null)
            {
                try
                {
                    ctx.Remove(strucnjaci);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Zahtjev za stručnjake obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja zahtjeva za stručnjake: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoje stručnjaci";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }

    }
}
