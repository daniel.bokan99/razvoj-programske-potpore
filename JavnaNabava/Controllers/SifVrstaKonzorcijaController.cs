﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class SifVrstaKonzorcijaController : Controller {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifVrstaKonzorcijaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifVrstaKonzorcija.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifVrstaKonzorcija, object>> orderSelector = null;
            if (sort == 1) {
                orderSelector = sif => sif.Naziv;
            }

            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var djelatnosti = query.Skip((page - 1) * pagesize)
                              .Take(appSettings.PageSize)
                              .ToList();

            var model = new SifVrstaKonzorcijaViewModel {
                SifVrstaKonzorcija = djelatnosti,
                PagingInfo = pagingInfo
            };

            return View("SifVrstaKonzorcija", model);
        }

        public IActionResult Create() {
            return View("AddSifVrstaKonzorcija");
        }

        public IActionResult Edit(int Id) {

            var vrsta = ctx.SifVrstaKonzorcija.Find(Id);

            return View("AddSifVrstaKonzorcija", vrsta);
        }

        public IActionResult Delete(int Id) {

            try {
                var vrsta = ctx.SifVrstaKonzorcija.Find(Id);
                ctx.Remove(vrsta);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Vrsta konzorcija {vrsta.Naziv} uspješno izbrisana.";
                TempData[Constants.ErrorOccurred] = false;
            } catch (Exception e) {
                TempData[Constants.Message] = "Greška prilikom brisanja vrste konzorcija: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }

            return RedirectToAction("Index");
        }

        public IActionResult ProcessCreate(SifVrstaKonzorcija vrsta) {

            var vk = ctx.SifVrstaKonzorcija.SingleOrDefault(d => d.Naziv == vrsta.Naziv);

            if (vk == null) {

                try {
                    string naziv = vrsta.Naziv;
                    ctx.Add(vrsta);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta konzorcija {naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddSifVrstaKonzorcija", vrsta);

                }

            } else {

                try {
                    vk.Naziv = vk.Naziv;
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Vrsta konzorcija {vk.Naziv} uspješno ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddSifVrstaKonzorcija", vrsta);
                }

            }

        }
    }
}
