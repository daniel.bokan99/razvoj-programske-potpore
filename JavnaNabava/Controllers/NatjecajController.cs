﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class NatjecajController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public NatjecajController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public async Task<IActionResult> CreateAsync()
        {
            await PrepareDropdownLists();
            return View();
        }

        private Task PrepareDropdownLists()
        {
            var stavkaNabave = ctx.StavkaNabave.OrderBy(d => d.EvidencijskiBroj).Select(d => new { d.PredmetNabave, d.Id }).ToList();

            var vrstaUgovora = ctx.SifVrstaUgovora.OrderBy(d => d.Naziv).Select(d => new { d.Naziv, d.Id }).ToList();
            var djelatnostTvrtke = ctx.SifDjelatnostTvrtke.OrderBy(d => d.Id).Select(d => new { d.Naziv, d.Id }).ToList();

            ViewBag.StavkaNabave = new SelectList(stavkaNabave, "Id", "PredmetNabave");
            ViewBag.VrstaUgovora = new SelectList(vrstaUgovora, "Id", "Naziv");
            ViewBag.DjelatnostTvrtke = new SelectList(djelatnostTvrtke, "Id", "Naziv");

            return Task.CompletedTask;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Natjecaj.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Natjecaj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = n => n.EvidencijskiBr;
                    break;
                case 2:
                    orderSelector = n => n.Naziv;
                    break;
                case 3:
                    orderSelector = n => n.IdStavkaNabaveNavigation.PredmetNabave;
                    break;
                case 4:
                    orderSelector = n => n.RokZaDostavu;
                    break;
                case 5:
                    orderSelector = n => n.TrajanjeUgovora;
                    break;
                case 6:
                    orderSelector = n => n.IdVrstaUgovoraNavigation.Naziv;
                    break;
                case 7:
                    orderSelector = n => n.SlanjeElPostom;
                    break;
                case 8:
                    orderSelector = n => n.PopisPotrebnihDokumenata;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var natjecaji = query
                .Select(n => new NatjecajViewModel
                {
                    EvidencijskiBr = n.EvidencijskiBr,
                    Naziv = n.Naziv,
                    NazivStavke = n.IdStavkaNabaveNavigation.PredmetNabave,
                    RokZaDostavu = n.RokZaDostavu,
                    TrajanjeUgovora = n.TrajanjeUgovora,
                    NazivUgovora = n.IdVrstaUgovoraNavigation.Naziv,
                    SlanjeElPostom = n.SlanjeElPostom,
                    PopisPotrebnihDokumenata = n.PopisPotrebnihDokumenata,
                    NazivDjelatnosti = n.IdDjelatnostNavigation.Naziv,
                    JePonisten = n.JePonisten
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            for (int i = 0; i < natjecaji.Count(); i++)
            {
                var idZakon = ctx.NatjecajZakon.Where(d => d.EvidencijskiBr == natjecaji[i].EvidencijskiBr).FirstOrDefault();
                var nazivZakon = ctx.SifZakon.Where(d => d.Id == idZakon.IdZakon).FirstOrDefault();
                natjecaji[i].NazivZakon = nazivZakon.Naziv;
            }

            for (int i = 0; i < natjecaji.Count(); i++)
            {
                var kriterij = ctx.NatjecajKriterijOcjenjivanja.Where(d => d.EvidencijskiBr == natjecaji[i].EvidencijskiBr).FirstOrDefault();
                natjecaji[i].Kriterij = kriterij.Opis;
            }

            var model = new NatjecajiViewModel
            {
                Natjecaji = natjecaji,
                PagingInfo = pagingInfo
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Natjecaj natjecaj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(natjecaj);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"natjecaj { natjecaj.Naziv} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(natjecaj);
                }
            }
            else
            {
                await PrepareDropdownLists();
                return View(natjecaj);
            }
        }


       /* public IActionResult AddStrucnjak(string oib)
        {

            var strucnjak = ctx.NatjecajStrucnjaci
                                .Select(s => new { s.Id, s.OpisKriterija})
                                .ToList();

            ViewBag.FkOibkonzorcij = new SelectList(konzorciji, "Oib", "Naziv");

            var tvrtke = ctx.Tvrtka
                            .Select(t => new { t.Naziv, t.Oib })
                            .ToList();

            ViewBag.FkOibtvrtka = new SelectList(tvrtke, "Oib", "Naziv");

            var konzorcijTvrtka = new KonzorcijTvrtka
            {
                FkOibkonzorcij = oib
            };

            return View("AddTvrtkaToKonzorcijForm", konzorcijTvrtka);

        }*/

        [HttpPost]
        public IActionResult ProcessAddNatjecajStrucnjaci(NatjecajStrucnjaci natjecajStrucnjak)
        {

            ctx.Add(natjecajStrucnjak);
            ctx.SaveChanges();
            TempData[Constants.Message] = "Strucnjak uspješno dodan u natječaj.";
            TempData[Constants.ErrorOccurred] = false;

            return RedirectToAction("Details", natjecajStrucnjak.EvidencijskiBr);

        }


        [HttpGet]
        public IActionResult EditDetail(string EvidencijskiBr, int page = 1, int sort = 1, bool ascending = true, string viewName = nameof(Show))
        {
            return Show(EvidencijskiBr, page, sort, ascending, viewName: nameof(EditDetail));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditDetail(NatjecajViewModel model, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            if (ModelState.IsValid)
            {
                var natjecaj = ctx.Natjecaj
                                             .Include(t => t.NatjecajStrucnjacis)
                                             .Where(t => t.EvidencijskiBr == model.EvidencijskiBr)
                                             .FirstOrDefault();
                if (natjecaj == null)
                {
                    return NotFound($"Ne postoji natječaj s id-om {model.EvidencijskiBr}");
                }
                natjecaj.Naziv = model.Naziv;
                natjecaj.RokZaDostavu = model.RokZaDostavu;
                natjecaj.TrajanjeUgovora = model.Naziv;
                natjecaj.SlanjeElPostom = model.SlanjeElPostom;
                natjecaj.JePonisten = model.JePonisten;
                natjecaj.IdStavkaNabave = ctx.StavkaNabave.Where(o => o.PredmetNabave == model.NazivStavke).Select(o => o.Id).FirstOrDefault();
                natjecaj.IdVrstaUgovora = ctx.SifVrstaUgovora.Where(o => o.Naziv == model.NazivUgovora).Select(o => o.Id).FirstOrDefault();
                natjecaj.PopisPotrebnihDokumenata = model.PopisPotrebnihDokumenata;
                natjecaj.IdDjelatnost = ctx.SifDjelatnostTvrtke.Where(o => o.Naziv == model.NazivDjelatnosti).Select(o => o.Id).FirstOrDefault();

                try
                {
                    List<int> idStrucnjaci = model.NatjecajStrucnjaci
                                              .Where(s => s.Id > 0)
                                              .Select(s => s.Id)
                                              .ToList();
                    var zaBrisanje = natjecaj.NatjecajStrucnjacis.Where(s => !idStrucnjaci.Contains(s.Id));
                    ctx.RemoveRange(zaBrisanje);

                    foreach (var strucnjaci in model.NatjecajStrucnjaci)
                    {
                        NatjecajStrucnjaci noviStrucnjaci;
                        if (strucnjaci.Id > 0)
                        {
                            noviStrucnjaci = natjecaj.NatjecajStrucnjacis.First(s => s.Id == strucnjaci.Id);
                        }
                        else
                        {
                            noviStrucnjaci = new NatjecajStrucnjaci();
                            natjecaj.NatjecajStrucnjacis.Add(noviStrucnjaci);

                        }
                        noviStrucnjaci.IdStrucnaSprema = ctx.SifStrucnaSprema.Where(o => String.Equals(o.Naziv, strucnjaci.StrucnaSprema)).Select(o => o.Id).FirstOrDefault();
                        noviStrucnjaci.BrPotrebnihStrucnjaka = strucnjaci.BrPotrebnihStrucnjaka;
                        noviStrucnjaci.OpisKriterija = strucnjaci.OpisKriterija;
                        noviStrucnjaci.EvidencijskiBr = model.EvidencijskiBr;
                    }

                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Natječaj {natjecaj.EvidencijskiBr} uspješno ažuriran";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(EditDetail),
                        new
                        {
                            id = natjecaj.EvidencijskiBr,
                            page,
                            sort,
                            ascending
                        }
                    );
                }

                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
            System.Diagnostics.Debug.WriteLine(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string EvidencijskiBr, int page = 1, int sort = 1, bool ascending = true)
        {

            var nko = ctx.NatjecajKriterijOcjenjivanja.Where(n => n.EvidencijskiBr == EvidencijskiBr).ToList();

            foreach (var element in nko)
            {
                ctx.Remove(element);
            }

            var ns = ctx.NatjecajStrucnjaci.Where(n => n.EvidencijskiBr == EvidencijskiBr).ToList();
            foreach (var element in ns)
            {
                ctx.Remove(element);
            }

            var nz = ctx.NatjecajZakon.Where(n => n.EvidencijskiBr == EvidencijskiBr).ToList();
            foreach (var element in ns)
            {
                ctx.Remove(element);
            }

            ctx.SaveChanges();

            var natjecaj = await ctx.Natjecaj.FindAsync(EvidencijskiBr);
            if (natjecaj == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = natjecaj.Naziv;
                    ctx.Remove(naziv);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Natječaj { natjecaj.Naziv} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja natječaja: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

         public IActionResult Show(string EvidencijskiBr, int page = 1, int sort = 1, bool ascending = true, string viewName = nameof(Show))
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var natjecaj = ctx.Natjecaj
                .Where(n => n.EvidencijskiBr == EvidencijskiBr)
                .Select(n => new NatjecajViewModel
                {
                    EvidencijskiBr = n.EvidencijskiBr,
                    Naziv = n.Naziv,
                    IdStavkaNabave = n.IdStavkaNabaveNavigation.Id,
                    RokZaDostavu = n.RokZaDostavu,
                    TrajanjeUgovora = n.TrajanjeUgovora,
                    IdVrstaUgovora = n.IdVrstaUgovoraNavigation.Id,
                    SlanjeElPostom = n.SlanjeElPostom,
                    PopisPotrebnihDokumenata = n.PopisPotrebnihDokumenata
                })
                .FirstOrDefault();

            if (natjecaj == null)
            {
                return NotFound($"Natječaj sa {EvidencijskiBr} ne postoji");
            }
            else
            {
                var natjecajstrucnjaci = ctx.NatjecajStrucnjaci
                    .Where(n => n.EvidencijskiBr == natjecaj.EvidencijskiBr)
                    .OrderBy(n => n.Id)
                    .Select(n => new NatjecajStrucnjaciViewModel
                    {
                        Id = n.Id,
                        BrPotrebnihStrucnjaka = n.BrPotrebnihStrucnjaka,
                        OpisKriterija = n.OpisKriterija,
                        EvidencijskiBr = n.EvidencijskiBr,
                        StrucnaSprema = n.IdStrucnaSpremaNavigation.Naziv,
                    })
                    .ToList();
                natjecaj.NatjecajStrucnjaci = natjecajstrucnjaci;
                return View(viewName, natjecaj);
            }
        }

        public IActionResult ShowKriterij(string EvidencijskiBr, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var natjecaj = ctx.Natjecaj
                .Where(n => n.EvidencijskiBr == EvidencijskiBr)
                .Select(n => new NatjecajViewModel
                {
                    EvidencijskiBr = n.EvidencijskiBr,
                    Naziv = n.Naziv,
                    IdStavkaNabave = n.IdStavkaNabaveNavigation.Id,
                    RokZaDostavu = n.RokZaDostavu,
                    TrajanjeUgovora = n.TrajanjeUgovora,
                    IdVrstaUgovora = n.IdVrstaUgovoraNavigation.Id,
                    SlanjeElPostom = n.SlanjeElPostom,
                    PopisPotrebnihDokumenata = n.PopisPotrebnihDokumenata
                })
                .FirstOrDefault();

            if (natjecaj == null)
            {
                return NotFound($"Natječaj sa {EvidencijskiBr} ne postoji");
            }
            else
            {
                var natjecajkriterij = ctx.NatjecajKriterijOcjenjivanja
                    .Where(n => n.EvidencijskiBr == natjecaj.EvidencijskiBr)
                    .OrderBy(n => n.Id)
                    .Select(n => new NatjecajKriterijOcjenjivanjaViewModel
                    {
                        Id = n.Id,
                        EvidencijskiBr = n.EvidencijskiBr,
                        Opis = n.Opis,
                        Ponder = n.Ponder,
                    })
                    .ToList();
                natjecaj.NatjecajKriterij = natjecajkriterij;
                return View(natjecaj);
            }
        }
    }
}
