﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class SifCPVController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifCPVController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("CreateCPV");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifCpv cpvunos)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(cpvunos);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"CPV {cpvunos.Naziv} uspješno dodan.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View("CreateCPV", cpvunos);
                }

            }
            else
            {
                return View("CreateCPV", cpvunos);
            }

        }


        [HttpGet]
        public IActionResult Edit(string cpvsifra, int page = 1, int sort = 1, bool ascending = true)
        {
            var odredenicpv = ctx.SifCpv
                .AsNoTracking()
                .Where(d => d.Cpv == cpvsifra)
                .SingleOrDefault();
            if (odredenicpv == null)
            {
                return NotFound($"Ne postoji CPV s oznakom {cpvsifra}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View("EditSifCPV", odredenicpv);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string Cpv, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifCpv odredenicpv = await ctx.SifCpv.Where(o => o.Cpv == Cpv).FirstOrDefaultAsync();
                //SifCpv odredenicpv = await ctx.SifCpv.FindAsync(cpvsifra);
                if (odredenicpv == null)
                {
                    return NotFound($"Ne postoji CPV s oznakom {Cpv}");
                }
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifCpv>(odredenicpv, "", d => d.Cpv, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"CPV {odredenicpv.Naziv} uspješno ažuriran.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View("EditSifCPV", odredenicpv);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o CPV-u nije moguće povezati s forme");
                    return View("EditSifCPV", odredenicpv);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Cpv, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string cpv, int page = 1, int sort = 1, bool ascending = true)
        {
            var odredenicpv = ctx.SifCpv.Find(cpv);
            if (odredenicpv == null)
            {
                return NotFound("Nisam ga pronasao");
            }
            else
            {
                try
                {
                    string naziv = odredenicpv.Naziv;
                    ctx.Remove(odredenicpv);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"CPV {naziv} uspješno obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = $"Pogreška prilikom brisanja CPV-a." + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifCpv.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<JavnaNabava.Model.SifCpv, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Cpv;
                    break;
                case 2:
                    orderSelector = d => d.Naziv;
                    break;
                
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }


            var cpvovi = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();
            var model = new CPVoviViewModel
            {
                CPVovi = (System.Collections.Generic.IEnumerable<SifCpv>)cpvovi,
                PagingInfo = pagingInfo
            };
            return View("SifCPV", model);
        }
    }
}
