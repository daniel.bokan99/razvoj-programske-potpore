﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class StavkaTroskovnikaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public StavkaTroskovnikaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View();
        }

        private void PrepareDropDownLists()
        {
            var sifre = ctx.SifJedMjere
                            .Select(d => new { d.Naziv, d.Id })
                            .OrderBy(d => d.Naziv)
                            .ToList();
            var natjecaji = ctx.TroskovnikNatjecaj
                            .Select(d => new { d.Opis, d.Id })
                            .OrderBy(d => d.Opis)
                            .ToList();
            ViewBag.Sifre = new SelectList(sifre, nameof(SifJedMjere.Id), nameof(SifJedMjere.Naziv));
            ViewBag.Natjecaji = new SelectList(natjecaji, nameof(TroskovnikNatjecaj.Id), nameof(TroskovnikNatjecaj.Opis));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(StavkaTroskovnika stavka)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(stavka);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Mjera {stavka.Opis} uspješno dodana. Id stavke = {stavka.Id}";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View(stavka);
                }
            }
            else
            {
                PrepareDropDownLists();
                return View(stavka);
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.StavkaTroskovnika.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<StavkaTroskovnika, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Opis;
                    break;
                case 2:
                    orderSelector = d => d.Kolicina;
                    break;
                case 3:
                    orderSelector = d => d.FkJedMjereNavigation.Naziv;
                    break;
                case 4:
                    orderSelector = d => d.FkTroskovnikNatjecajNavigation.Naslov;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var stavke = query
                              .Select(s => new StavkaTroskovnikaViewModel
                              {
                                  Id = s.Id,
                                  Naslov = s.FkTroskovnikNatjecajNavigation.Naslov,
                                  Kolicina = s.Kolicina,
                                  Naziv = s.FkJedMjereNavigation.Naziv,
                                  Opis = s.Opis
                              })
                              .Skip((page - 1) * pagesize)
                              .Take(pagesize)
                              .ToList();
            var model = new StavkeTroskovnikaViewModel
            {
                stavke = stavke,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int Id)
        {
            var pst = ctx.PonudaStavkaTroskovnik.Where(pst => pst.FkNatjecajStavkaTroskovnik == Id).ToList();

            foreach(var element in pst)
            {
                ctx.Remove(element);
                ctx.SaveChanges();
            }

            var stavka = await ctx.StavkaTroskovnika.FindAsync(Id);
            if (stavka != null)
            {
                try 
                {
                    string naziv = stavka.Opis;
                    ctx.Remove(stavka);
                    await ctx.SaveChangesAsync();
                    var result = new
                    {
                        message = $"{naziv} sa šifrom {Id} obrisano",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = $"Pogreška prilikom brisanja stavke troškovnika {exc.CompleteExceptionMessage()}",
                        successful = true
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Stavka troškovnika sa šifrom {Id} ne postoji");
            }
        }

        [HttpGet]
        
        public async Task<IActionResult> Edit(int id)
        {
            var pst = ctx.PonudaStavkaTroskovnik.Where(pst => pst.FkNatjecajStavkaTroskovnik == id).ToList();

            foreach (var element in pst)
            {
                ctx.Update(element);
                ctx.SaveChanges();
            }
            var stavka = await ctx.StavkaTroskovnika.FindAsync(id);
            if (stavka != null)
            {
                PrepareDropDownLists();
                return PartialView(stavka);
            }
            else
            {
                return NotFound($"Neispravan id stavke: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(StavkaTroskovnika stavka)
        {
            if (stavka == null)
            {
                return NotFound("Nema poslanih podataka");
            }

            bool checkId = ctx.StavkaTroskovnika.Any(s => s.Id == stavka.Id);
            if (!checkId)
            {
                return NotFound($"Nispravan id stavke: {stavka.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(stavka);
                    ctx.SaveChanges();
                    return StatusCode(302, Url.Action(nameof(Row), new { id = stavka.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return PartialView(stavka);
                }

            }
            else
            {
                PrepareDropDownLists();
                return PartialView(stavka);
            }
        }
        public PartialViewResult Row(int id)
        {
            var stavka = ctx.StavkaTroskovnika
                            .Where(s => s.Id == id)
                            .Select(s => new StavkaTroskovnikaViewModel
                            {
                                Id = s.Id,
                                Naslov = s.FkTroskovnikNatjecajNavigation.Naslov,
                                Kolicina = s.Kolicina,
                                Naziv = s.FkJedMjereNavigation.Naziv,
                                Opis = s.Opis
                            })
                            .SingleOrDefault();
            if (stavka != null)
            {
                return PartialView(stavka);
            }
            else
            {
                return PartialView("ErrorMessageRow", $"Neispravan id stavke : {id}");
            }
        }
    }
}
