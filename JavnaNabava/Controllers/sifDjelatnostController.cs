﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class sifDjelatnostController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public sifDjelatnostController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = ctx.SifDjelatnostTvrtke.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifDjelatnostTvrtke, object>> orderSelector = null;
            if(sort == 1) {
                orderSelector = sif => sif.Naziv;
            }

            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var djelatnosti = query.Skip((page - 1) * pagesize)
                              .Take(appSettings.PageSize)
                              .ToList();

            var model = new SifDjelatnostViewModel {
                SifDjelatnost = djelatnosti,
                PagingInfo = pagingInfo
            };

            return View("SifDjelatnost", model);
        }

        public IActionResult Create() {
            return View("AddDjelatnost");
        }

        public IActionResult ProcessCreate(SifDjelatnostTvrtke djelatnost) {

            var djel = ctx.SifDjelatnostTvrtke.SingleOrDefault(d => d.Naziv == djelatnost.Naziv);

            if(djel == null) {

                try {
                    string naziv = djelatnost.Naziv;
                    ctx.Add(djelatnost);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Djelatnost {naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch(Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddDjelatnost", djelatnost);

                }

            } else {

                try {
                    djel.Naziv = djelatnost.Naziv;
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Djelatnost {djelatnost.Naziv} uspješno ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                } catch(Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddDjelatnost", djelatnost);
                }
                
            }

        }

        public IActionResult Edit(int Id) {

            var djelatnost = ctx.SifDjelatnostTvrtke.Find(Id);

            return View("AddDjelatnost", djelatnost);
        }

        public IActionResult Delete(int Id) {

            try {
                var djelatnost = ctx.SifDjelatnostTvrtke.Find(Id);
                ctx.Remove(djelatnost);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Djelatnost {djelatnost.Naziv} uspješno izbrisana.";
                TempData[Constants.ErrorOccurred] = false;
            } catch(Exception e) {
                TempData[Constants.Message] = "Greška prilikom brisanja djelatnosti: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }
            
            return RedirectToAction("Index");
        }
    }
}
