﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class AutoCompleteController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appData;

        public AutoCompleteController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        public async Task<IEnumerable<IdLabel>> Mjesto(string term)
        {
            var query = ctx.Mjesto
                .Select(m => new IdLabel
                {
                    Id = m.PostanskiBroj,
                    Label = m.PostanskiBroj + " " + m.Naziv
                })
                .Where(l => l.Label.Contains(term));

          var list = await query.OrderBy(l => l.Label)
                .ThenBy(l => l.Id)
                .Take(appData.AutoCompleteCount)
                .ToListAsync();
          return list;
        }

        public async Task<IEnumerable<IdLabel>> Tvrtka(string term)
        {
            var query = ctx.Tvrtka
                .Select(t => new IdLabel
                {
                    Id = Convert.ToInt64(t.Oib),
                    Label = t.Naziv + " (" + t.Oib + ")"
                })
                .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                .ThenBy(l => l.Id)
                .Take(appData.AutoCompleteCount)
                .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<IdLabel>> Funkcija(string term)
        {
            var query = ctx.SifFunkcija
                  .Select(f => new IdLabel
                  {
                      Id = f.Id,
                      Label = f.Naziv
                  })
                  .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                  .ThenBy(l => l.Id)
                  .Take(appData.AutoCompleteCount)
                  .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<IdLabel>> Strucnasprema(string term)
        {
            var query = ctx.SifStrucnaSprema
                  .Select(s => new IdLabel
                  {
                      Id = s.Id,
                      Label = s.Naziv
                  })
                  .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                  .ThenBy(l => l.Id)
                  .Take(appData.AutoCompleteCount)
                  .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<AutoCompleteArtikl>> Kontakt(string term)
        {
            var query = ctx.Kontakti
                  .Select(k => new AutoCompleteArtikl
                  {
                      Id = k.Id,
                      Label = k.KontaktPodatci,
                      Naziv = k.FkSifKontakta.Naziv,
                      FkKontaktId = k.FkSifKontakta.Id
                  })
                  .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                  .ThenBy(l => l.Id)
                  .Take(appData.AutoCompleteCount)
                  .ToListAsync();
            return list;
        }
    }
}
