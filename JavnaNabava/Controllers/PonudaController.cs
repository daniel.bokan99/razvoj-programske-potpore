﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class PonudaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public PonudaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        private Task getViewBags()
        {
            var vrste = ctx.SifVrstaPonuditelja.OrderBy(d => d.Id).Select(d => new { d.Id, d.Naziv }).ToList();
            var tvrtke = ctx.Tvrtka.OrderBy(d => d.Oib).Select(d => new { d.Oib, d.Naziv }).ToList();
            var konzorciji = ctx.Konzorcij.OrderBy(d => d.Oib).Select(d => new { d.Oib, d.Naziv }).ToList();

            ViewBag.vrste = new SelectList(vrste, nameof(SifVrstaPonuditelja.Id), nameof(SifVrstaPonuditelja.Naziv));
            ViewBag.tvrtke = new SelectList(tvrtke, nameof(Tvrtka.Oib), nameof(Tvrtka.Naziv));
            ViewBag.konzorciji = new SelectList(konzorciji, nameof(Konzorcij.Oib), nameof(Konzorcij.Naziv));

            return Task.CompletedTask;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Ponuda.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Ponudum, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.Id;
                    break;
                case 2:
                    orderSelector = p => p.UkupnaCijenaPonude;
                    break;
                case 3:
                    orderSelector = p => p.FkVrstaPonuditeljaId;
                    break;
                case 4:
                    orderSelector = p => p.FkTvrtkaOib;
                    break;
                case 5:
                    orderSelector = p => p.FkKonzorcijOib;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var ponude = query.Select(p => new PonudaViewModel
            {
                Id = p.Id,
                ukupnaCijenaPonude = p.UkupnaCijenaPonude,
                vrstaPonuditelja = p.FkVrstaPonuditelja.Naziv,
                tvrtka = p.FkTvrtkaOib.Length > 0 ? p.FkTvrtkaOibNavigation.Naziv : "-",
                konzorcij = p.FkKonzorcijOib.Length > 0 ? p.FkKonzorcijOibNavigation.Naziv : "-",


            }).Skip((page - 1) * pagesize)
                                .Take(appSettings.PageSize)
                                .ToList();
            foreach (PonudaViewModel ponuda in ponude)
            {
                var ponudaDokumenti = ctx.Dokument
                    .Where(p => p.FkPonudaId == ponuda.Id)
                    .OrderBy(p => p.FkPonudaId)
                    .Select(p => new DokumentiViewModel
                    {
                        urudzbeniBr = p.UrudzbeniBr,
                        naslov = p.Naslov,
                        datumDokumenta = p.DatumDokumenta,
                        fileBinary = p.FileBinary,
                        ponuda = p.FkPonudaId.HasValue ? p.FkPonudaId.ToString() : "-",
                        ponudaStrucnjak = p.FkPonudaStrucnjakId.HasValue ? p.FkPonudaStrucnjak.FkOsobaOibNavigation.Ime + p.FkPonudaStrucnjak.FkOsobaOibNavigation.Prezime : "-",
                        natjecaj = p.FkNatjecajDokumentEvBr.Length > 0 ? p.FkNatjecajDokumentEvBrNavigation.Naziv : "-",
                        vrsta = p.FkVrstaDokumenta.Naziv,
                    })
                    .ToList();
                ponuda.PonudaDokumenti = ponudaDokumenti;
            }

            var model = new PonudeViewModel
            {
                Ponude = ponude,
                PagingInfo = pagingInfo
            };

            return View("Ponuda", model);
        }


        [HttpGet]
        public IActionResult Create()
        {

            getViewBags();

            return View("CreatePonudaForm");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessCreate(Ponudum ponuda)
        {

            Ponudum pon = ctx.Ponuda.SingleOrDefault(pon => pon.Id == ponuda.Id);

            if (pon == null)
            {
                try
                {
                    ctx.Add(ponuda);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuda je uspješno stvorena.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    getViewBags();
                    return View("CreatePonudaForm", ponuda);
                }
            }
            else
            {

                try
                {
                    pon.Id = ponuda.Id;
                    pon.UkupnaCijenaPonude = ponuda.UkupnaCijenaPonude;
                    pon.FkVrstaPonuditeljaId = ponuda.FkVrstaPonuditeljaId;
                    pon.FkTvrtkaOib = ponuda.FkTvrtkaOib;
                    pon.FkKonzorcijOib = ponuda.FkKonzorcijOib;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.CompleteExceptionMessage());
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = false;
                    getViewBags();
                    return View("CreatePonudaForm", ponuda);
                }

            }

        }
        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {

            var ponuda = ctx.Ponuda.Include(k => k.FkTvrtkaOibNavigation).Include(k => k.FkKonzorcijOibNavigation).Include(k => k.FkVrstaPonuditelja).Where(p=> p.Id == Id).FirstOrDefault();
            if (ponuda != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                getViewBags();
                var ponudaView = new PonudaViewModel
                {
                    Id = ponuda.Id,
                    ukupnaCijenaPonude = ponuda.UkupnaCijenaPonude,
                    vrstaPonuditelja = ponuda.FkVrstaPonuditelja.Naziv,
                    tvrtka = ponuda.FkTvrtkaOibNavigation.Naziv,
                    konzorcij = ponuda.FkKonzorcijOibNavigation.Naziv
                };


                return View("EditPonuda", ponudaView);
            }
            else
            {
                return NotFound($"Neispravan id vrste: {Id}");
            }
        }


        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {

                Ponudum ponuda = await ctx.Ponuda.Include(k => k.FkTvrtkaOibNavigation).Include(k => k.FkKonzorcijOibNavigation).Include(k => k.FkVrstaPonuditelja).Where(p => p.Id == Id).Where(d => d.Id == Id).FirstOrDefaultAsync();
                var ponudaView = new PonudaViewModel
                {
                    Id = ponuda.Id,
                    ukupnaCijenaPonude = ponuda.UkupnaCijenaPonude,
                    vrstaPonuditelja = ponuda.FkVrstaPonuditelja.Naziv,
                    tvrtka = ponuda.FkTvrtkaOibNavigation.Naziv,
                    konzorcij = ponuda.FkKonzorcijOibNavigation.Naziv
                };
                if (ponuda == null)
                {
                    return NotFound($"Ne postoji ponuda s Id: {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<Ponudum>(ponuda, "", d => d.Id, d => d.UkupnaCijenaPonude, d => d.FkVrstaPonuditeljaId, d => d.FkTvrtkaOib, d => d.FkKonzorcijOib);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Ponuda s Id: { ponuda.Id} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(ponudaView);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Greška prilikom ažuriranja ponude");
                    return View(ponudaView);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var tp = ctx.TroskovnikPonuda.Where(o => o.FkPonudaId == Id).ToList();

            foreach (var element in tp)
            {
                var st = ctx.PonudaStavkaTroskovnik.Where(o => o.FkTroskovnikPonuda == element.Id).ToList();
                foreach (var stElement in st)
                {
                    ctx.Remove(stElement);
                }
                ctx.Remove(element);
            }


            ctx.SaveChanges();



            var ponuda = ctx.Ponuda.Find(Id);
            if (ponuda != null)
            {
                try
                {
                    int naziv = ponuda.Id;
                    ctx.Remove(ponuda);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuda sa Id: {naziv} je obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ponude: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji dokument s id: {Id}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }

        public IActionResult Show(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;


            var ponuda = ctx.Ponuda
                .Where(p => p.Id == Id)
                .Select(p => new PonudaViewModel
                {
                    Id = p.Id,
                    ukupnaCijenaPonude = p.UkupnaCijenaPonude,
                    vrstaPonuditelja = p.FkVrstaPonuditelja.Naziv,
                    tvrtka = p.FkTvrtkaOib.Length > 0 ? p.FkTvrtkaOibNavigation.Naziv : "-",
                    konzorcij = p.FkKonzorcijOib.Length > 0 ? p.FkKonzorcijOibNavigation.Naziv : "-",

                })
                .FirstOrDefault();
            if (ponuda == null)
            {
                return NotFound($"Ponuda sa Id: {Id} ne postoji");
            }
            else
            {
                var ponudaDokumenti = ctx.Dokument
                    .Where(p => p.FkPonudaId == ponuda.Id)
                    .OrderBy(p => p.FkPonudaId)
                    .Select(p => new DokumentiViewModel
                    {
                        urudzbeniBr = p.UrudzbeniBr,
                        naslov = p.Naslov,
                        datumDokumenta = p.DatumDokumenta,
                        fileBinary = p.FileBinary,
                        ponuda = p.FkPonudaId.HasValue ? p.FkPonudaId.ToString() : "-",
                        ponudaStrucnjak = p.FkPonudaStrucnjakId.HasValue ? p.FkPonudaStrucnjak.FkOsobaOibNavigation.Ime + p.FkPonudaStrucnjak.FkOsobaOibNavigation.Prezime : "-",
                        natjecaj = p.FkNatjecajDokumentEvBr.Length > 0 ? p.FkNatjecajDokumentEvBrNavigation.Naziv : "-",
                        vrsta = p.FkVrstaDokumenta.Naziv,
                    })
                    .ToList();
                ponuda.PonudaDokumenti = ponudaDokumenti;
                return View(ponuda);
            }
        }
    }
}
