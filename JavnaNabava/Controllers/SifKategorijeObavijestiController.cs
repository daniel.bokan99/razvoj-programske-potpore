﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.Models;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers {
    public class SifKategorijeObavijestiController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public SifKategorijeObavijestiController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true) {

            int pagesize = appSettings.PageSize;
            var query = ctx.SifKategorijeObavijesti.AsNoTracking();
            int count = query.Count();

            var pagingInfo = new PagingInfo {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages) {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifKategorijeObavijesti, object>> orderSelector = null;
            if (sort == 1) {
                orderSelector = sif => sif.Naziv;
            }

            if (orderSelector != null) {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            var kategorije = query.Skip((page - 1) * pagesize)
                              .Take(appSettings.PageSize)
                              .ToList();

            var model = new SifKategorijaObavijestiViewModel {
                SifKategorijeObavijesti = kategorije,
                PagingInfo = pagingInfo
            };

            return View("SifKategorijeObavijesti", model);
        }

        public IActionResult Create() {
            return View("AddSifKategorijeObavijesti");
        }

        public IActionResult Edit(int Id) {
            var kategorija = ctx.SifKategorijeObavijesti.Find(Id);
            return View("AddSifKategorijeObavijesti", kategorija);
        }

        public IActionResult Delete(int Id) {

            try {
                var kategorije = ctx.SifKategorijeObavijesti.Find(Id);
                ctx.Remove(kategorije);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Kategorija {kategorije.Naziv} uspješno izbrisana.";
                TempData[Constants.ErrorOccurred] = false;
            }catch(Exception e) {
                TempData[Constants.Message] = "Greška prilikom brisanja kategorije obavijesti: " + e.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult ProcessCreate(SifKategorijeObavijesti obavijest) {
            var ob = ctx.SifKategorijeObavijesti.SingleOrDefault(o => o.Naziv == obavijest.Naziv);

            if(ob == null) {
                try {
                    ctx.Add(obavijest);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Kategorija {obavijest.Naziv} uspješno dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                }catch(Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddSifKategorijeObavijesti", obavijest);
                }
            } else {
                try {
                    ob.Naziv = obavijest.Naziv;
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Kategorija {obavijest.Naziv} uspješno ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction("Index");
                }catch(Exception e) {
                    TempData[Constants.Message] = e.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    return View("AddSifKategorijeObavijesti", obavijest);
                }
            }
        }
    }
}
