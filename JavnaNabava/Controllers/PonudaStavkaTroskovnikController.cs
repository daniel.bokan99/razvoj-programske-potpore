﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class PonudaStavkaTroskovnikController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public PonudaStavkaTroskovnikController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View();
        }

        private void PrepareDropDownLists()
        {
            var ponude = ctx.TroskovnikPonuda
                            .Select(d => new { d.Naslov, d.Id })
                            .OrderBy(d => d.Naslov)
                            .ToList();
            var stavke = ctx.StavkaTroskovnika
                            .Select(d => new { d.Opis, d.Id })
                            .OrderBy(d => d.Opis)
                            .ToList();
            ViewBag.Stavke = new SelectList(stavke, nameof(StavkaTroskovnika.Id), nameof(StavkaTroskovnika.Opis));
            ViewBag.Ponude = new SelectList(ponude, nameof(TroskovnikPonuda.Id), nameof(TroskovnikPonuda.Naslov));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PonudaStavkaTroskovnik ponuda)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(ponuda);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = "Uspješno dodano";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View(ponuda);
                }
            }
            else
            {
                PrepareDropDownLists();
                return View(ponuda);
            }
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.PonudaStavkaTroskovnik.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<PonudaStavkaTroskovnik, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.JedinicnaCijena;
                    break;
                case 2:
                    orderSelector = d => d.UkupnaCijenaBezPdv;
                    break;
                case 3:
                    orderSelector = d => d.Pdv;
                    break;
                case 4:
                    orderSelector = d => d.UkupnaCijenaSaPdv;
                    break;
                case 5:
                    orderSelector = d => d.FkTroskovnikPonudaNavigation.Naslov;
                    break;
                case 6:
                    orderSelector = d => d.FkNatjecajStavkaTroskovnikNavigation.Opis;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var ponuda = query
                              .Select(s => new PonudaStavkaTroskovnikViewModel
                              {
                                  Id = s.Id,
                                  JedinicnaCijena = s.JedinicnaCijena,
                                  UkupnaCijenaBezPDV = s.UkupnaCijenaBezPdv,
                                  UkupnaCijenaSaPDV = s.UkupnaCijenaSaPdv,
                                  PDV = s.Pdv,
                                  Naslov = s.FkTroskovnikPonudaNavigation.Naslov,
                                  Opis = s.FkNatjecajStavkaTroskovnikNavigation.Opis

                              })
                              .Skip((page - 1) * pagesize)
                              .Take(pagesize)
                              .ToList();
            var model = new PonudaStavkeTroskovnikViewModel
            {
                ponuda = ponuda,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var ponuda = await ctx.PonudaStavkaTroskovnik.FindAsync(id);
            if (ponuda != null)
            {
                try
                {
                    
                    ctx.Remove(ponuda);
                    await ctx.SaveChangesAsync();
                    var result = new
                    {
                        message = $"Obrisano, id: {id}",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = $"Pogreška prilikom brisanja ponude stavke troškovnika {exc.CompleteExceptionMessage()}",
                        successful = true
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Ponuda stavke troškovnika sa šifrom {id} ne postoji");
            }

        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var ponuda = await ctx.PonudaStavkaTroskovnik.FindAsync(id);
            if (ponuda != null)
            {
                PrepareDropDownLists();
                return PartialView(ponuda);
            }
            else
            {
                return NotFound($"Neispravan id ponude stavke: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PonudaStavkaTroskovnik ponuda)
        {
            if (ponuda == null)
            {
                return NotFound("Nema poslanih podataka");
            }

            bool checkId = ctx.PonudaStavkaTroskovnik.Any(s => s.Id == ponuda.Id);
            if (!checkId)
            {
                return NotFound($"Nispravan id stavke: {ponuda.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(ponuda);
                    ctx.SaveChanges();
                    return StatusCode(302, Url.Action(nameof(Row), new { id = ponuda.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return PartialView(ponuda);
                }

            }
            else
            {
                PrepareDropDownLists();
                return PartialView(ponuda);
            }
        }

        public PartialViewResult Row(int id)
        {
            var ponuda = ctx.PonudaStavkaTroskovnik
                            .Where(s => s.Id == id)
                            .Select(s => new PonudaStavkaTroskovnikViewModel
                            {
                                Id = s.Id,
                                JedinicnaCijena = s.JedinicnaCijena,
                                UkupnaCijenaBezPDV = s.UkupnaCijenaBezPdv,
                                UkupnaCijenaSaPDV = s.UkupnaCijenaSaPdv,
                                PDV = s.Pdv,
                                Naslov = s.FkTroskovnikPonudaNavigation.Naslov,
                                Opis = s.FkNatjecajStavkaTroskovnikNavigation.Opis
                            })
                            .SingleOrDefault();
            if (ponuda != null)
            {
                return PartialView(ponuda);
            }
            else
            {
                return PartialView("ErrorMessageRow", $"Neispravan id stavke : {id}");
            }
        }
    }
}

