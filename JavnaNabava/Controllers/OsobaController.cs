﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class OsobaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly ILogger<OsobaController> logger;
        private readonly AppSettings appSettings;

        public OsobaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot, ILogger<OsobaController> logger)
        {
            this.ctx = ctx;
            this.logger = logger;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            OsobaViewModel osoba = new OsobaViewModel();
            return View(osoba);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OsobaViewModel osoba)
        {
            logger.LogTrace(JsonSerializer.Serialize(osoba));
            if (ModelState.IsValid)
            {
                Osoba o = new Osoba
                {
                    Oib = osoba.Oib,
                    Ime = osoba.Ime,
                    Prezime = osoba.Prezime,
                    FkTvrtka = osoba.FkTvrtka,
                    FkFunkcijaId = osoba.FkFunkcijaId,
                    Adresa = osoba.Adresa,
                    FkStrucnaSprema = osoba.FkStrucnaSprema,
                    FkMjesto = osoba.FkMjesto
                };
                try
                {
                    ctx.Add(o);
                    ctx.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Osoba {o.Ime} dodana.");

                    TempData[Constants.Message] = $"Osoba { o.Ime} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                } catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanje nove osobe: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(osoba);
                }
            }
            else
            {
                return View(osoba);
            }
        }

        [HttpGet]
        public IActionResult Edit(string Oib, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var osoba = ctx.Osoba
                .Where(o => o.Oib == Oib)
                .Select(o => new OsobaViewModel
                {
                    Oib = o.Oib,
                    Ime = o.Ime,
                    Prezime = o.Prezime,
                    FkTvrtka = o.FkTvrtka,
                    NazivTvrtke = o.FkTvrtkaNavigation.Naziv,
                    FkFunkcijaId = o.FkFunkcijaId,
                    NazivFunkcije = o.FkFunkcija.Naziv,
                    Adresa = o.Adresa,
                    FkStrucnaSprema = o.FkStrucnaSprema,
                    NazivStrucneSpreme = o.FkStrucnaSpremaNavigation.Naziv,
                    FkMjesto = o.FkMjesto,
                    NazivMjesta = o.FkMjestoNavigation.Naziv

                })
                .FirstOrDefault();
            if (osoba == null)
            {
                return NotFound($"Osoba sa {Oib} ne posotji");
            }
            else
            {
                var osobakontakt = ctx.OsobaKontakt
                    .Where(o => o.FkOsobaOib == osoba.Oib)
                    .OrderBy(o => o.FkKontaktId)
                    .Select(o => new OsobaKontaktViewModel
                    {
                        FkKontaktId = o.FkKontaktId,
                        KontaktId = o.FkKontakt.Id,
                        KontaktPodatci = o.FkKontakt.KontaktPodatci,
                        NazivTipKontakta = o.FkKontakt.FkSifKontakta.Naziv
                    })
                    .ToList();
                osoba.OsobaKontakti = osobakontakt;
                return View(osoba);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(OsobaViewModel osoba, int page = 1, int sort = 1, bool ascending = true)
        {
            var osoba2 = await ctx.Osoba.FirstOrDefaultAsync();
            if (ModelState.IsValid)
            {
                osoba2 = await ctx.Osoba
                    .Include(o => o.OsobaKontakts)
                    .Where(o => o.Oib == osoba.Oib)
                    .FirstOrDefaultAsync();
                if (osoba2 == null)
                {
                    return NotFound("Ne postoji osoba s oib-om: " + osoba.Oib);
                }
                List<int> idKontakta = osoba.OsobaKontakti
                                          .Where(ok => ok.KontaktId > 0)
                                          .Select(ok => ok.KontaktId)
                                          .ToList();
                ctx.RemoveRange(osoba2.OsobaKontakts.Where(ok => !idKontakta.Contains(ok.FkKontaktId)));

                foreach (var osobaKontakt in osoba.OsobaKontakti)
                {
                    //ažuriraj postojeće i dodaj nove
                    OsobaKontakt novaOsobaKontakt;// potpuno nova ili dohvaćena ona koju treba izmijeniti
                    Kontakti k;
                    List<int> kontakti = await ctx.OsobaKontakt
                        .Select(k => k.FkKontaktId)
                        .ToListAsync();
                    if (kontakti.Contains(osobaKontakt.FkKontaktId))
                    {
                        k = await ctx.Kontakti
                            .Where(k => k.Id == osobaKontakt.KontaktId)
                            .FirstOrDefaultAsync();
                    }
                    else
                    {
                        novaOsobaKontakt = new OsobaKontakt();
                        novaOsobaKontakt.FkKontaktId = osobaKontakt.KontaktId;
                        novaOsobaKontakt.FkOsobaOib = osoba.Oib;
                        osoba2.OsobaKontakts.Add(novaOsobaKontakt);

                        k = new Kontakti();
                    }
                    k.KontaktPodatci = osobaKontakt.KontaktPodatci;
                }
            }

            try
            {
                var osobakontakt = ctx.OsobaKontakt
                    .Where(o => o.FkOsobaOib == osoba.Oib)
                    .OrderBy(o => o.FkKontaktId)
                    .Select(o => new OsobaKontaktViewModel
                    {
                        FkKontaktId = o.FkKontaktId,
                        KontaktId = o.FkKontakt.Id,
                        KontaktPodatci = o.FkKontakt.KontaktPodatci,
                        NazivTipKontakta = o.FkKontakt.FkSifKontakta.Naziv
                    })
                    .ToList();
                osoba.OsobaKontakti = osobakontakt;

                var Oib = await ctx.Osoba.FindAsync(osoba.Oib);
                if (osoba == null)
                {
                    return NotFound($"Nema poslanih podataka");
                }
                else if (Oib == null)
                {
                    return NotFound($"Ne postoji osoba sa Oib-om {Oib}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                Osoba osoba3 = await ctx.Osoba.Where(d => d.Oib == osoba.Oib).FirstOrDefaultAsync();
                bool ok = await TryUpdateModelAsync<Osoba>(osoba3, "", o => o.Oib, o => o.Ime, o => o.Prezime, o => o.FkTvrtka,
                o => o.FkFunkcijaId, o => o.Adresa, o => o.FkStrucnaSprema, o => o.FkMjesto);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Osoba { osoba.Ime} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Edit), new { osoba.Oib, page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(osoba);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o osobi nije moguće povezati s forme");
                    return View(osoba);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return View(osoba);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string Oib, int page = 1, int sort = 1, bool ascending = true)
        {

            var ps = ctx.PonudaStrucnjak.Where(o => o.FkOsobaOib == Oib).ToList();

            foreach (var element in ps)
            {
                ctx.Remove(element);
            }

            var ok = ctx.OsobaKontakt.Where(o => o.FkOsobaOib == Oib).ToList();
            foreach(var element in ok)
            {
                ctx.Remove(element);
            }
            ctx.SaveChanges();

            var osoba = await ctx.Osoba.FindAsync(Oib);
            if (osoba == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = osoba.Ime;
                    ctx.Remove(osoba);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Osoba { osoba.Ime} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                } catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja osobe: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Osoba.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Osoba, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = o => o.Oib;
                    break;
                case 2:
                    orderSelector = o => o.Ime;
                    break;
                case 3:
                    orderSelector = o => o.Prezime;
                    break;
                case 4:
                    orderSelector = o => o.FkTvrtkaNavigation.Naziv;
                    break;
                case 5:
                    orderSelector = o => o.FkFunkcija.Naziv;
                    break;
                case 6:
                    orderSelector = o => o.Adresa;
                    break;
                case 7:
                    orderSelector = o => o.FkStrucnaSpremaNavigation.Naziv;
                    break;
                case 8:
                    orderSelector = o => o.FkMjestoNavigation.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var osobe = query
                .Select(o => new OsobaViewModel
                {
                    Oib = o.Oib,
                    Ime = o.Ime,
                    Prezime = o.Prezime,
                    NazivTvrtke = o.FkTvrtkaNavigation.Naziv,
                    NazivFunkcije = o.FkFunkcija.Naziv,
                    Adresa = o.Adresa,
                    NazivStrucneSpreme = o.FkStrucnaSpremaNavigation.Naziv,
                    NazivMjesta = o.FkMjestoNavigation.Naziv
                })
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            foreach (OsobaViewModel osoba in osobe)
            {
                var osobakontakt = ctx.OsobaKontakt
                    .Where(o => o.FkOsobaOib == osoba.Oib)
                    .OrderBy(o => o.FkKontaktId)
                    .Select(o => new OsobaKontaktViewModel
                    {
                        FkKontaktId = o.FkKontaktId,
                        KontaktId = o.FkKontakt.Id,
                        KontaktPodatci = o.FkKontakt.KontaktPodatci,
                        NazivTipKontakta = o.FkKontakt.FkSifKontakta.Naziv
                    })
                    .ToList();
                osoba.OsobaKontakti = osobakontakt;
            }
            
            var model = new OsobeViewModel
            {
                Osobe = osobe,
                PagingInfo = pagingInfo
            };

            return View(model);
        }

        public IActionResult Show(string Oib, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var osoba = ctx.Osoba
                .Where(o => o.Oib == Oib)
                .Select(o => new OsobaViewModel
                {
                    Oib = o.Oib,
                    Ime = o.Ime,
                    Prezime = o.Prezime,
                    NazivTvrtke = o.FkTvrtkaNavigation.Naziv,
                    NazivFunkcije = o.FkFunkcija.Naziv,
                    Adresa = o.Adresa,
                    NazivStrucneSpreme = o.FkStrucnaSpremaNavigation.Naziv,
                    NazivMjesta = o.FkMjestoNavigation.Naziv

                })
                .FirstOrDefault();
            if (osoba == null)
            {
                return NotFound($"Osoba sa {Oib} ne posotji");
            }
            else
            {
                var osobakontakt = ctx.OsobaKontakt
                    .Where(o => o.FkOsobaOib == osoba.Oib)
                    .OrderBy(o => o.FkKontaktId)
                    .Select(o => new OsobaKontaktViewModel
                    {
                        FkKontaktId = o.FkKontaktId,
                        KontaktId = o.FkKontakt.Id,
                        KontaktPodatci = o.FkKontakt.KontaktPodatci,
                        NazivTipKontakta = o.FkKontakt.FkSifKontakta.Naziv
                    })
                    .ToList();
                osoba.OsobaKontakti = osobakontakt;
                return View(osoba);
            }
        }
    }
}
