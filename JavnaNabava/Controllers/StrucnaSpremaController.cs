﻿using JavnaNabava.Exceptions;
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class StrucnaSpremaController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public StrucnaSpremaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifStrucnaSprema strucnaSprema)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(strucnaSprema);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Stručna sprema { strucnaSprema.Naziv} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(strucnaSprema);
                }
            }
            else
            {
                return View(strucnaSprema);
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var strucnaSprema = ctx.SifStrucnaSprema.AsNoTracking().Where(d => d.Id == Id).FirstOrDefault();
            if (strucnaSprema == null)
            {
                return NotFound($"Ne postoji stručna sprema sa tim Id {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(strucnaSprema);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifStrucnaSprema strucnaSprema = await ctx.SifStrucnaSprema.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (strucnaSprema == null)
                {
                    return NotFound($"Ne postoji stručna sprema sa tim Id {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifStrucnaSprema>(strucnaSprema, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"stručna sprema { strucnaSprema.Naziv} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(strucnaSprema);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o stručnoj spremi nije moguće povezati s forme");
                    return View(strucnaSprema);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var strucnaSprema = ctx.SifStrucnaSprema.Find(Id);
            if (strucnaSprema == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = strucnaSprema.Naziv;
                    ctx.Remove(strucnaSprema);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Stručna sprema { strucnaSprema.Naziv} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja stručne spreme: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifStrucnaSprema.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifStrucnaSprema, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var strucneSpreme = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new StrucneSpremeViewModel
            {
                StrucneSpreme = strucneSpreme,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
