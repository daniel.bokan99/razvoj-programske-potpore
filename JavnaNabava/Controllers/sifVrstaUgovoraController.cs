﻿using System.Linq;
using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using JavnaNabava.Exceptions;
using JavnaNabava.ViewModels;

namespace JavnaNabava.Controllers
{
    public class sifVrstaUgovoraController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public sifVrstaUgovoraController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.SifVrstaUgovora.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<SifVrstaUgovora, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var vrstaUgovora = query
                .Skip((page - 1) * pagesize)
                .Take(pagesize)
                .ToList();

            var model = new SifVrsteUgovoraViewModel
            {
                SifVrstaUgovora = vrstaUgovora,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SifVrstaUgovora vrstaUgovora)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(vrstaUgovora);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Vrsta ugovora { vrstaUgovora.Naziv} uspješno dodana";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(vrstaUgovora);
                }
            }
            else
            {
                return View(vrstaUgovora);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var vrstaUgovora = ctx.SifVrstaUgovora.Find(Id);
            if (vrstaUgovora == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string naziv = vrstaUgovora.Naziv;
                    ctx.Remove(vrstaUgovora);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Vrsta ugovora {naziv} uspješno obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja zakona: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            var vrstaUgovora = ctx.SifVrstaUgovora
                .AsNoTracking()
                .Where(d => d.Id == Id)
                .FirstOrDefault();
            if (vrstaUgovora == null)
            {
                return NotFound($"Ne postoji vrsta ugovora sa tim Id {Id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(vrstaUgovora);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                SifVrstaUgovora vrstaUgovora = await ctx.SifVrstaUgovora.Where(d => d.Id == Id).FirstOrDefaultAsync();
                if (vrstaUgovora == null)
                {
                    return NotFound($"Ne postoji vrsta ugovora sa tim Id {Id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                bool ok = await TryUpdateModelAsync<SifVrstaUgovora>(vrstaUgovora, "", d => d.Id, d => d.Naziv);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Vrsta ugovora { vrstaUgovora.Naziv} uspješno ažurirana";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(vrstaUgovora);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o vrsti ugovora nije moguće povezati s forme");
                    return View(vrstaUgovora);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { Id, page, sort, ascending });
            }

        }


    }
}
