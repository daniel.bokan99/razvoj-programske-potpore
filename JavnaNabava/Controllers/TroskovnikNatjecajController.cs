﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Controllers
{
    public class TroskovnikNatjecajController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;
        public TroskovnikNatjecajController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        [HttpGet]
        public IActionResult Create()
        {
            PrepareDropDownLists();
            return View();
        }

        private void PrepareDropDownLists()
        {
            var natj = ctx.TroskovnikNatjecaj
                            .Select(d => new { d.FkNatjecajEvBrNavigation.Naziv, d.FkNatjecajEvBr })
                            .OrderBy(d => d.Naziv)
                            .ToList();

            ViewBag.TrosNatj = new SelectList(natj, nameof(TroskovnikNatjecaj.FkNatjecajEvBr), nameof(TroskovnikNatjecaj.FkNatjecajEvBrNavigation.Naziv));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TroskovnikNatjecaj stavka)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(stavka);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuda {stavka.Naslov} uspješno dodana. Id ponude = {stavka.Id}";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return View(stavka);
                }
            }
            else
            {
                PrepareDropDownLists();
                return View(stavka);
            }

        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.TroskovnikNatjecaj.AsNoTracking();

            int count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<TroskovnikNatjecaj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.Naslov;
                    break;
                case 2:
                    orderSelector = d => d.Opis;
                    break;
                case 3:
                    orderSelector = d => d.FkNatjecajEvBrNavigation.Naziv;
                    break;
                case 4:
                    orderSelector = d => d.Id;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var stavke = query
                              .Select(s => new TroskovnikNatjecajViewModel
                              {
                                  Id = s.Id,
                                  Naslov = s.Naslov,
                                  Opis = s.Opis,
                                  NazivNat = s.FkNatjecajEvBrNavigation.Naziv
                              })
                              .Skip((page - 1) * pagesize)
                              .Take(pagesize)
                              .ToList();
            var model = new TroskovnikNatjecajaViewModel
            {
                natjecaji = stavke,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]

        public IActionResult Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            var stavka = ctx.TroskovnikNatjecaj.AsNoTracking().Where(o => o.Id == id).FirstOrDefault();
            if (stavka != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                PrepareDropDownLists();
                return View(stavka);
            }
            else
            {
                return NotFound($"Neispravan id ponude: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TroskovnikNatjecaj stavka)
        {
            if (stavka == null)
            {
                return NotFound("Nema poslanih podataka");
            }

            bool checkId = ctx.TroskovnikNatjecaj.Any(s => s.Id == stavka.Id);
            if (!checkId)
            {
                return NotFound($"Nispravan id ponude: {stavka.Id}");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(stavka);
                    ctx.SaveChanges();
                    return StatusCode(302, Url.Action(nameof(Row), new { id = stavka.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    PrepareDropDownLists();
                    return PartialView(stavka);
                }

            }
            else
            {
                PrepareDropDownLists();
                return PartialView(stavka);
            }
        }
        public PartialViewResult Row(int id)
        {
            var stavka = ctx.TroskovnikNatjecaj
                            .Where(s => s.Id == id)
                            .Select(s => new TroskovnikNatjecajViewModel
                            {
                                Id = s.Id,
                                Naslov = s.Naslov,
                                Opis = s.Opis,
                                NazivNat = s.FkNatjecajEvBrNavigation.Naziv
                            })
                           .SingleOrDefault();
            if (stavka != null)
            {
                return PartialView(stavka);
            }
            else
            {
                return PartialView("ErrorMessageRow", $"Neispravan id ponude : {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int Id)
        {

            var trosnatj = await ctx.TroskovnikNatjecaj.FindAsync(Id);
            if (trosnatj != null)
            {
                try
                {
                    List<StavkaTroskovnika> stavke_troskovnika = await ctx.StavkaTroskovnika.Where(o => o.FkTroskovnikNatjecaj == trosnatj.Id).ToListAsync();
                    if (stavke_troskovnika.Count > 0)
                    {
                        foreach (var stavka in stavke_troskovnika)
                        {
                            List<PonudaStavkaTroskovnik> ponude_stavki = await ctx.PonudaStavkaTroskovnik.Where(o => o.FkNatjecajStavkaTroskovnik == stavka.Id).ToListAsync();
                            if (ponude_stavki.Count > 0)
                            {
                                foreach (var ponuda in ponude_stavki)
                                {
                                    ctx.Remove(ponuda);
                                    await ctx.SaveChangesAsync();
                                }
                            }
                        }
                        foreach (var stavka in stavke_troskovnika)
                        {
                            ctx.Remove(stavka);
                            await ctx.SaveChangesAsync();
                        }
                    }
                    string naziv = trosnatj.Naslov;
                    ctx.Remove(trosnatj);
                    await ctx.SaveChangesAsync();
                    var result = new
                    {
                        message = $"{naziv} sa šifrom {Id} obrisano",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = $"Pogreška prilikom brisanja {exc.CompleteExceptionMessage()}",
                        successful = true
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Ponuda sa id-om: {Id} ne postoji");
            }
        }


        public IActionResult Show(int Id, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var troskovnik_natjecaj = ctx.TroskovnikNatjecaj
                .Where(o => o.Id == Id)
                .Select(o => new TroskovnikNatjecajViewModel
                {
                    Id = o.Id,
                    Naslov = o.Naslov,
                    Opis = o.Opis,
                    NazivNat = o.FkNatjecajEvBrNavigation.Naziv,
                    
                })
                .FirstOrDefault();
            if (troskovnik_natjecaj == null)
            {
                return NotFound($"Osoba sa id-om {Id} ne posotji");
            }
            else
            {
                var stavka = ctx.StavkaTroskovnika
                    .Where(o => o.FkTroskovnikNatjecaj == troskovnik_natjecaj.Id)
                    .OrderBy(o => o.Opis)
                    .Select(o => new StavkaTroskovnikaViewModel
                    {
                        Id = o.Id,
                        Naslov = o.FkTroskovnikNatjecajNavigation.Naslov,
                        Kolicina = o.Kolicina,
                        Naziv = o.FkJedMjereNavigation.Naziv,
                        Opis = o.Opis
                    })
                    .ToList();
                troskovnik_natjecaj.stavke = stavka;
                return View(troskovnik_natjecaj);
            }
        }
    }
}
