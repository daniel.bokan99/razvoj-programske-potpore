﻿$(document).on('click', '.deleterow', function () {
    console.log("tu sam")
    event.preventDefault();
    var tr = $(this).parents("tr");
    tr.remove();
    clearOldMessage();
});

$(function () {
    $(".form-control").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
        }
    });

    $("#tvrtka-naziv, #tvrtka-adresa").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            dodajArtikl();
        }
    });


    $("#tvrtka-dodaj").click(function () {
        event.preventDefault();
        dodajArtikl();
    });
});

function dodajArtikl() {
    var oib = $("#tvrtka-oib").val();
    if (oib != '') {
        if ($("[name='Tvrtka[" + oib + "].Oib'").length > 0) {
            alert('Artikl je već u dokumentu');
            return;
        }

        //var kolicina = parseFloat($("#tvrtka-naziv").val().replace(',', '.')); //treba biti točka, a ne zarez za parseFloat
        //if (isNaN(kolicina))
        //    kolicina = 1;

        //var rabat = parseFloat($("#artikl-rabat").val().replace(',', '.')); //treba točka, a ne zarez za parseFloat
        //if (isNaN(rabat))
        //    rabat = 0;

        //var cijena = parseFloat($("#artikl-cijena").val());
        var template = $('#novaTvrtkaTemplate').html();
        var naziv = $("#tvrtka-naziv").val();
        var mjesto = $("#tvrtka-mjesto").val();
        var adresa = $("#tvrtka-adresa").val();
        var djelatnost = $("#tvrtka-djelatnost").val();
        //var iznos = kolicina * cijena * (1 - rabat);
        //iznos = iznos.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' kn';
        //var cijena_formatirana = cijena.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' kn'

        //Alternativa ako su hr postavke sa zarezom //http://haacked.com/archive/2011/03/19/fixing-binding-to-decimals.aspx/
        //ili ovo http://intellitect.com/custom-model-binding-in-asp-net-core-1-0/

        template = template.replace(/--Oib--/g, oib)
            .replace(/--Naziv--/g, naziv)
            .replace(/--Mjesto--/g, mjesto)
            .replace(/--Adresa--/g, adresa)
            .replace(/--Djelatnost--/g, djelatnost)
        $(template).find('tr').insertBefore($("#table-tvrtke").find('tr').last());

        $("#tvrtka-oib").val('');
        $("#tvrtka-naziv").val('');
        $("#tvrtka-mjesto").val('');
        $("#tvrtka-adresa").val('');
        $("#tvrtka-djelatnost").val('');

        clearOldMessage();
    }
}