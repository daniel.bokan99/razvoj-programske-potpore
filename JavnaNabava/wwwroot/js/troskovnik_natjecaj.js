﻿$(document).on('click', '.deleterow', function () {
    event.preventDefault();
    let tr = $(this).parents('tr');
    tr.remove();
    clearOldMessage();
})

$(function () {
    $(".form-control").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
        }
    });

    $("#stavka_troskovnika-kolicina").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            dodajStavkuTroskovnika();
        }
    });


    $("#stavka_troskovnika-dodaj").click(function () {
        event.preventDefault();
        dodajStavkuTroskovnika();
    });
});

function dodajStavkuTroskovnika() {
    var sifra = $("#stavkatroskovnika-id").val();
    if (sifra != '') {
        if ($("[name='Stavke[" + sifra + "].Id'").length > 0) {
            alert('Stavka je već u troškovniku');
            return;
        }

        var opis = $("#stavka_troskovnika-opis").val();
        var template = $('#template').html();
        var naziv = $("#stavkatroskovnika-naziv").val();
        var kolicina = $("#stavka_troskovnika-kolicina").val();

        template = template.replace(/--sifra--/g, sifra)
            .replace(/--kolicina--/g, kolicina)
            .replace(/--opis--/g, opis)
            .replace(/--naziv--/g, naziv)
        $(template).find('tr').insertBefore($("#table-stavke").find('tr').last());

        $("#stavkatroskovnika-id").val('');
        $("#stavka_troskovnika-kolicina").val('');
        $("#stavka_troskovnika-opis").val('');
        $("#stavkatroskovnika-naziv").val('');

        clearOldMessage();
    }
}