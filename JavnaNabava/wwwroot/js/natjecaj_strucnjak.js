﻿$(document).on('click', '.deleterow', function () {
    event.preventDefault();
    let tr = $(this).parents('tr');
    tr.remove();
    clearOldMessage();
})

$(function () {
    $("#ns-dodaj").click(function () {
        event.preventDefault();
        dodajStrucnjaka();
    });
});

function dodajStrucnjaka() {
    var sifra = $("#ns-vrijednost").val();
    if (sifra != '') {
        if ($("[name='Ns[" + sifra + "].Id'").length > 0) {
            alert('Stavka je već u troškovniku');
            return;
        }
        console.log(sifra)
        console.log(opiskriterija)
        console.log(brpotrebnihstrucnjaka)
        console.log(strucnasprema)

        var opiskriterija = $("#ns-naziv").val();
        var template = $('#template').html();
        var brpotrebnihstrucnjaka = $('#ns-brojstrucnjaka').val();
        var strucnasprema = $("#ns-strucnasprema").val();

        template = template.replace(/--opis--/g, sifra)
            .replace(/--kolicina--/g, kolicina)
            .replace(/--naziv--/g, opis)
        $(template).find('tr').insertBefore($("#table-stavke").find('tr').last());

        $("#ns-vrijednost").val('');
        $("#ns-naziv").val('');
        $("#ns-brojstrucnjaka").val('');
        $("#ns-strucnasprema").val('');

        clearOldMessage();
    }
}