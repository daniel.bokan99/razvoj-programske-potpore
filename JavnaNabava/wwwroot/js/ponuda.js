﻿$(document).on('click', '.deleterow', function () {
    event.preventDefault();
    let tr = $(this).parents('tr');
    tr.remove();
    clearOldMessage();
})

$(function () {
    $(".form-control").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
        }
    });

    $("#stavka_troskovnika-kolicina").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            dodajStavkuTroskovnika();
        }
    });


    $("#stavka_troskovnika-dodaj").click(function () {
        event.preventDefault();
        dodajStavkuTroskovnika();
    });
});

function dodajDokumentPonude() {
    var sifra = $("#stavkatroskovnika-id").val();
    if (sifra != '') {
        if ($("[name='Ponude[" + urudzbeniBr + "].urudzbeniBr'").length > 0) {
            alert('Stavka je već u troškovniku');
            return;
        }

        var urudzbeniBr = $("#stavka_troskovnika-opis").val();
        var template = $('#novaPonudaTemplate').html();
        var naslov = $("#stavkatroskovnika-naziv").val();
        var fileBinary = $("#stavka_troskovnika-kolicina").val();
        var fK_vrstaDokumentaId = $("#stavka_troskovnika-kolicina").val();


        template = template.replace(/--UrudzbeniBr--/g, urudzbeniBr)
            .replace(/--DatumDokumenta--/g, datumDokumenta)
            .replace(/--Naslov--/g, naslov)
            .replace(/--FileBinary--/g, fileBinary)
            .replace(/--FK_vrstaDokumentaId--/g, fK_vrstaDokumentaId)
        $(template).find('tr').insertBefore($("#table-stavke").find('tr').last());

        $("#stavkatroskovnika-id").val('');
        $("#stavka_troskovnika-kolicina").val('');
        $("#stavka_troskovnika-opis").val('');
        $("#stavkatroskovnika-naziv").val('');

        clearOldMessage();
    }
}