﻿$(document).on('click', '.deleterow', function () {
    event.preventDefault();
    let tr = $(this).parents('tr');
    tr.remove();
    clearOldMessage();
})

$(function () {
    $("#kontakt-dodaj").click(function () {
        event.preventDefault();
        dodajKontaktOsobe();
    });
});

function dodajKontaktOsobe() {
    var kontaktId = $("#kontakt-id").val();
    if (kontaktId != '') {
        if ($("[name='OsobaKontakti[" + kontaktId + "].FkKontaktId'").length > 0) {
            alert('Kontakt je već zabilježen uz osobu');
            return;
        }

        var fkKontaktId = $("#kontakt-fkkontaktid").val();
        var podaci = $("#kontakt-podaci").val();
        var naziv = $("#kontakt-naziv").val();
        var template = $('#template').html();

        template = template.replace(/--fkKontaktId--/g, fkKontaktId)
            .replace(/--kontaktId--/g, kontaktId)
            .replace(/--podaci--/g, podaci)
            .replace(/--naziv--/g, naziv)
        $(template).find('tr').insertBefore($("#table-kontakti").find('tr').last());

        $("#kontakt-fkkontaktid").val('');
        $("#kontakt-id").val('');
        $("#kontakt-podaci").val('');
        $("#kontakt-naziv").val('');

        clearOldMessage();
    }
}