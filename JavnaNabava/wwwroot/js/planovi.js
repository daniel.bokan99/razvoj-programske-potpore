﻿$(document).on('click', '.deleterow', function () {
    console.log("tu sam")
    event.preventDefault();
    var tr = $(this).parents("tr");
    tr.remove();
    clearOldMessage();
});

$(function () {
    $(".form-control").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
        }
    });

    $("#stavka-id, #stavka-vrijednost").bind('keydown', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            dodajArtikl();
        }
    });


    $("#stavka-dodaj").click(function () {
        event.preventDefault();
        dodajArtikl();
    });
});

function dodajArtikl() {
    var id = $("#stavka-id").val();
    if (id != '') {
        if ($("[name='Stavka[" + id + "].Id'").length > 0) {
            alert('Artikl je već u dokumentu');
            return;
        }

        //var kolicina = parseFloat($("#tvrtka-naziv").val().replace(',', '.')); //treba biti točka, a ne zarez za parseFloat
        //if (isNaN(kolicina))
        //    kolicina = 1;

        //var rabat = parseFloat($("#artikl-rabat").val().replace(',', '.')); //treba točka, a ne zarez za parseFloat
        //if (isNaN(rabat))
        //    rabat = 0;

        //var cijena = parseFloat($("#artikl-cijena").val());
        var template = $('#novaStavkaTemplate').html();
        var evidencijskibroj = $("#stavka-evidencijskibroj").val();
        var predmetnabave = $("#stavka-predmetnabave").val();
        var procjenjenavrijednost = $("#stavka-procjenjenavrijednost").val();
        var vrijediod = $("#stavka-vrijediod").val();
        var vrijedido = $("#stavka-vrijedido").val();
        var cpv = $("#stavka-cpv").val();
        var valuta = $("#stavka-valuta").val();
        var vrstapostupka = $("#stavka-vrstapostupka").val();
        //var iznos = kolicina * cijena * (1 - rabat);
        //iznos = iznos.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' kn';
        //var cijena_formatirana = cijena.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' kn'

        //Alternativa ako su hr postavke sa zarezom //http://haacked.com/archive/2011/03/19/fixing-binding-to-decimals.aspx/
        //ili ovo http://intellitect.com/custom-model-binding-in-asp-net-core-1-0/

        template = template.replace(/--Id--/g, id)
            .replace(/--EvidencijskiBroj--/g, evidencijskibroj)
            .replace(/--PredmetNabave--/g, predmetnabave)
            .replace(/--ProcjenjenaVrijednost--/g, procjenjenavrijednost)
            .replace(/--VrijediOd--/g, vrijediod)
            .replace(/--VrijediDo--/g, vrijedido)
            .replace(/--FkCpv--/g, cpv)
            .replace(/--FkIso--/g, valuta)
            .replace(/--FkIdVrstaPostupka--/g, vrstapostupka)
        $(template).find('tr').insertBefore($("#table-tvrtke").find('tr').last());

        $("#stavka-id").val('');
        $("#stavka-evidencijskibroj").val('');
        $("#stavka-predmetnabave").val('');
        $("#stavka-procjenjenavrijednost").val('');
        $("#stavka-vrijediod").val('');
        $("#stavka-vrijedido").val('');
        $("#stavka-cpv").val('');
        $("#stavka-valuta").val('');
        $("#stavka-vrstapostupka").val('');

        clearOldMessage();
    }
}