﻿using System.Text.Json.Serialization;

namespace JavnaNabava.ViewModels
{
    public class AutoCompleteArtikl
    {
        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("naziv")]
        public string Naziv { get; set; }
        [JsonPropertyName("fkkontaktid")]
        public int FkKontaktId { get; set; }
        public AutoCompleteArtikl() { }
        public AutoCompleteArtikl(int id, string label, string naziv, int fkkontaktid)
        {
            Id = id;
            Label = label;
            Naziv = naziv;
            FkKontaktId = fkkontaktid;
        }
    }
}
