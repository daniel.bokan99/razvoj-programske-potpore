﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels {
    public class KonzorcijViewModel2 {

        public KonzorcijViewModel2() {
            this.Tvrtke = new List<TvrtkaViewModel2>();
        }

        [Required(ErrorMessage = "OIB polje je obavezno")]
        [MaxLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [MinLength(11, ErrorMessage = "Duljina OIB-a mora biti 11")]
        [DisplayName("OIB")]
        public string Oib { get; set; }

        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Adresa sjedišta je obavezna")]
        [DisplayName("Adresa")]
        public string AdresaSjedista { get; set; }
        [Required(ErrorMessage = "Mjesto je obavezno")]
        [DisplayName("Mjesto")]
        public string FkMjesto { get; set; }
        [Required]
        [DisplayName("Vrsta Konzorcija")]
        public string FkVrstaKonzorcija { get; set; }

        public IEnumerable<TvrtkaViewModel2> Tvrtke { get; set; }



    }

    
}
