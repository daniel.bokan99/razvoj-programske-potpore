﻿using System;
using System.Collections.Generic;
using JavnaNabava.ViewModels;
using JavnaNabava.Model;

namespace JavnaNabava.ViewModels
{
    public class StavkaNabaveViewModel
    {
        public int Id { get; set; }

        public int EvidencijskiBroj { get; set; }

        public string PredmetNabave { get; set; }

        public int ProcjenjenaVrijednost { get; set; }

        public DateTime VrijediOd { get; set; }

        public DateTime VrijediDo { get; set; }

        public string PlanNabave { get; set; }

        public string CPV { get; set; }

        public string Valuta { get; set; }
     
        public string VrstaPostupka { get; set; }

    }
}
