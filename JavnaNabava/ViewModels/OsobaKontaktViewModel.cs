﻿using System;
using System.Collections.Generic;
using JavnaNabava.ViewModels;
using JavnaNabava.Model;

namespace JavnaNabava.ViewModels
{
    public class OsobaKontaktViewModel
    {
        public int FkKontaktId { get; set; }

        public int KontaktId { get; set; }

        public string KontaktPodatci { get; set; }

        public string NazivTipKontakta { get; set; }
    }
}
