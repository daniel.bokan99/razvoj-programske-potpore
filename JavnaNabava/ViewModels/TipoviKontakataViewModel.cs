﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class TipoviKontakataViewModel
    {
        public IEnumerable<SifTipKontaktum> TipoviKontakata { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
