﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class DokumentiViewModel
    {
        public string urudzbeniBr { get; set; }

        public DateTime datumDokumenta { get; set; }

        public string naslov { get; set; }

        public string fileBinary { get; set; }

        public string ponuda { get; set; }
        public string ponudaStrucnjak { get; set; }

        public string vrsta { get; set; }
        public string natjecaj { get; set; }


    }
}
