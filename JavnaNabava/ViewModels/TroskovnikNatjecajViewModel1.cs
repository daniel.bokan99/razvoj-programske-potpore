﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class TroskovnikNatjecajViewModel1
    {
        public TroskovnikNatjecajViewModel1 () {}
        public int Id { get; set; }
        public string Naslov { get; set; }
        public string Opis { get; set; }
        public string NazivNat { get; set; }

    }
}