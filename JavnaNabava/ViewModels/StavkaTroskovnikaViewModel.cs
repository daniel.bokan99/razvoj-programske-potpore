﻿namespace JavnaNabava.ViewModels
{
    public class StavkaTroskovnikaViewModel
    {
        public int Id { get; set; }

        public string Opis { get; set; }

        public int Kolicina { get; set; }

        public string Naziv { get; set; }

        public string Naslov { get; set; }
    }
}