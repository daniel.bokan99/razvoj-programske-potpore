﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class SifJedMjereViewModel
    {
        public IEnumerable<SifJedMjere> Sif_jedMj { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
