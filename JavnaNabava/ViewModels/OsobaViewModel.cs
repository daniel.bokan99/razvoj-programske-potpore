﻿using System;
using System.Collections.Generic;
using JavnaNabava.ViewModels;
using JavnaNabava.Model;
using System.ComponentModel.DataAnnotations;

namespace JavnaNabava.ViewModels
{
    public class OsobaViewModel
    {
        [Required(ErrorMessage = "Polje OIB je obavezno"),
            MinLength(11, ErrorMessage = "OIB mora imati 11 znakova"),
            MaxLength(11, ErrorMessage = "OIB mora imati 11 znakova"),
            Display(Name = "OIB", Prompt = "Unesite OIB")]
        public string Oib { get; set; }

        [Required(ErrorMessage = "Polje Ime je obavezno"),
            Display(Name = "Ime", Prompt = "Unesite ime osobe")]
        public string Ime { get; set; }

        [Required(ErrorMessage = "Polje Prezime je obavezno"),
            Display(Name = "Prezime", Prompt = "Unesite prezime osobe")]
        public string Prezime { get; set; }

        [Required(ErrorMessage = "Polje Tvrtka je obavezno"),
            Display(Name = "Tvrtka")]
        public string FkTvrtka { get; set; }
        [Display(Prompt = "Odaberite tvrtku osobe")]
        public string NazivTvrtke { get; set; }

        [Required(ErrorMessage = "Polje Funkcija je obavezno"),
            Range(1, Int32.MaxValue),
            Display(Name = "Funkcija")]
        public int FkFunkcijaId { get; set; }
        [Display(Prompt = "Odaberite funkciju osobe")]
        public string NazivFunkcije { get; set; }

        [Required(ErrorMessage = "Polje Adresa je obavezno"),
            Display(Name = "Adresa", Prompt = "Unesite adresu osobe")]
        public string Adresa { get; set; }

        [Required(ErrorMessage = "Polje Stručna sprema je obavezno"),
            Range(1, Int32.MaxValue),
            Display(Name = "Stručna sprema")]
        public int FkStrucnaSprema { get; set; }
        [Display(Prompt = "Odaberite stručnu spremu osobe")]
        public string NazivStrucneSpreme { get; set; }

        [Required(ErrorMessage = "Polje Mjesto je obavezno"),
            Range(1, Int32.MaxValue),
            Display(Name = "Mjesto")]
        public int FkMjesto { get; set; }
        [Display(Prompt = "Odaberite mjesto osobe")]
        public string NazivMjesta { get; set; }

        public virtual SifFunkcija FkFunkcija { get; set; }
        public IEnumerable<OsobaKontaktViewModel> OsobaKontakti { get; set; }

        public OsobaViewModel()
        {
            this.OsobaKontakti = new List<OsobaKontaktViewModel>();
        }
    }
}
