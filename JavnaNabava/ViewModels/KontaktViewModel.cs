﻿namespace JavnaNabava.ViewModels
{
    public class KontaktViewModel
    {
        public int Id { get; set; }
        public string KontaktPodaci { get; set; }
        public string NazivKontakta { get; set; }
    }
}