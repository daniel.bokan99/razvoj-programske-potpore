﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class NatjecajViewModel
    {
        public IEnumerable<NatjecajStrucnjaciViewModel> NatjecajStrucnjaci { get; set; }

        public IEnumerable<NatjecajKriterijOcjenjivanjaViewModel> NatjecajKriterij { get; set; }
        public string EvidencijskiBr { get; set; }

        public string Naziv { get; set; }

        public int IdStavkaNabave { get; set; }

        public DateTime RokZaDostavu { get; set; }

        public string TrajanjeUgovora { get; set; }

        public int IdVrstaUgovora { get; set; }

        public int SlanjeElPostom { get; set; }

        public string PopisPotrebnihDokumenata { get; set; }
        public int IdDjelatnost { get; set; }
        public int JePonisten { get; set; }

        public int Zakon { get; set; }

        public string NazivZakon { get; set; }

        public string Kriterij { get; set; }

        public string NazivStavke { get; set; }

        public string NazivUgovora { get; set; }

        public string NazivDjelatnosti { get; set; }

        public NatjecajViewModel()
        {
            this.NatjecajStrucnjaci = new List<NatjecajStrucnjaciViewModel>();
        }

    }
}
