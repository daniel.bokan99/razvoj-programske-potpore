﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels {
    public class TvrtkaViewModel2 {

        [Required(ErrorMessage = "Oib polje je obavezno")]
        [MaxLength(11, ErrorMessage = "Duljina oiba mora biti 11")]
        [MinLength(11, ErrorMessage = "Duljina oiba mora biti 11")]
        public string Oib { get; set; }

        public string Naziv { get; set; }

        [DisplayName("Mjesto")]
        public string FkMjesto { get; set; }

        public string Adresa { get; set; }

        public string Djelatnost { get; set; }

    }
}
