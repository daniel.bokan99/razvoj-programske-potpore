﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class StavkeNabaveViewModel
    {
        public IEnumerable<StavkaNabaveViewModel> Stavke { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
