﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class FunkcijeViewModel
    {
        public IEnumerable<SifFunkcija> Funkcije { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
