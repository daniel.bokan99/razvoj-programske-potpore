﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace JavnaNabava.ViewModels
{
    public class VrstePonuditeljaViewModel
    {
        public IEnumerable<SifVrstaPonuditelja> Vrste { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}


