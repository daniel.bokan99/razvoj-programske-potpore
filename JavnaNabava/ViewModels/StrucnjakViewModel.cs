﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class StrucnjakViewModel
    {
        public int Id { get; set; }

        public string ponuda { get; set; }

        public string natjecaj { get; set; }

        public string osoba { get; set; }

    }
}
