﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class NatjecajKriterijOcjenjivanjaTableViewModel
    {
        public IEnumerable<NatjecajKriterijOcjenjivanja> NatjecajKriterij { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
