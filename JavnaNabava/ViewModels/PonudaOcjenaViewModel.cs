﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class PonudaOcjenaViewModel
    {
        public int Id { get; set; }

        public int ostvareniBodovi { get; set; }

        public string natjecajKriterij { get; set; }

        public string ponuda { get; set; }
    }
}
