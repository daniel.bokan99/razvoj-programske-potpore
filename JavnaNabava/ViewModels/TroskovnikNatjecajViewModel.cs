﻿using System.Collections.Generic;

namespace JavnaNabava.ViewModels
{
    public class TroskovnikNatjecajViewModel
    {
        public int Id { get; set; }
        public string Naslov { get; set; }
        public string Opis { get; set; }
        public string NazivNat { get; set; }
        public IEnumerable<StavkaTroskovnikaViewModel> stavke { get; set; }
        public TroskovnikNatjecajViewModel()
        {
            this.stavke = new List<StavkaTroskovnikaViewModel>();
        }
    }
}