﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class PlanNabaveViewModel2
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string NazivTvrtke { get; set; }

    }
}
