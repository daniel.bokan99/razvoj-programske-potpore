﻿
using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace JavnaNabava.ViewModels
{
    public class VrsteDokumenataViewModel
    {
        public IEnumerable<SifVrstaDokumentum> Vrste { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}


