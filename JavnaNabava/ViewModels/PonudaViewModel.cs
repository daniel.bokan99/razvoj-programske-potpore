﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class PonudaViewModel
    {
        public PonudaViewModel()
        {
            this.ponude = new List<PonudaViewModel>();
        }
        public int Id { get; set; }

        public double ukupnaCijenaPonude { get; set; }

        public string vrstaPonuditelja { get; set; }

        public string tvrtka { get; set; }

        public string konzorcij { get; set; }

        public IEnumerable<DokumentiViewModel> PonudaDokumenti { get; set; }
        public IEnumerable<PonudaViewModel> ponude { get; set; }

    }
}
