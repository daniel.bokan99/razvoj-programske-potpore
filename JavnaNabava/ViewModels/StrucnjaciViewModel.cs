﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace JavnaNabava.ViewModels
{
    public class StrucnjaciViewModel
    {
        public IEnumerable<StrucnjakViewModel> Strucnjaci { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}

