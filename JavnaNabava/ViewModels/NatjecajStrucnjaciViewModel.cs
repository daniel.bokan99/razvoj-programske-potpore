﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class NatjecajStrucnjaciViewModel
    {
        public int Id { get; set; }

        public string OpisKriterija { get; set; }

        public int BrPotrebnihStrucnjaka { get; set; }

        public string EvidencijskiBr { get; set; }

        public string StrucnaSprema { get; set; }
    }
}
