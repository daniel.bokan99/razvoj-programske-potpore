﻿namespace JavnaNabava.ViewModels
{
    public class TroskovnikPonudaViewModel
    {
        public int Id { get; set; }
        public string Naslov { get; set; }
        public string Opis { get; set; }
        public double UkCijena { get; set; }
        public string Ponuditelj { get; set; }
    }
}