﻿namespace JavnaNabava.ViewModels
{
    public class MjestoViewModel
    {
        public int PostanskiBroj { get; set; }
        public string Naziv { get; set; }
        public string NazivDrzave { get; set; }
    }
}