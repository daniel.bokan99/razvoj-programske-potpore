﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace JavnaNabava.ViewModels
{
    public class DokumentViewModel
    {
        public IEnumerable<DokumentiViewModel> Dokumenti { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}


