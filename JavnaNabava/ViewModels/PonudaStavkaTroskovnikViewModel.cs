﻿namespace JavnaNabava.ViewModels
{
    public class PonudaStavkaTroskovnikViewModel
    {
        public int Id { get; set; }

        public double JedinicnaCijena { get; set; }

        public double UkupnaCijenaBezPDV { get; set; }

        public double PDV { get; set; }

        public double UkupnaCijenaSaPDV { get; set; }

        public string Naslov { get; set; }

        public string Opis { get; set; }
    }
}