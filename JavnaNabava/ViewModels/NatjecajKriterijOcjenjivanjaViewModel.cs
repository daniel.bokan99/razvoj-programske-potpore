﻿using JavnaNabava.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.ViewModels
{
    public class NatjecajKriterijOcjenjivanjaViewModel
    {
        public int Id { get; set; }
        public string Opis { get; set; }
        public int Ponder { get; set; }
        public string EvidencijskiBr { get; set; }
    }
}
