using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace JavnaNabava {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) {
            services.AddControllersWithViews()
                    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>())
                    .AddJsonOptions(configure => configure.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddDbContext<Model.RPPP17Context>(options =>
            {
                string connString = Configuration.GetConnectionString("JavnaNabava");
                options.UseSqlServer(connString);
            });

            var appSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSection);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "RPPP17 WebAPI", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddTransient<ApiControllers.ApiKonzorcijController>();
            services.AddTransient<ApiControllers.ApiPlanNabaveController>();
            services.AddTransient<ApiControllers.SifDjelatnostTvrtkeController>();
            services.AddTransient<ApiControllers.MjestoController>();
            services.AddTransient<ApiControllers.TroskovnikNatjecajController>();
            services.AddTransient<ApiControllers.PonudaController>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {

            string baseApiUrl = Configuration.GetSection("BaseApiUrl").Value;

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                   name: "AutoCompleteRoute",
                   pattern: "autocomplete/{controller}/{action=Get}",
                   new { area = "AutoComplete" });

                endpoints.MapControllerRoute("",
                "{action}/{controller:regex(^(StavkaTroskovnika|TroskovnikNatjecaj|SifJedMjere|TroskovnikPonuda|PonudaStavkaTroskovnik)$)}/Page{page}/Sort{sort:int}/ASC-{ascending:bool}/{id?}",
                new { action = "Index" });

                //endpoints.MapDefaultControllerRoute();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("swagger/v1/swagger.json", "RPPP17 WebAPI");
                c.RoutePrefix = String.Empty;
            });

        }
    }
}
