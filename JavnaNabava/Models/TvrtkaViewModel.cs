﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Models {
    public class TvrtkaViewModel {

        public IEnumerable<TvrtkaDTO> tvrtka { get; set; }

        public PagingInfo PagingInfo { get; set; }
        
    }
}
