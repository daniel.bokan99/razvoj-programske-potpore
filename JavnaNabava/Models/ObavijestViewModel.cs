﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Models {
    public class ObavijestViewModel {

        public IEnumerable<ObavijestDTO> Obavijest { get; set; }

        public PagingInfo PagingInfo { get; set; }

    }
}
