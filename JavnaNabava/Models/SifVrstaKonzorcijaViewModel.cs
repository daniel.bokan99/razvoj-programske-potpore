﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Models {
    public class SifVrstaKonzorcijaViewModel {

        public IEnumerable<SifVrstaKonzorcija> SifVrstaKonzorcija { get; set; }

        public PagingInfo PagingInfo { get; set; }

    }
}
