﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Models {
    public class SifKategorijaObavijestiViewModel {

        public IEnumerable<SifKategorijeObavijesti> SifKategorijeObavijesti { get; set; }

        public PagingInfo PagingInfo { get; set; }

    }
}
