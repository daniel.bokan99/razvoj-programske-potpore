﻿using JavnaNabava.Model;
using JavnaNabava.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Models {
    public class KonzorcijViewModel {

        public IEnumerable<KonzorcijDTO> Dto { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
