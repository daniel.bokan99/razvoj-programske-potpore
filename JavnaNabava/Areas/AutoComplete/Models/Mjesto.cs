﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models {
    public class Mjesto : IdLabel{

        public Mjesto() { }
        public Mjesto(int id, string label) : base(id, label) { }
    }
}
