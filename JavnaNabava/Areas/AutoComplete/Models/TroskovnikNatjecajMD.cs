﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models
{
    public class TroskovnikNatjecajMD
    {

            [JsonPropertyName("label")]
            public string Label { get; set; }
            [JsonPropertyName("evbr")]
            public string Id { get; set; }
            public TroskovnikNatjecajMD() { }
            public TroskovnikNatjecajMD(string evbr, string label)
            {
                Id = evbr;
                Label = label;
            }
    }
}
