﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models {
    public class Tvrtka{

        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("id")]
        public string Id { get; set; }
        public Tvrtka() { }
        public Tvrtka(string evbr, string label) {
            Id = evbr;
            Label = label;
        }

        //public Tvrtka() { }
        //public Tvrtka(int id, string label) : base(id, label) { }

    }
}
