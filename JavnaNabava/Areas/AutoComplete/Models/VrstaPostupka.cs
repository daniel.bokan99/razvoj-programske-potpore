﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models
{
    public class VrstaPostupka : IdLabel
    {

        public VrstaPostupka() { }
        public VrstaPostupka(int id, string label) : base(id, label) { }

    }
}
