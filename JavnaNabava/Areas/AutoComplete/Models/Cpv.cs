﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models
{
    public class Cpv
    {

        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("id")]
        public string Id { get; set; }
        public Cpv() { }
        public Cpv(string cpv, string label)
        {
            Id = cpv;
            Label = label;
        }

        //public Tvrtka() { }
        //public Tvrtka(int id, string label) : base(id, label) { }

    }
}
