﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models {
    public class Djelatnost : IdLabel{

        public Djelatnost() { }
        public Djelatnost(int id, string label) : base(id, label) { }

    }
}
