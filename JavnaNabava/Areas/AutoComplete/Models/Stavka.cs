﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models
{
    public class Stavka
    {

        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("id")]
        public int Id { get; set; }
        public Stavka() { }
        public Stavka(int evbr, string label)
        {
            Id = evbr;
            Label = label;
        }

        

    }
}
