﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Models {
    public class StrucnaSprema : IdLabel{

        public StrucnaSprema() { }
        public StrucnaSprema(int id, string label) : base(id, label) { }
    }
}
