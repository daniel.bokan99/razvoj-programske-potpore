﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Controllers {
    [Area("AutoComplete")]
    public class ACDjelatnostController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appData;

        public ACDjelatnostController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appData = optionsSnapshot.Value;
        }

        public async Task<IEnumerable<Models.Djelatnost>> Get(string term) {
            var query = ctx.SifDjelatnostTvrtke
                .Select(t => new Models.Djelatnost {
                    Id = t.Id,
                    Label = t.Naziv
                })
                .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                .ThenBy(l => l.Id)
                .Take(appData.AutoCompleteCount)
                .ToListAsync();
            return list;
        }

    }
}
