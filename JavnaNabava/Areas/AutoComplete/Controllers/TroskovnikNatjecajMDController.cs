﻿using JavnaNabava.Areas.AutoComplete.Models;
using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Controllers
{
    [Area("AutoComplete")]
    public class TroskovnikNatjecajMDController : Controller
    {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appSettings;

        public TroskovnikNatjecajMDController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        public IEnumerable<Models.TroskovnikNatjecajMD> Get(string term)
        {
            var query = ctx.Natjecaj
                            .Where(s => s.Naziv.Contains(term))
                            .OrderBy(s => s.Naziv)
                            .Select(m => new Models.TroskovnikNatjecajMD
                            {
                                Id = m.EvidencijskiBr,
                                Label = m.Naziv
                            });

            var list = query.OrderBy(l => l.Label)
                                  .ThenBy(l => l.Id)
                                  .Take(appSettings.AutoCompleteCount)
                                  .ToList();
            return list;
        }
    }
}
