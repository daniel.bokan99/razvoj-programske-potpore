﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Controllers {
    [Area("AutoComplete")]
    public class ACTvrtkaController : Controller {

        private readonly RPPP17Context ctx;
        private readonly AppSettings appData;

        public ACTvrtkaController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appData = optionsSnapshot.Value;
        }

        public async Task<IEnumerable<Models.Tvrtka>> Get(string term) {
            var query = ctx.Tvrtka
                .Select(t => new Models.Tvrtka {
                    Id = t.Oib,
                    Label = t.Naziv
                })
                .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                .ThenBy(l => l.Id)
                .Take(appData.AutoCompleteCount)
                .ToListAsync();
            return list;
        }

    }
}
