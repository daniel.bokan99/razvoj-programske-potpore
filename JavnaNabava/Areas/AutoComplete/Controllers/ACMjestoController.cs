﻿using JavnaNabava.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavnaNabava.Areas.AutoComplete.Controllers {
    [Area("AutoComplete")]
    public class ACMjestoController : Controller {
        private readonly RPPP17Context ctx;
        private readonly AppSettings appData;

        public ACMjestoController(RPPP17Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot) {
            this.ctx = ctx;
            appData = optionsSnapshot.Value;
        }

        public async Task<IEnumerable<Models.Mjesto>> Get(string term) {
            var query = ctx.Mjesto
                .Select(m => new Models.Mjesto {
                    Id = m.PostanskiBroj,
                    Label = m.Naziv
                })
                .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                  .ThenBy(l => l.Id)
                  .Take(appData.AutoCompleteCount)
                  .ToListAsync();
            return list;
        }
    }
}
